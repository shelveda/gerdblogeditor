import { configureStore } from 'redux/slices/node_modules/@reduxjs/toolkit';
import reducer from './../slices';

export default configureStore({
  reducer,
});
