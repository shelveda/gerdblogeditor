import baseReducer from './base';
import loadingReduer from './loader';

const reducer = {
  base: baseReducer,
  loading: loadingReduer,
};

export default reducer;
