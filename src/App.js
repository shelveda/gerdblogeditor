import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { LoadingProvider } from './contexts/LoadingContext';
import Router from './Router';
import { AuthProvider } from './contexts/AuthContext';
import { ResourceProvider } from './contexts/ResourceContext';
import { MasterApi } from './api/MasterApi';
import { errorAlert } from './components/Alerts';

function App() {
  useEffect(() => {
    async function fetchData() {
      const user = JSON.parse(sessionStorage.getItem('user'));

      if (user) {
        const response = await MasterApi('POST', '/check', {
          role_name: user.role,
        });

        if (!response) {
          sessionStorage.clear();
          window.location.reload();
        }
        if (response.status === 200) {
          return '';
        } else {
          errorAlert(
            () => {
              sessionStorage.clear();
              window.location.reload();
            },
            {
              text: 'در حال بررسی ',
            }
          );
        }
      }
    }
    fetchData();
  });

  return (
    <AuthProvider>
      <ResourceProvider>
        <LoadingProvider>
          <BrowserRouter>
            <div className="container mx-auto pt-2 pb-10">
              <Router />
            </div>
          </BrowserRouter>
        </LoadingProvider>
      </ResourceProvider>
    </AuthProvider>
  );
}

export default App;
