import React from 'react';
import './index.css';
const LoadingComponent = () => {
  return <div className="spinner"></div>;
};

export default LoadingComponent;
