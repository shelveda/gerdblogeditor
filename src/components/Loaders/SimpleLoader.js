import React from 'react';

import styled from 'styled-components';
import LoadingComponent from './LoadingComponent';

const Div = styled.div`
  position: fixed;
  top: 0px;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(112, 112, 112, 0.1);
  z-index: 2000;
  text-align: center;
  display: grid;
  align-items: center;
`;

export default function(props) {
  return (
    <Div>
      <LoadingComponent />
    </Div>
  );
}
