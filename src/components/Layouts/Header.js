import React from 'react';

function Header({ type, title = 'سعید', loc = null }) {
  let Text = type === 'edit' ? ` ویرایش مقاله ${title}` : 'نوشتن مقاله جدید';

  if (loc === 'page')
    Text = type === 'edit' ? ` ویرایش صفحه ${title}` : 'نوشتن صفحه جدید';
  if (loc === 'blog')
    Text = type === 'edit' ? ` ویرایش بلاگ ${title}` : 'نوشتن بلاگ جدید';

  return (
    <section className="section-edit-header">
      <div className="head" style={{ marginTop: '50px' }}>
        <h1
          style={{ color: '#4baf00', fontSize: '2.3em', textAlign: 'center' }}
        >
          {Text}
        </h1>
      </div>
    </section>
  );
}

export default Header;
