import React, { useState } from 'react';
import { UseUserDispatch } from '../../../contexts/AuthContext';
import { MasterApi } from '../../../api/MasterApi';
import styled from 'styled-components';
import { useLoadingDispatch } from '../../../contexts/LoadingContext';

// #region comps

const Input = styled.input`
  background-color: #ffffff;
  color: black;
  min-width: 80px;
  border: 1px solid white;
  box-shadow: 2px 2px 5px gray;
  border-radius: 5px;
`;

// #endregion

export default function LoginModal(props) {
  const dispatch = UseUserDispatch();
  const { setLoading } = useLoadingDispatch();
  const { setToggle } = props;
  const [state, setState] = useState({
    email: '',
    password: '',
    deactive: false,
  });

  async function handleSubmit(e) {
    setLoading(true);
    e.preventDefault();
    setState({ ...state, deactive: true });
    const response = await MasterApi('POST', '/login', {
      email: state.email,
      password: state.password,
    });

    if (response.status === 200) {
      dispatch({
        type: 'LOGIN',
        payload: {
          name: response.data.name,
          role: response.data.role,
          token: response.data.token,
        },
      });
    }

    setToggle(false);
    setLoading(false);
  }

  function handleChange(e) {
    e.persist();
    const name = e.target.name;
    const value = e.target.value;
    setState({
      ...state,
      [name]: value,
    });
  }
  return (
    <div style={{ minHeight: '150px', direction: 'ltr' }}>
      <label style={{ padding: '8px' }}>userName or Email </label>
      <Input
        name="email"
        id={'modal_user'}
        value={state.email}
        onChange={handleChange}
      />
      <br />
      <label style={{ padding: '8px' }}>Password </label>
      <Input
        name="password"
        id={'modal_pass'}
        value={state.password}
        onChange={handleChange}
      />
      <br />
      <button
        style={{
          margin: '8px',
          backgroundColor: 'red',
          color: 'white',
          border: '1px solid red',
          boxShadow: '2px 1px 5px gray',
          borderRadius: '5px',
          padding: '2px 8px',
          cursor: 'pointer',
        }}
        onClick={handleSubmit}
      >
        submit
      </button>
    </div>
  );
}
