import React, { useState } from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';

import LOGO from 'assets/logo.png';

import OutsideCallBack from 'hooks/useOutSideCallback';

export default function AdminNav() {
  // const url = useLocation();
  // const history = useHistory();

  // function newPost() {
  //   if (url.pathname.search('editor') && url.pathname !== '/editor/newPost') {
  //     history.replace('/editor/newPost');
  //     history.go('');
  //   }
  // }
  return (
    <NewHeader />
    // <AddNavDiv>
    //   <ul
    //     style={{
    //       float: 'left',
    //       color: 'black',
    //       display: 'flex',
    //       direction: 'ltr',
    //       paddingRight: '1rem',
    //     }}
    //   >
    //     <li
    //       style={{
    //         marginLeft: '10px',
    //         paddingLeft: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/CommentList"> نظرات</Link>
    //     </li>
    //     {/* <li
    //       style={{
    //         marginLeft: '10px',
    //         marginRight: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/seo"> Seo</Link>
    //     </li>
    //      */}

    //     <li
    //       style={{
    //         marginLeft: '10px',
    //         marginRight: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/posts"> مقالات و صفحات </Link>
    //     </li>
    //     <li
    //       style={{
    //         marginLeft: '10px',
    //         marginRight: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/tags">برچسب ها</Link>
    //     </li>
    //     <li
    //       style={{
    //         marginLeft: '10px',
    //         marginRight: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/catList"> تعریف رسته و حوزه</Link>
    //     </li>
    //     <li style={{ marginLeft: '10px', listStyle: 'none' }}>
    //       <Link onClick={newPost} to="/editor/newPost">
    //         مقاله جدید
    //       </Link>
    //     </li>
    //     {/* <li
    //       style={{
    //         marginLeft: '10px',
    //         paddingLeft: '10px',
    //         listStyle: 'none',
    //       }}
    //     >
    //       <Link to="/register"> register</Link>
    //     </li> */}
    //   </ul>
    // </AddNavDiv>
  );
}

const nabVarLinks = [
  {
    link: '/blogs',
    title: 'مدیریت بلاگ',
  },
  {
    link: '/posts',
    title: 'مدیریت مقالات',
  },
  {
    link: '/pages',
    title: 'مدیریت صفحات',
  },
  {
    link: '/CommentList',
    title: 'مدیریت نظرات',
  },
  {
    link: '/Content',
    title: 'تنظیمات کلی',
  },
];

const NewHeader = () => {
  const history = useHistory();

  function newPost() {
    history.push('/editor/newPost');
    // history.go('');
    setShow(false);
  }
  function newBlog() {
    history.push('/blog');
    // history.go('');
    setShow(false);
  }

  const [show, setShow] = useState(false);
  return (
    <header>
      <img alt="logoLocation" id="logo" src={LOGO} className="w-32 mt-2" />
      <div className="-mt-12">
        <div
          id="new_item"
          className="inline-block cursor-pointer w-20 text-sm py-1 text-center text-black rounded-t-lg"
          onClick={() => {
            setShow(!show);
          }}
        >
          {/* asgasgasdgasdg asdgjaslkdgj asdg */}
          <span>جدید</span>
          {show && (
            <OutsideCallBack
              callback={() => {
                setShow(false);
              }}
              Listener="mouseup"
            >
              <div id="children">
                <button
                  id="defaultTab"
                  className="tablinks inline-block cursor-pointer w-20 text-sm py-1 text-center text-black"
                  onClick={newPost}
                >
                  مقاله جدید
                </button>
                <button
                  className="tablinks inline-block cursor-pointer w-20 text-sm py-1 text-center text-black"
                  onClick={newBlog}
                >
                  بلاگ
                </button>
                <Link to="/pageEditor/newPost">
                  <button className="tablinks inline-block cursor-pointer w-20 text-sm py-1 text-center text-black">
                    صفحه
                  </button>
                </Link>
                <button className="tablinks inline-block cursor-pointer w-20 text-sm py-1 text-center text-black rounded-b-lg">
                  تبلیغ
                </button>
              </div>
            </OutsideCallBack>
          )}
        </div>

        <div className="tab text-left">
          {nabVarLinks.map(item => {
            return (
              <LINK
                link={item.link}
                key={`${item.link}item`}
                title={item.title}
              />
            );
          })}
        </div>
      </div>
    </header>
  );
};

const LINK = props => {
  const { link, title } = props;

  return (
    <Link to={link}>
      <button
        style={{ marginRight: '5px' }}
        className="tablinks px-4 text-sm py-1 text-center text-black rounded-t-lg bg-green-300 hover:bg-green-400"
      >
        {title}
      </button>
    </Link>
  );
};
