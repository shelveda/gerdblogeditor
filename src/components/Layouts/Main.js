import React from 'react';
import { Link } from 'react-router-dom';

function Main() {
  return (
    <section className="section-edit-main">
      <div className="edit-main-bar">
        <div className="edit-title">
          <p>عنوان</p>
        </div>
        <div className="edit-title">
          <p>مقدمه</p>
        </div>
        <div className="edit-title disable">
          <p>مدارک</p>
        </div>
        <div className="edit-title">
          <p>فرآیند اصلی</p>
        </div>
        <div className="edit-title">
          <input type="text" placeholder="فرایند اصلی" />
        </div>
        <div className="edit-image">
          <div className="title-image">
            <Link to="#">
              <i className="far fa-window-close"></i>
            </Link>
            <p>گالری عکس</p>
          </div>
          <div className="main-image">
            <div className="image-box selected">
              <Link to="#">
                <i className="check far fa-check-square"></i>
              </Link>
              <Link to="#">
                <i className="link fas fa-link"></i>
              </Link>
              <Link to="#">
                <i className="delete far fa-trash-alt"></i>
              </Link>
            </div>
            <div className="image-box"></div>
            <div className="image-box"></div>
            <div className="image-box"></div>
            <div className="image-box"></div>
          </div>
          <div className="foot-image">
            <div className="new-image">
              <Link to="#">
                <p>تصویر جدید</p>
              </Link>
              <Link to="#">
                <i className="fas fa-plus-circle"></i>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="edit-side-bar">
        <div className="link-box"></div>
      </div>
    </section>
  );
}

export default Main;
