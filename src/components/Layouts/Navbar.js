import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <section className="section-nav">
      <div className="nav-search">
        <div className="search">
          <Link to="#">
            <i className="fab fa-sistrix"></i>
          </Link>
          <input type="text" placeholder="جستجو" />
        </div>
      </div>
      <div className="navbar">
        <Link to="#" className="btn-nav">
          درباره ما
        </Link>
        <Link to="#" className="btn-nav">
          ارتباط با ما
        </Link>
        <Link to="#" className="btn-nav">
          موضوعات
        </Link>
        <Link to="#" className="btn-nav">
          خانه
        </Link>
      </div>
      <div className="square-nav"></div>
    </section>
  );
}

export default Navbar;
