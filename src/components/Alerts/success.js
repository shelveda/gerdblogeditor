import Swal from 'sweetalert2';

export default function successAlert(callBack, options = {}) {
  Swal.fire({
    icon: options.icon || 'success',
    title: options.title || 'Your work has been saved',
    showConfirmButton: false,
    timer: 1500,
    ...options,
  }).then(result => {
    if (result.dismiss) {
      callBack && callBack();
    }
  });
}
