import Swal from 'sweetalert2';

export default function errorAlert(callBack, options = {}) {
  return Swal.fire({
    icon: options.icon || 'error',
    title: options.title || 'Oops...',
    text: options.text || 'Something went wrong!',
    footer: options.footer || '<a href>Why do I have this issue?</a>',
  }).then(result => {
    if (result.value) {
      callBack && callBack();
    }
  });
}
