import Swal from 'sweetalert2';

export default function warningAlert(callBack) {
  Swal.fire({
    title: 'آیا مطمئن هستید؟',
    text: 'این دستور غیر قابل بازگشت است!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'خیر',
    confirmButtonText: 'بله',
  }).then(result => {
    if (result.value) {
      callBack && callBack();
    }
  });
}
