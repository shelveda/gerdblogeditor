import React from 'react';

import { COLORS } from '../styles/Colors';

function StyleButton(props) {
  const { onToggle, style, active, label } = props;
  function handleToggle(e) {
    e.preventDefault();
    onToggle(style);
  }

  let className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-purple-500';

  switch (label) {
    case 'Red':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-red-500';
      break;
    case 'Orange':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-orange-500';
      break;
    case 'Green':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-green-500';
      break;
    case 'Yellow':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-yellow-500';
      break;
    case 'Blue':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-blue-500';
      break;
    case 'Violet':
      className = 'btn bg-opacity-75 hover:bg-opacity-100 bg-purple-500';
      break;

    default:
      break;
  }

  return (
    <span
      active={active}
      onMouseDown={handleToggle}
      className={className}
    ></span>
  );
}

export default function ColorsControl(props) {
  const currentStyle = props.editorState.getCurrentInlineStyle();
  return (
    <>
      {COLORS.map(type => (
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </>
  );
}
