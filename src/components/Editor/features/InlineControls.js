import React from 'react';
import StyleButton from '../styles/StyleButton';

import { INLINE } from './../styles/Inlines';

export default function InlineControl(props) {
  const currentStyle = props.editorState.getCurrentInlineStyle();

  return (
    <>
      {INLINE.map(type => {
        return (
          <StyleButton
            color={type.label === 'Highlight' ? 'yellow' : ''}
            key={type.label}
            active={currentStyle.has(type.style)}
            label={type.label}
            onToggle={props.onToggle}
            style={type.style}
          />
        );
      })}
    </>
  );
}
