import React, { useEffect, useState } from 'react';
import { EditorState, RichUtils } from 'draft-js';
import { UseEditorContext, UseEditorDispatch } from '../contexts/EditorContext';
import styled from 'styled-components';
import { LinkA } from '../styles/StyledComponents';

export default ({ editorRef }) => {
  const urlRef = React.useRef(null);
  const editorState = UseEditorContext();
  const dispatch = UseEditorDispatch();
  const [state, setState] = useState({
    show: false,
    url: '',
    editorFocus: false,
  });

  useEffect(() => {
    state.editorFocus && editorRef && editorRef.current.focus();
    if (state.show) {
      urlRef.current.focus();
    }
  }, [state.show, state.editorFocus, urlRef, editorRef]);

  function onChange(e) {
    const value = e.target.value;
    setState({ ...state, url: value });
  }

  function promptForLink(e) {
    e.preventDefault();
    const selection = editorState.getSelection();

    if (!selection.isCollapsed()) {
      const contentState = editorState.getCurrentContent();
      const startKey = editorState.getSelection().getStartKey();
      const startOffset = editorState.getSelection().getStartOffset();
      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

      let url = '';
      if (linkKey) {
        const linkInstance = contentState.getEntity(linkKey);
        url = linkInstance.getData().url;
      }
      setState({
        ...state,
        show: true,
        editorFocus: false,
        url: url,
        followCond: true,
      });
    }
  }

  function confirmLink(e) {
    e.preventDefault();
    const { url, followCond } = state;
    const rel = followCond ? 'follow' : 'nofollow';
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'LINK',
      'MUTABLE',
      { url: url, rel: rel }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {
      currentContent: contentStateWithEntity,
    });

    dispatch({
      type: 'CHANGE',
      editorState: RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      ),
    });

    setState({
      show: false,
      editorFocus: true,
      url: '',
    });
    // editorRef.current.focus();
  }

  function onLinkInputKeyDown(e) {
    if (e.which === 13) {
      confirmLink(e);
    }
  }

  function removeLink(e) {
    e.preventDefault();
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      dispatch({
        type: 'CHANGE',
        editorState: RichUtils.toggleLink(editorState, selection, null),
      });
    }
  }
  function onFollowChange(e) {
    const { checked } = e.target;

    setState(prev => ({ ...prev, followCond: checked }));
  }

  return (
    <div class="insert-link flex-1">
      {!state.show && (
        <span
          onClick={promptForLink}
          style={{ marginRight: 10 }}
          className="remove-links rounded-lg text-xs px-2 mx-4 inline-block align-middle leading-7 cursor-pointer h-8 bg-green-200 border border-green-300 bg-opacity-75 hover:bg-opacity-100"
        >
          افزودن پیوند
        </span>
      )}

      {/* <div>
        <Butt onMouseDown={promptForLink} style={{ marginRight: 10 }}>
          درج لینک
        </Butt>
        <Butt onMouseDown={removeLink}>حذف لینک ها</Butt>
      </div> */}

      {state.show && (
        <>
          <span class="text-sm text-green-600 ml-2 font-bold">
            افزودن پیوند:
          </span>
          <input
            class="border border-green-600 rounded-lg text-xs h-8 px-1 ml-1 w-48"
            placeholder="آدرس پیوند را وارد کنید"
            onChange={onChange}
            ref={urlRef}
            type="text"
            value={state.url}
            onKeyDown={onLinkInputKeyDown}
          />
          <label class="cursor-pointer text-blue-500 mr-4">
            <input
              class="align-middle"
              type="checkbox"
              checked={state.followCond}
              onChange={onFollowChange}
            />
            <span className="ml-4 mr-2">follow</span>
          </label>

          <button
            class="submitButton w-8 h-8 font-bold align-top"
            onMouseDown={confirmLink}
          >
            +
          </button>
        </>
      )}
      {!state.show && (
        <span
          class="remove-links rounded-lg text-xs px-2 mx-4 inline-block align-middle leading-7 cursor-pointer h-8 bg-red-200 border border-red-300 bg-opacity-75 hover:bg-opacity-100"
          onMouseDown={removeLink}
        >
          حذف پیوندها
        </span>
      )}
    </div>
  );
};

// for decorator

export function findLinkEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
}

export const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};
