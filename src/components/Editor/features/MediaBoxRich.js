import React, { useEffect, useState } from 'react';
import { EditorState, AtomicBlockUtils } from 'draft-js';
import { UseEditorContext, UseEditorDispatch } from '../contexts/EditorContext';

export default function ImageBox({ editorRef }) {
  const editorState = UseEditorContext();
  const dispatch = UseEditorDispatch();

  const ImgRef = React.useRef(null);

  const [state, setState] = useState({
    show: false,
    url: '',
    urlType: '',
    alt: '',
    editorFocus: false,
  });

  useEffect(() => {
    state.editorFocus && editorRef && editorRef.current.focus();
    if (state.show) {
      ImgRef.current.focus();
    }
  }, [state.show, state.editorFocus, ImgRef, editorRef]);

  function onURLChange(e) {
    const value = e.target.value;
    setState({ ...state, url: value });
  }
  function onAltChange(e) {
    const value = e.target.value;
    setState({ ...state, alt: value });
  }

  function confirmMedia(e) {
    e.preventDefault();
    const { url, urlType, alt } = state;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      urlType,
      'MUTABLE',
      { src: url, alt }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {
      currentContent: contentStateWithEntity,
    });

    dispatch({
      type: 'CHANGE',
      editorState: AtomicBlockUtils.insertAtomicBlock(
        newEditorState,
        entityKey,
        ' '
      ),
    });

    setState({
      ...state,
      show: false,
      url: '',
      editorFocus: true,
      urlType: 'IMAGERICH',
    });
  }

  function onURLInputKeyDown(e) {
    if (e.which === 13) {
      confirmMedia(e);
    }
  }

  function promptForMedia(type) {
    setState({
      ...state,
      show: true,
      editorFocus: false,
      url: '',
      urlType: type,
    });
  }

  function addImage() {
    promptForMedia('IMAGERICH');
  }
  return (
    <div class="insert-image flex-1">
      {!state.show && (
        <span
          onClick={addImage}
          style={{ marginRight: 10 }}
          className="remove-links rounded-lg text-xs px-2 mx-4 inline-block align-middle leading-7 cursor-pointer h-8 bg-green-200 border border-green-300 bg-opacity-75 hover:bg-opacity-100"
        >
          افزودن عکس
        </span>
      )}
      {state.show && (
        <>
          <span class="text-sm text-green-600 ml-2 font-bold">افزودن عکس:</span>
          <input
            className="border border-green-600 rounded-lg text-xs h-8 px-1 ml-1 w-48"
            placeholder="آدرس عکس را وارد کنید"
            onChange={onURLChange}
            ref={ImgRef}
            type="text"
            value={state.url}
            onClick={onURLInputKeyDown}
          />

          <input
            class="border border-green-600 rounded-lg text-xs h-8 px-1 ml-1 w-48"
            placeholder="متن  alt را وارد کنید"
            type=""
            value={state.alt}
            onChange={onAltChange}
          />
          <button
            onClick={confirmMedia}
            class="submitButton w-8 h-8 font-bold align-top"
          >
            +
          </button>
        </>
      )}
    </div>
  );
}
