import React from 'react';
import StyleButton from '../styles/StyleButton';

import { UseEditorContext, UseEditorDispatch } from '../contexts/EditorContext';
import { BLOCKS } from './../styles/Blocks';
import { RichUtils } from 'draft-js';

export default function BlockControls(props) {
  const editorState = UseEditorContext();
  const dispatch = UseEditorDispatch();

  function toggleBlockType(blockType) {
    dispatch({
      type: 'CHANGE',
      editorState: RichUtils.toggleBlockType(editorState, blockType),
    });
  }

  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();
  return (
    <>
      {BLOCKS.map(type => {
        let color = '';
        switch (type.label) {
          case 'Step':
            color = 'green';
            break;
          case 'List':
            color = 'blue';
            break;
          case 'Tips':
            color = 'green2';
            break;

          default:
            break;
        }
        return (
          <StyleButton
            key={type.label}
            active={type.style === blockType}
            label={type.label}
            color={color}
            onToggle={toggleBlockType}
            style={type.style}
          />
        );
      })}
    </>
  );
}
