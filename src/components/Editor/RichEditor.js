import React from 'react';
import styled from 'styled-components';

import {
  RichUtils,
  getDefaultKeyBinding,
  Modifier,
  EditorState,
} from 'draft-js';

import createImagePlugin from 'draft-js-image-plugin';

import Editor from 'draft-js-plugins-editor';

import { stateToHTML } from 'draft-js-export-html';

import {
  BlockStyleControls,
  InlineControl,
  LinkBox,
  LinkBoxRich,
  ImageBox,
  ImageBoxRich,
  ColorsControl,
} from './features';

import {
  blockRendererFn,
  getBlockStyle,
  blockRenderForHtml,
} from './styles/Blocks';

import { ColorStyleForHtml } from './styles/Colors';

import { ColorStyles } from './styles/Colors';
import { InlineStyles, InlineStyleForHtml } from './styles/Inlines';

import {
  EditorProvider,
  UseEditorContext,
  UseEditorDispatch,
} from './contexts/EditorContext';

const imagePlugin = createImagePlugin();

export default function RichEditor(props) {
  const {
    onSubmit,
    type,
    name,
    init,
    index,
    readOnly,
    title = 'مقدمه',
  } = props;

  return (
    <EditorProvider>
      <RichComponents
        name={name}
        init={init}
        onSubmit={onSubmit}
        index={index}
        readOnly={readOnly}
        title={title}
        type={type}
      />
    </EditorProvider>
  );
}

const styleMap = {
  ...InlineStyles,
  ...ColorStyles,
};

function RichComponents(props) {
  const editorState = UseEditorContext();
  const dispatch = UseEditorDispatch();

  const { onSubmit, name, index, readOnly, title, init } = props;

  React.useEffect(() => {
    dispatch({
      type: 'CHANGE',
      editorState: init,
    });
  }, [init, dispatch]);

  const editorRef = React.useRef(null);

  function onChange(editorState) {
    dispatch({
      type: 'CHANGE',
      editorState,
    });
  }

  function EditorSubmit() {
    let options = {
      entityStyleFn: entity => {
        const entityType = entity.get('type').toLowerCase();
        if (entityType === 'link') {
          const data = entity.getData();

          return {
            element: 'a',
            attributes: {
              href: data.url,
              rel: data.rel,
              target: '_blank',
            },
            style: {},
          };
        }
        if (entityType === 'imagerich') {
          const data = entity.getData();

          return {
            element: 'img',
            attributes: {
              src: data.src,
              alt: data.alt,
            },
            style: {
              // Put styles here...
            },
          };
        }

        if (entityType === 'richlink') {
          const data = entity.getData();
          return {
            element: 'a',
            attributes: {
              href: data.url,
              rel: data.rel,
              class: 'richbox-link',
              target: '_blank',
            },
            style: {
              backgroundImage:
                'linear-gradient(to right, rgb(244, 107, 69) 0%, rgb(238, 168, 73) 51%, rgb(244, 107, 69) 100%);',
            },
          };
        }
      },
      inlineStyles: {
        ...InlineStyleForHtml,
        ...ColorStyleForHtml,
      },

      blockRenderers: blockRenderForHtml,

      blockStyleFn: block => {
        if (block.getType() === 'unstyled') {
          return {
            attributes: {
              className: 'p-4 pl-6 text-justify',
            },
            // style: {
            //   textAlign: 'justify',
            //   paddingLeft: '1.5rem',
            //   paddingRight: '1rem',
            // },
          };
        }
        // if (block.getType() === 'tip') {
        //   return {
        //     attributes: {
        //       tip: 'here',
        //     },
        //     // style: {
        //     //   textAlign: 'justify',
        //     //   paddingLeft: '1.5rem',
        //     //   paddingRight: '1rem',
        //     // },
        //   };
        // }
      },
    };

    let markup = stateToHTML(editorState.getCurrentContent(), options);

    let meHtml = markup.split('&nbsp;').join(' ');

    let html = meHtml;

    if (
      meHtml ===
      ('<p className="p-4 pl-6 text-justify"><br></p>' ||
        '<p className="p-4 pl-6 text-justify"> </p>')
    )
      html = '';

    onSubmit(name, index, html, editorState);
  }

  // methods

  function handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return true;
    }
    return false;
  }

  function mapKeyToEditorCommand(e) {
    const selection = editorState.getSelection();
    if (e.keyCode === 9 /* TAB */) {
      const newEditorState = RichUtils.onTab(e, editorState, 4 /* maxDepth */);
      if (newEditorState !== editorState) {
        onChange(newEditorState);
      }
      return;
    }

    if (selection.isCollapsed() && e.shiftKey && e.keyCode === 32) {
      const newEditorState = RichUtils.insertSoftNewline(editorState);
      if (newEditorState !== editorState) {
        onChange(newEditorState);
      }
    }
    return getDefaultKeyBinding(e);
  }

  function toggleInlineStyle(inlineStyle) {
    onChange(RichUtils.toggleInlineStyle(editorState, inlineStyle));
  }

  function editorFocus() {
    editorRef.current.focus();
  }

  function toggleColor(toggledColor) {
    const selection = editorState.getSelection();
    const nextContentState = Object.keys(ColorStyles).reduce(
      (contentState, color) => {
        return Modifier.removeInlineStyle(contentState, selection, color);
      },
      editorState.getCurrentContent()
    );

    let nextEditorState = EditorState.push(
      editorState,
      nextContentState,
      'change-inline-style'
    );

    const currentStyle = editorState.getCurrentInlineStyle();

    // Unset style override for current color.
    if (selection.isCollapsed()) {
      nextEditorState = currentStyle.reduce((state, color) => {
        return RichUtils.toggleInlineStyle(state, color);
      }, nextEditorState);
    }

    // If the color is being toggled on, apply it.
    if (!currentStyle.has(toggledColor)) {
      nextEditorState = RichUtils.toggleInlineStyle(
        nextEditorState,
        toggledColor
      );
    }

    onChange(nextEditorState);
  }

  return (
    <>
      <div className="flex">
        <div className="flex-1 bg-green-600 text-center text-white p-2 rounded-lg text-sm">
          {title}
        </div>
        <button
          className="submitButton w-24 mr-2 font-bold"
          onClick={EditorSubmit}
        >
          تایید
        </button>
      </div>

      <div className="mt-4 mb-4 flex justify-between">
        <LinkBox editorRef={editorRef} />
        <ImageBox editorRef={editorRef} />
      </div>
      <div
        className="mt-4 mb-4 flex justify-between"
        style={{ margin: '1rem' }}
      >
        rich:
        <LinkBoxRich editorRef={editorRef} />
        {/* <ImageBoxRich editorRef={editorRef} /> */}
      </div>

      <div onClick={editorFocus}>
        <div className="RichEditor-controls my-2 flex justify-between">
          <BlockStyleControls />

          <InlineControl
            editorState={editorState}
            onToggle={toggleInlineStyle}
          />
          <ColorsControl editorState={editorState} onToggle={toggleColor} />
        </div>
      </div>

      <div className="notranslate public-DraftEditor-content rounded-lg border border-green-600 h-64 p-2 outline-none focus:outline-none select-text whitespace-pre-wrap break-words overflow-x-hidden overflow-y-scroll mt-4">
        <Editor
          onChange={onChange}
          editorState={editorState}
          ref={editorRef}
          blockStyleFn={getBlockStyle}
          customStyleMap={styleMap}
          handleKeyCommand={handleKeyCommand}
          blockRendererFn={blockRendererFn}
          keyBindingFn={mapKeyToEditorCommand}
          spellCheck={true}
          readOnly={readOnly}
          decorators={customDecorators}
          plugins={[imagePlugin]}
        />
      </div>

      {/* <div style={{ textAlign: 'left' }}>
          <Butt onClick={EditorSubmit}>تایید</Butt>
        </div> */}
    </>
  );
}

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};

export const LinkA = styled.a`
  color: #3b5998;
  text-decoration: underline;
`;

const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};

export const LinkB = styled.a`
  /* color: #3b5998; */
  text-decoration: underline;
  background-image: linear-gradient(
    to right,
    rgb(244, 107, 69) 0%,
    rgb(238, 168, 73) 51%,
    rgb(244, 107, 69) 100%
  );
`;

// rich
function findLinkRichEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    console.log('object');
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'RICHLINK'
    );
  }, callback);
}

const EditorRichLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkB href={url} rel={rel}>
      {props.children}
    </LinkB>
  );
};

const customDecorators = [
  {
    strategy: findLinkEntities,
    component: EditorLink,
  },
  {
    strategy: findLinkRichEntities,
    component: EditorRichLink,
  },
];
