import React from 'react';

import { EditorState, CompositeDecorator } from 'draft-js';
import { LinkA } from '../styles/StyledComponents';
import styled from 'styled-components';

// const LinkA = styled.a`
//   color: #3b5998;
//   text-decoration: underline;
// `;

export const LinkB = styled.a`
  /* color: #3b5998; */
  text-decoration: underline;
  background-image: linear-gradient(
    to right,
    rgb(244, 107, 69) 0%,
    rgb(238, 168, 73) 51%,
    rgb(244, 107, 69) 100%
  );
`;
const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};

const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};

function findLinkRichEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    console.log('object');
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'RICHLINK'
    );
  }, callback);
}

const EditorRichLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkB href={url} rel={rel}>
      {props.children}
    </LinkB>
  );
};

const decorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: EditorLink,
  },
  {
    strategy: findLinkRichEntities,
    component: EditorRichLink,
  },
]);

export default { editorState: EditorState.createEmpty(decorator) };
