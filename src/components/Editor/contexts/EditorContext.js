import React, { createContext, useContext, useReducer } from 'react';
import initState from './editorDecorator';

const reducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE':
      return action.editorState;
    case 'ADD':
      return [...state];
    case 'REMOVE':
      return state
        .filter(item => item.id !== action.id)
        .filter(item => item.pId !== action.id);

    case 'TOGGLE':
      return state.map(item =>
        item.id === action.id ? { ...item, done: !item.done } : item
      );
    case 'RESET':
      return [];

    default:
      return state;
  }
};

export const EditorProvider = props => {
  const [editorState, dispatch] = useReducer(
    reducer,
    props.init || initState.editorState
  );

  return (
    <EditorContext.Provider value={editorState}>
      <DispatchContext.Provider value={dispatch}>
        {props.children}
      </DispatchContext.Provider>
    </EditorContext.Provider>
  );
};

const EditorContext = createContext();
const DispatchContext = createContext();

export const UseEditorContext = () => useContext(EditorContext);
export const UseEditorDispatch = () => useContext(DispatchContext);
