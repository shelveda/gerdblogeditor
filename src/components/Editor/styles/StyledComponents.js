import styled, { css } from 'styled-components';

export const BOX = styled.div`
  font-family: 'Helvetica', sans-serif;
  font-size: 14px;
  margin-bottom: 5px;
  user-select: none;
  direction: ltr;
  text-align: left;
`;

export const SpanControl = styled.span`
  color: #999;
  cursor: pointer;
  margin-right: 16px;
  padding: 2px 0;
  display: inline-block;
  ${props =>
    props.active &&
    css`
      color: #5890ff;
      font-weight: 600;
    `}
`;

export const LinkA = styled.a`
  color: #3b5998;
  text-decoration: underline;
`;
export const LinkB = styled.a`
  /* color: #3b5998; */
  text-decoration: underline;
  background-image: linear-gradient(
    to right,
    rgb(244, 107, 69) 0%,
    rgb(238, 168, 73) 51%,
    rgb(244, 107, 69) 100%
  );
`;

export const ContainerInput = styled.input`
  font-family: 'Georgia', serif;
  margin-right: 10px;
  padding: 3;
`;

export const LinkContainer = styled.div`
  margin-bottom: 10;
`;

export const IMG = styled.img`
  width: 500px;
  white-space: initial;
`;
export const IMG2 = styled.img`
  width: 100%;
  box-shadow: 0 0 20px #eee;
  width: 300px;
  white-space: initial;
`;
