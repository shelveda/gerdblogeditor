const COLORS = [
  { label: 'Red', style: 'red', color: 'rgba(255, 0, 0, 1.0)' },
  { label: 'Orange', style: 'orange', color: 'rgba(255, 127, 0, 1.0)' },
  { label: 'Yellow', style: 'yellow', color: 'rgba(180, 180, 0, 1.0)' },
  { label: 'Green', style: 'green', color: 'rgba(0, 180, 0, 1.0)' },
  { label: 'Blue', style: 'blue', color: 'rgba(0, 0, 255, 1.0)' },
  { label: 'Violet', style: 'violet', color: 'rgba(127, 0, 255, 1.0)' },
];

const ColorStyles = [...COLORS].reduce(
  (acc, color) => ({
    ...acc,
    [color.style]: { color: color.color },
  }),
  {}
);
const ColorStyleForHtml = [...COLORS].reduce(
  (acc, color) => ({
    ...acc,
    [color.style]: { style: { color: color.color } },
  }),
  {}
);

export { COLORS, ColorStyles, ColorStyleForHtml };
