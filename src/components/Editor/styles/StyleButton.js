import React from 'react';

export default function StyleButton(props) {
  const { onToggle, style, active, label, color } = props;
  function handleToggle(e) {
    e.preventDefault();
    onToggle(style);
  }

  let className =
    'btn bg-opacity-75 hover:bg-opacity-100 bg-gray-300 border border-gray-400';

  switch (color) {
    case 'green':
      className =
        'btn bg-opacity-75 hover:bg-opacity-100 bg-green-500 border border-green-600';
      break;
    case 'blue':
      className =
        'btn bg-opacity-75 hover:bg-opacity-100 bg-blue-400 border border-blue-500';
      break;

    case 'yellow':
      className =
        'btn bg-opacity-75 hover:bg-opacity-100 bg-yellow-300 border border-yellow-400';
      break;
    case 'green2':
      className =
        'btn bg-opacity-75 hover:bg-opacity-100 bg-green-300 border border-green-400';
      break;
    default:
      break;
  }

  return (
    <span active={active} onMouseDown={handleToggle} className={className}>
      {label}
    </span>
  );
}
