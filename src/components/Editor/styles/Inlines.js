const INLINE = [
  { label: 'Highlight', style: 'Highlight' },
  { label: 'Bold', style: 'BOLD' },
  { label: 'Italic', style: 'ITALIC' },
  { label: 'Underline', style: 'UNDERLINE' },
];

const InlineStyles = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    padding: 2,
  },
  Highlight: {
    backgroundColor: 'yellow',
    padding: 2,
  },
};

const InlineStyleForHtml = Object.keys(InlineStyles).reduce(
  (acc, key) => ({
    ...acc,
    [key]: { style: InlineStyles[key] },
  }),
  {}
);

export { INLINE, InlineStyles, InlineStyleForHtml };
