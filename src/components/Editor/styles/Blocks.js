import React from 'react';
import { IMG, IMG2 } from './StyledComponents';
import './styles.css';
const BLOCKS = [
  { label: 'Step', style: 'step' },
  { label: 'List', style: 'unordered-list-item' },
  { label: 'Tips', style: 'tip' },
  { label: 'H3', style: 'header-three' },
  { label: 'H4', style: 'header-four' },
  { label: 'H5', style: 'header-five' },
  { label: 'H6', style: 'header-six' },
  { label: 'CTA', style: 'cta' },
  { label: 'richSt', style: 'richStart' },
  { label: 'richEnd', style: 'richEnd' },
  // { label: 'Normal', style: 'P' },
  // { label: 'Tip Start', style: 'tipStart' },
  // { label: 'Tip End', style: 'tipEnd' },
  // { label: 'ulSecond', style: 'ulSecond' },
];

const Image = props => {
  return <IMG src={props.src} />;
};
const Image2 = props => {
  return <IMG2 src={props.src} />;
};

const Media = props => {
  const entity = props.contentState.getEntity(props.block.getEntityAt(0));
  const { src, alt } = entity.getData();

  const type = entity.getType();

  let media;
  if (type === 'audio') {
    // media = <Audio src={src} />;
  } else if (type === 'IMAGE') {
    media = <Image src={src} alt={alt} />;
  } else if (type === 'video') {
    // media = <Video src={src} />;
  } else if (type === 'IMAGERICH') {
    media = <Image2 src={src} alt={alt} />;
  }

  return media;
};

function StepManger(props) {
  // entity.getData();

  return (
    <div>
      <h6
        style={{ fontSize: '2rem', backgroundColor: '#0a4758', color: 'white' }}
      >
        step
      </h6>
      <h3>{props.block.text}</h3>
    </div>
  );
}
const Richer = pos => props => {
  // entity.getData();

  return (
    <div>
      <h6
        style={{
          fontSize: '2rem',
          backgroundColor: pos === 'start' ? 'red' : 'blue',
          color: 'white',
        }}
      >
        {pos}
      </h6>
      {/* <h3>{props.block.text}</h3> */}
    </div>
  );
};
function CtaBox(props) {
  // entity.getData();

  return (
    <div className="cta">
      <a
        href="test"
        style={{
          backgroundImage:
            'linear-gradient(to right, rgb(255, 128, 8) 0%, rgb(255, 200, 55) 51%, rgb(255, 128, 8) 100%)',
        }}
      >
        {props.block.text}
      </a>
    </div>
  );
}

function blockRendererFn(block) {
  if (block.getType() === 'atomic') {
    return {
      component: Media,
      editable: false,
    };
  }
  if (block.getType() === 'richStart') {
    return {
      component: Richer('start'),
      editable: false,
    };
  }
  if (block.getType() === 'richEnd') {
    return {
      component: Richer('end'),
      editable: false,
    };
  }
  if (block.getType() === 'step') {
    return {
      component: StepManger,
      editable: false,
    };
  }
  // if (block.getType() === 'cta') {
  //   return {
  //     component: CtaBox,
  //     editable: true,
  //   };
  // }
  return null;
}

function getBlockStyle(block) {
  switch (block.getType()) {
    case 'blockquote':
      return 'RichEditor-blockquote';
    case 'tip':
      return 'tip';
    case 'cta':
      return 'dcta';
    case 'unordered-list-item':
      return 'RichEditor-List';

    default:
      return null;
  }
}

const blockRenderForHtml = {
  step: block => {
    const me = block.getText();
    return `<h4>step</h4>${me}<st>`;
  },

  tip: block => {
    const me = block.getText();
    const toLines = me.split('\n');
    const output = toLines.reduce((acc, line, index) => {
      return `${acc}${line}<br/>`;
    }, ``);
    return `<p class="p-4 pl-6 text-justify">
    <span
      class="tip border border-green-700 bg-gray-200 text-green-700 block rounded-lg p-6 pb-4"
    >
      <span
        class="icon bg-green-700 text-yellow-200 rounded-full block border-white border-solid border-2 w-8 h-8 text-center text-xl -mt-10"
        >!</span
      >
      ${output}
    </span>
  </p>`;
  },

  cta: block => {
    const me = block.getText();
    const toLines = me.split('\n');
    const output = toLines.reduce((acc, line, index) => {
      return `${acc}${line}<br/>`;
    }, ``);

    return `<div class="cta">
        <a
          href="test"
          style="background-image: linear-gradient(to right, rgb(255, 128, 8) 0%, rgb(255, 200, 55) 51%, rgb(255, 128, 8) 100%);"
        >
          ${output}
        </a>
      </div>`;
  },

  tipStart: block => {
    // const text = block.getText();
    return `<tipstart>`;
  },
  tipEnd: block => {
    // const text = block.getText();
    return `<tipend>`;
  },
  richStart: block => {
    // const text = block.getText();
    return `<richstart>`;
  },
  richEnd: block => {
    // const text = block.getText();
    return `<richend>`;
  },
};

export { blockRendererFn, getBlockStyle, blockRenderForHtml, BLOCKS };
