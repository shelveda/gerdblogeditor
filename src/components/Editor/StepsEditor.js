import React, { useState } from 'react';
import RichEditor from './RichEditor';
import _ from 'lodash';

function StepsEditor(props) {
  const { name, onSubmit, init, index } = props;
  const [state, setState] = useState({
    values: [{ name: 'editor_steps_0', init: init }, { init: init }],
  });

  function handelSubmit(name, index, html, raw, editor) {}
  function addEditor() {
    const count = Object.keys(state).length;
  }
  function removeEditor(e) {
    const index = e.target.id.split('_')[2];
  }
  function onEditStep() {}
  const { values } = state;
  return (
    <div>
      <button onClick={addEditor}>add Step</button>
      {values.map((step, index) => {
        return (
          <div>
            <h2>step:</h2>
            <RichEditor
              key={step.name}
              type={'steps'}
              onSumbit={handelSubmit}
              EditorEdit={onEditStep}
              init={step.init}
              index={2}
              readOnly={true}
            />
            <button id={`editor_remove_${index}`} onClick={removeEditor}>
              removeStep
            </button>
          </div>
        );
      })}
    </div>
  );
}

export default StepsEditor;
