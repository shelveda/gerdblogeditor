import React, { useState } from 'react';
import styled from 'styled-components';

import useToggle from './../../hooks/UseToggle';

function Tree({ options, setId }) {
  const [text, setText] = useState('انتخاب کنید');
  const { toggle, setToggle, toggleTrue, toggleFalse } = useToggle(false);
  function setOp() {
    setToggle(!toggle);
  }
  function handleClick(Pid, id) {
    setId(id);
    const item = options
      .filter(item => item.id === Pid)[0]
      .cats.filter(child => child.id === id)[0];
    setText(item.name);
  }

  return (
    <div style={{ fontSize: '16px' }}>
      <div
        onClick={setOp}
        style={{ display: 'inline-flex', backgroundColor: 'white' }}
        value={'22222'}
      >
        {text}
      </div>
      {toggle && (
        <DropDownTree
          onChange={handleClick}
          onClose={toggleFalse}
          items={options}
        />
      )}
    </div>
  );
}

const DropDownTree = React.memo(props => {
  const { items, onChange, onClose } = props;
  return (
    <div
      style={{
        marginTop: '10px',
        backgroundColor: 'white',
        border: '1px solid black',
        width: '200px',
      }}
    >
      {items.map(item => {
        function setChild(e) {
          const id = Number(e.target.id);
          const Pid = Number(item.id);
          onChange(Pid, id);
          onClose();
        }
        return (
          <ul key={`parent_${item.id}`}>
            {item.name}
            {item.cats.map(child => (
              <li
                id={child.id}
                key={`child_${child.id}`}
                style={{
                  paddingRight: '10px',
                  borderBottom: '1px solid black',
                }}
                onClick={setChild}
              >
                {child.name}
              </li>
            ))}
          </ul>
        );
      })}
    </div>
  );
});

export default Tree;
