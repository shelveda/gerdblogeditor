import { useState } from 'react';

export default function useToggle() {
  const [toggle, setToggle] = useState(false);

  function toggleTrue() {
    setToggle(true);
  }
  function toggleFalse() {
    setToggle(false);
  }

  return { toggle, setToggle, toggleTrue, toggleFalse };
}
