import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

export default function OutsideCallback(props) {
  const { callback, Listener } = props;
  const wrapperRef = useRef(null);
  OutSideClickHandler(wrapperRef, Listener, callback);

  return <div ref={wrapperRef}>{props.children}</div>;
}

function OutSideClickHandler(ref, Listener, callback) {
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      callback && callback();
    }
  }

  useEffect(() => {
    document.addEventListener(Listener, handleClickOutside);
    return () => {
      document.removeEventListener(Listener, handleClickOutside);
    };
  });
}

OutsideCallback.propTypes = {
  children: PropTypes.element.isRequired,
};
