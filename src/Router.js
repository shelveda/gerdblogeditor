import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
// container components
import ResetApp from './utils/Reset';
import NotFound from './pages/staticPage/NotFound';
import TaskBar from './components/Layouts/TaskBar';
// import SeoManager from 'pages/Manager/Seo';
// Manager
// import CatList from './pages/Manager/content/CatList';
import PostList from './pages/Manager/posts/PostList';
import PageList from './pages/Manager/pages/PageList';
import UnpublishedPosts from './pages/Manager/UnpublishedPosts';
import MyPosts from './pages/Manager/MyPosts';
import UserManagement from './pages/Manager/UserManagement';
import CommentList from './pages/Manager/CommentList';
import EditorPage from './pages/Editor/EditorPage';
import Register from './pages/Auth/Register';
import { useLoadingContext } from './contexts/LoadingContext';
import SimpleLoader from './components/Loaders/SimpleLoader';
import { UseAuthContext } from './contexts/AuthContext';
// import Tags from './pages/Manager/content/Tags';
import BlogEditor from 'pages/blog/Blog';
import PageEditor from 'pages/pageEditor/PageManager';
import Test from 'pages/Tests/Test';
import Content from 'pages/Manager/content/Content';

import BlogList from 'pages/Manager/blogs/BlogsLIst';
// const ScrollTest = import('./pages/ScrollTest/Scroll');

const PrivateRoute = ({ component, ...options }) => {
  const user = UseAuthContext();
  const { isAdmin } = user;

  if (isAdmin) {
    return <Route {...options} component={component} />;
  }
  ResetApp();
  return <Redirect to="/" />;
};

const Router = props => {
  const { Loading } = useLoadingContext();
  const user = UseAuthContext();
  const { isAdmin } = user;
  return (
    <>
      {Loading && <SimpleLoader />}
      {isAdmin && <TaskBar />}
      {/* <section id="container" className="mt-0"> */}
      <div
        id="articles_management"
        className="tabcontent bg-white p-8 border border-green-600 rounded-b-lg rounded-tr-lg"
      >
        <Switch>
          <Route exact path="/404" component={NotFound} />
          <PrivateRoute
            exact
            path="/UserManagement"
            component={UserManagement}
          />
          <PrivateRoute exact path="/MyPosts" component={MyPosts} />
          <PrivateRoute
            exact
            path="/UnpublishedPosts"
            component={UnpublishedPosts}
          />
          {/* <PrivateRoute exact path="/seo/:loc/:id" component={SeoManager} /> */}
          <Route exact path="/" component={Register} />
          <PrivateRoute path="/editor/post/:postId" component={EditorPage} />
          <PrivateRoute path="/editor/newPost" component={EditorPage} />
          <PrivateRoute path="/page/:pageId" component={PageEditor} />
          {/* <PrivateRoute path="/pageEditor/newPost" component={PageEditor} /> */}
          <PrivateRoute exact path="/posts" component={PostList} />
          <PrivateRoute exact path="/pages" component={PageList} />
          {/* <PrivateRoute exact path="/Tags" component={Tags} /> */}
          {/* <PrivateRoute exact path="/CatList" component={CatList} /> */}
          <PrivateRoute path="/blog/:blogId" component={BlogEditor} />
          <PrivateRoute path="/blog" component={BlogEditor} />
          <PrivateRoute path="/blogs" component={BlogList} />
          {/*  */}
          <PrivateRoute exact path="/Content" component={Content} />
          <PrivateRoute exact path="/CommentList" component={CommentList} />
          <Route exact path="/test" component={Test} />
          <Redirect to="/404" />
        </Switch>
      </div>
      {/* </section> */}
    </>
  );
};

export default Router;
