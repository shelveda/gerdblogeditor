import axios from './ApiClient';
// import axios from 'axios';
export const MasterApi = async (method, link, data) => {
  method = method.toLowerCase();
  try {
    const res = await axios[method](link, data);
    return res.data;
  } catch (error) {
    return error.response;
  }
};
