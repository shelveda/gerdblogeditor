import React, { useState, useEffect } from 'react';
import PageManager from './PageManager';
import Header from 'components/Layouts/Header';
import { MasterApi } from 'api/MasterApi';
import { useLoadingDispatch } from 'contexts/LoadingContext';

function EditorPage(props) {
  const { setLoading } = useLoadingDispatch();
  const { match } = props;

  const [state, setState] = useState({
    content: null,
    loading: true,
    title: '',
    type: 'new',
    images: [],
  });

  useEffect(() => {
    async function fetchData() {
      setLoading(true);

      const { pageId } = match.params;
      if (pageId) {
        const response = (await MasterApi('GET', `/page/${pageId}`)) || {};

        // const images = await MasterApi('POST', `images/${pageId}`);

        // if (response.status === 200 && images.status === 200) {
        if (response.status === 200) {
          console.log('heree');
          setLoading(false);
          return setState(prev => ({
            ...prev,
            title: response.data.title,
            content: response.data,
            imgUrl: response.data.html.url,
            loading: false,
            type: 'edit',
            // images: images.data.image_content,
          }));
        }
      } else {
        setState(prev => ({
          ...prev,
          loading: false,
          type: 'new',
        }));
      }
      setLoading(false);
    }

    fetchData();
  }, []);

  if (state.loading) return '';

  return (
    <div>
      <Header type={state.type} title={state.title} loc={'page'} />
      <PageManager
        image
        Content={state.content}
        type={state.type}
        img={{ images: state.images, imgHeader: state.imgHeader }}
      />
    </div>
  );
}

export default EditorPage;
