import React, { useState } from 'react';
import * as R from 'ramda';
import { useCatContext } from 'contexts/ResourceContext';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import { MasterApi } from 'api/MasterApi';

export default function TagSection(props) {
  const { setLoading } = useLoadingDispatch();
  const { setTags, tags } = useCatContext();

  const { addTags, initTags = [] } = props;

  const options = tags.map(tag => ({
    value: tag.id,
    label: tag.name,
  }));

  const [tag, setTag] = useState(options[0].value);

  const selectedTags =
    options.filter(opt => {
      return initTags.includes(Number(opt.value));
    }) || [];

  function setTagsIn(Seltags) {
    // console.log('getTags -> Seltags', Seltags);
    // const tagsId = (Seltags && Seltags.map(tag => Number(tag.value))) || [];
    // addTags(tagsId);
  }

  function addTag() {
    // const selTag = e.target.value;
    const newTags = R.uniq([...initTags, Number(tag)]);
    // console.log('handleSave -> newTags', newTags);
    addTags(newTags);
  }
  const handleRemoveTag = val => () => {
    addTags(R.filter(item => !R.equals(item, val), [...initTags]));
  };

  async function addTagByInput() {
    // console.log(inTag);
    setLoading(true);
    const response = await MasterApi('POST', '/tag', {
      name: inTag.title,
      description: inTag.desc,
    });
    if (response.status === 201) {
      const allTags = await MasterApi('GET', '/tags');
      if (allTags.status === 200) {
        setTags(allTags.data);
      }
      const newTags = R.uniq([...initTags, Number(response.data.id)]);
      // console.log('handleSave -> newTags', newTags);
      addTags(newTags);
    }
    setLoading(false);
  }
  const [inTag, setinTag] = useState({
    title: '',
    desc: '',
  });

  function onInputTag(e) {
    const { name, value } = e.target;
    setinTag(prev => ({ ...prev, [name]: value }));
  }

  return (
    <>
      <div className="flex">
        <div className="flex-1 bg-green-600 text-center text-white p-2 rounded-lg text-sm">
          انتخاب برچسب
        </div>
        <button
          className="submitButton w-24 mr-2 font-bold"
          onClick={setTagsIn}
        >
          تایید
        </button>
      </div>

      <div className="mt-6">
        <select
          className="border border-green-600 rounded-lg mx-2 text-sm py-1 px-4"
          onChange={e => setTag(e.target.value)}
          value={tag}
        >
          {options.map(opt => {
            return <option value={opt.value}>{opt.label}</option>;
          })}
        </select>
        <button
          className="submitButton w-8 h-8 font-bold align-middle mx-2"
          onClick={addTag}
        >
          +
        </button>
        یا
        <input
          type="text"
          name="title"
          className="border border-green-600 rounded-lg mx-2 text-sm py-1 px-4"
          placeholder="عنوان برچسب"
          onChange={onInputTag}
          value={inTag.title}
        />
        <input
          type="text"
          name="desc"
          className="border border-green-600 rounded-lg mx-2 text-sm py-1 px-4"
          placeholder="توضیحات"
          onChange={onInputTag}
          value={inTag.desc}
        />
        <button
          className="submitButton w-8 h-8 font-bold align-middle"
          onClick={addTagByInput}
        >
          +
        </button>
      </div>

      <div id="article_selected_tags" className="mt-4 py-2">
        <div className="flex-1 bg-green-600 text-center text-white p-2 rounded-lg text-sm">
          برچسب های انتخاب شده
        </div>
        <div className="grid grid-cols-5 gap-4 pt-2">
          {selectedTags.map(tag => {
            return (
              <div>
                <button
                  className="text-xs text-center rounded-md bg-red-300 hover:bg-red-400 px-2 h-6 mt-2 ml-2 border border-red-600"
                  onClick={handleRemoveTag(tag.value)}
                >
                  حذف
                </button>
                <span className="inline-block text-sm">{tag.label}</span>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}
