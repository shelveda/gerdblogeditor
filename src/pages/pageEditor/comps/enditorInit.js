import React from 'react';
import styled from 'styled-components';
import { CompositeDecorator } from 'draft-js';

import { convertFromRaw, EditorState } from 'draft-js';

const LinkA = styled.a`
  color: #3b5998;
  text-decoration: underline;
`;

// #region for Editor start

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};
const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};
const decorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: EditorLink,
  },
]);

function createEditorState(keys, Content, type) {
  return keys.reduce((acc, key) => {
    let editor = EditorState.createEmpty(decorator);

    if (type === 'edit') {
      const { blocks = [], entityMap = [] } = Content.editor[key];
      const newBlocks = blocks.map(item => {
        if (item.text === null) return { ...item, text: ' ' };
        return item;
      });

      const NewEditorContent = { blocks: newBlocks, entityMap };
      editor = EditorState.createWithContent(
        convertFromRaw(NewEditorContent),
        decorator
      );
    }

    const html = type === 'edit' ? Content.html[key] : '';

    return {
      ...acc,
      [key]: {
        editor: editor || null,
        html: html,
      },
    };
  }, {});
}

function stepDivider(html) {
  let HtmlArray2 = html.split('<h4>step</h4>');

  const fHtml = HtmlArray2.reduce((acc, block, index) => {
    if (block === '') return '';
    const blockSplit = block.split('<st>');
    if (index === 0) {
      if (blockSplit.length === 1 && blockSplit[0] !== '')
        return `${acc}${blockSplit[0]}`;
    }
    const outPut = `<div class="step mr-4 mb-4">
    <h3 class="rounded-l-full text-green-700 text-xl shadow1 lalezar h-10 pt-1 pr-4 ml-4 mt-6 mb-3">
      ${blockSplit[0]}
      <span
        class="float-left cursor-pointer text-green-700 ml-6 text-xl"
        >-</span
      >
    </h3>
    ${blockSplit[1]}
    </div>`;
    return `${acc}${outPut}`;
  }, ``);
  return fHtml;
}

export { createEditorState, stepDivider };
