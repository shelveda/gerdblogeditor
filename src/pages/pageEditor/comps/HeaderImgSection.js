import React, { useState } from 'react';
import styled from 'styled-components';
import Swal from 'sweetalert2';

export default function HeaderImgSection(props) {
  const { images, onSubmit } = props;
  console.log('HeaderImgSection -> images', images);

  const onRadioClick = image => () => {
    onSubmit(image.image_url);
    Swal.fire({
      position: 'bottom-end',
      icon: 'success',
      title: 'تغییرات انجام شد',
      showConfirmButton: false,
      timer: 500,
    });
  };

  return (
    <div id="article_image" className="editortabcontent">
      <div className="flex">
        <div className="flex-1 bg-green-600 text-center text-white p-2 rounded-lg text-sm">
          انتخاب تصویر شاخص
        </div>
        {/* <button className="submitButton w-24 mr-2 font-bold">تایید</button> */}
      </div>
      <div className="grid grid-cols-5 gap-4 mt-6">
        {images.map(image => {
          return (
            <label className="cursor-pointer border-b border-transparent hover:border-blue-500">
              <div className="thumbnail mb-1">
                <div className="rounded-md border border-green-600">
                  <img src={`${image.image_url}`} alt="تصویر" />
                </div>
              </div>
              <input
                type="radio"
                name="radio"
                className="mr-1 align-middle"
                onClick={onRadioClick(image)}
              />
              <span className="text-sm text-blue-500">
                انتخاب به عنوان عکس شاخص
              </span>
            </label>
          );
        })}
      </div>
    </div>
  );
}
