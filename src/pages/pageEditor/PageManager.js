import React, { useState, useEffect } from 'react';
import PageManager from './PageEditor';

import { MasterApi } from '../../api/MasterApi';
import { useLoadingDispatch } from '../../contexts/LoadingContext';
import { useLocation } from 'react-router-dom';

function EditorPage(props) {
  const { setLoading } = useLoadingDispatch();
  const url = useLocation();
  const { match } = props;
  const { postId } = match.params;
  const [state, setState] = useState({
    content: null,
    loading: true,
    title: '',
    type: 'new',
    images: [],
    imgHeader: {},
  });

  useEffect(() => {
    async function fetchData() {
      setLoading(true);

      const { pageId } = match.params;
      if (pageId && url.pathname !== '/pageEditor/newPost') {
        const response = (await MasterApi('GET', `/page/${pageId}`)) || {};

        if (response.status === 200) {
          setLoading(false);
          let content = { ...response.data };

          if (content.editor.length === 0) {
            content = {
              ...content,
              editor: { page: { blocks: [], entityMap: [] } },
            };
          }
          return setState(prev => ({
            ...prev,
            title: response.data.title,
            content: content,
            imgUrl: response.data.html.url,
            loading: false,
            type: 'edit',
            // images: images.data.image_content,
          }));
        }
      } else {
        setState(prev => ({
          ...prev,
          loading: false,
          type: 'new',
        }));
      }
      setLoading(false);
    }

    fetchData();
  }, [postId, url]);

  let initID = 0;
  if (state.type === 'edit') {
    initID = state.content.id;
  }

  if (state.loading) return '';

  return (
    <div className="editor">
      <PageManager
        Content={state.content}
        type={state.type}
        img={{ images: state.images, imgHeader: state.imgHeader }}
        initID={initID}
      />
    </div>
  );
}

export default EditorPage;
