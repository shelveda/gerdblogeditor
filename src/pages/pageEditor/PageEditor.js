import React, { useState, useEffect } from 'react';
import { MasterApi } from 'api/MasterApi';
import './editor.css';
import ImageUpload from './comps/ImageUpload';
import TagSection from './comps/TagSection';
import EditorSection from './comps/EditorSection';

// import Tree from '../../components/TreeMenu/Tree';
import { convertToRaw } from 'draft-js';
import { createEditorState, stepDivider } from './comps/enditorInit';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import { useLocation } from 'react-router-dom';
import Swal from 'sweetalert2';

// key attachments for later

const keys = ['page'];
const editrosKeys = [
  {
    title: 'مشخصات صفحه',
    name: 'page',
    id: 'page_id',
  },
];

export default function EditorManager({ Content, type, img }) {
  console.log('EditorManager -> Content', Content);
  const url = useLocation();
  const { setLoading } = useLoadingDispatch();

  let initTags = [];
  // if (type === 'edit') initTags = Content.tags.map(tag => Number(tag.id));

  const newState = createEditorState(keys, Content, type);

  const [state, setState] = useState({
    title: {
      error: type === 'edit' ? false : true,
      value: type === 'edit' ? Content.title : '',
    },
    postId: type === 'edit' ? Content.id : 0,
    postPath: type === 'edit' ? Content.path : '',
    images: img.images,
    imagesId: [],
    imageActive: false,
    tags: initTags,
    ...newState,
  });

  useEffect(() => {
    if (url.pathname === '/pageEditor/newPost') {
      const newState = createEditorState(keys, {}, 'new');
      setState({
        postId: 0,
        postPath: '',
        images: [],
        imagesId: [],
        ...newState,
        title: {
          error: true,
          value: '',
        },
        // step: 4,
        tags: [],
        ...newState,
      });
    }
  }, [url]);

  const { step } = state;

  function addTags(tags) {
    setState(prev => ({ ...prev, tags: tags }));
  }

  async function SumbitPage() {
    setLoading(true);

    const editorContent = keys.reduce((acc, key) => {
      return {
        ...acc,
        [key]:
          state[key].editor &&
          convertToRaw(state[key].editor.getCurrentContent()),
      };
    }, {});

    const html = keys.reduce(
      (acc, key) => {
        return { ...acc, [key]: state[key].html };
      },
      { url: '' }
    );

    const slug = state.title.value.split(' ').join('-');

    const vals = {
      title: state.title.value,
      slug: slug,
      editor: editorContent,
      html: html,
      image_content: state.imagesId,
      tags: state.tags,
    };

    const response =
      type === 'edit'
        ? await MasterApi('PATCH', `/page/${state.postId}`, vals)
        : await MasterApi('POST', `/page`, vals);

    const { status } = response;
    if (status === 200 || status === 201 || status === 202) {
      Swal.fire({
        title: 'صفحه ویرایش شد',
        icon: 'success',
        // showConfirmButton: true,
        timer: 1000,
      });
    }
    setLoading(false);
  }

  async function handleSumbit(name, index, html, editor) {
    const stepHtml = stepDivider(html);
    const { step, init } = state;
    function stepFinder(index, step) {
      if (type === 'edit' && !init) return 10;
      if (index === step) return step + 1;
      return step;
    }

    const newStep = stepFinder(index, step);
    setState({
      ...state,
      step: newStep,
      [name]: {
        editor: editor,
        html: stepHtml,
      },
    });
    Swal.fire({
      position: 'bottom-end',
      icon: 'success',
      title: 'تغییرات انجام شد',
      showConfirmButton: false,
      timer: 500,
    }); // setTab(newStep);
    // setShow(Number(index + 1));
  }

  function setTitle(e) {
    setState({ ...state, title: { value: e.target.value, error: false } });
  }

  function addImage(data) {
    setState({
      ...state,
      images: [
        ...state.images,
        {
          ...data,
        },
      ],
      imagesId: [...state.imagesId, data.id],
    });
  }

  // create tabs from
  const editorPages = editrosKeys.reduce((acc, item, index) => {
    const { title, name } = item;

    const inProps = {
      title,
      name,
      step,
      type,
      index: index + 4,
      EditorState: state[name].editor,
      onSubmit: handleSumbit,
    };
    return [
      ...acc,
      {
        Component: EditorSection,
        PROPS: inProps,
      },
    ];
  }, []);

  const headerPages = [
    {
      Component: ImageUpload,
      PROPS: {
        addImage: addImage,
        images: state.images,
      },
    },
    {
      Component: TagSection,
      PROPS: {
        addTags: addTags,
        initTags: state.tags,
      },
    },
  ];
  const PAGES = [...headerPages, ...editorPages];

  const [tab, setTab] = useState(0);

  return (
    <>
      <button
        onClick={() => {
          console.log(state);
        }}
      >
        get
      </button>
      <TitleBar value={state.title.value} onChange={setTitle} type={type} />

      <div className="editortab flex">
        <EditorTabs setTab={setTab} step={step} />
        <div className="editor-contents flex-1 border border-green-600 rounded-lg p-4">
          <DisplayBox PAGES={PAGES} tab={tab} />
        </div>
      </div>
      <div className='className="mr-32"'>
        <button
          className="w-48 mx-auto block my-4 p-2 bg-green-600 text-white rounded-lg hover:bg-green-500"
          style={{ textAlign: 'center' }}
          onClick={SumbitPage}
        >
          ذخیره صفحه
        </button>
      </div>
    </>
  );
}

const TitleBar = props => {
  const { value, onChange, type } = props;
  let header = 'صفحه جدید';
  if (type === 'edit') header = 'ادیت';

  return (
    <>
      <div className="editor-title w-full rounded-lg">
        <h1 className="text-white text-xl text-center pt-1 pb-2">{header}</h1>
      </div>
      <div className="article-title flex my-4">
        <label
          for="article-title"
          className="text-gray-600 pt-1 font-bold w-32 inline-block text-center"
        >
          عنوان صفحه:
        </label>
        <input
          type="text"
          name="article-title"
          className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
          value={value}
          onChange={onChange}
        />
      </div>
    </>
  );
};

const EditorTabs = props => {
  const { setTab } = props;

  // const [active, setActive, activate] = useShowArray(4);

  const shower = num => () => {
    // activate(num);
    setTab(num);
  };

  React.useEffect(() => {
    // activate(0);
    setTab(0);
  }, []);

  const classNameActive =
    'editortablinks mt-1 w-32 p-2  rounded-r-lg text-left text-xs block active';

  return (
    <div className="editor-menu w-32 my-4 text-left">
      <button
        id="defaultEditorTab"
        className={classNameActive}
        onClick={shower(0)}
      >
        گالری
      </button>
      {/* <button
        id="article_image_button"
        className={ classNameActive }
        onClick={shower(1)}
        disabled={step < 1}
        style={{ backgroundColor: step < 1 ? 'gray' : '' }}
      >
        تصویر شاخص
      </button> */}

      <button
        id="article_tags_button"
        className={classNameActive}
        onClick={shower(1)}
      >
        برچسب ها
      </button>

      {editrosKeys.map((item, index) => {
        const { title, name } = item;
        return (
          <button
            key={`${title}${index}${name}`}
            className={classNameActive}
            onClick={shower(index + 2)}
          >
            {title}
          </button>
        );
      })}
    </div>
  );
};

const DisplayBox = props => {
  const { PAGES, tab } = props;
  const Page = PAGES[tab];
  const { Component, PROPS } = Page;
  return <Component {...PROPS} />;
};
