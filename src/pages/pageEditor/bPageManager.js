import React, { useState } from 'react';
import RichEditor from '../../components/Editor/RichEditor';
import useShowArray from '../../hooks/useShowArray';
import styled from 'styled-components';
import { MasterApi } from './../../api/MasterApi';

import { useCatContext } from '../../contexts/ResourceContext';
import CopyToClipboard from 'react-copy-to-clipboard';
import RSelect from 'react-select';
import makeAnimated from 'react-select/animated';

// import Tree from '../../components/TreeMenu/Tree';
import {
  convertFromRaw,
  convertToRaw,
  EditorState,
  CompositeDecorator,
} from 'draft-js';
import { useLoadingDispatch } from '../../contexts/LoadingContext';

// #region comps

const EditTitle = styled.div`
  padding: 0.5rem;
  cursor: ${props => (props.disable ? '' : 'pointer')};
  background-color: ${props => (props.disable ? '#9cbdb7' : '#17A589')};
  border-radius: 1rem;
  /* width: 100%; */
  margin: 1rem 2rem;
  p {
    font-size: 1.5rem;
    color: ${props => (props.disable ? '#e6d5d5' : '#ffffff')};
    text-align: right;
    padding-right: 1rem;
  }
  && ~ div {
    /* padding: 1rem; */
    /* padding-right: 3rem; */
    margin: 0 3rem;
  }
`;

const ShowBox = styled.div``;
const LinkA = styled.a`
  color: #3b5998;
  text-decoration: underline;
`;

const Section = styled.section`
  width: 90%;
  margin: 0 auto;
`;
const Input = styled.input`
  height: 30px;
  font-family: Parastoo;
  padding: 0 0.2rem;
  width: 70%;
  border: 1px solid #17a589;
  border-radius: 5px;
  font-family: calibri;
`;

const Select = styled.select`
  width: 150px;
  height: 30px;
  border: 1px solid #17a589;
  font-family: Parastoo;
  border-radius: 5px;
`;

const Butt = styled.button`
  margin-top: 1rem;
  margin-bottom: 100px;
  font-family: Parastoo;
  cursor: pointer;
  padding: 0.5rem 2rem;
  border: 1px solid gray;
  box-shadow: 0 0 5px #999;
  border-radius: 3px;
`;

const NewButt = styled.button`
  font-family: Parastoo;
  margin-top: 1rem;
  cursor: pointer;
  padding: 0.1rem;
  border: 1px solid gray;
  box-shadow: 0 0 5px #999;
  border-radius: 3px;
  width: 100px;
`;

// #endregion comps

// #region for Editor start

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};
const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};

const decorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: EditorLink,
  },
]);

function setNewState(Content, type) {
  let editor = EditorState.createEmpty(decorator);
  if (type === 'edit') {
    const { blocks = [], entityMap = [] } = Content.editor;
    const newBlocks = blocks.map(item => {
      if (item.text === null) return { ...item, text: ' ' };
      return item;
    });

    const NewEditorContent = { blocks: newBlocks, entityMap };
    editor = EditorState.createWithContent(
      convertFromRaw(NewEditorContent),
      decorator
    );
  }
  const html = type === 'edit' ? Content.html : '';
  return {
    editor: editor || null,
    html: html,
  };
}

// //#endregion

export default function EditorManager({ Content, type, img }) {
  const { setLoading } = useLoadingDispatch();
  let initTags = [];

  // if (type === 'edit') initTags = Content.tags.map(tag => Number(tag.id));

  const newState = setNewState(Content, type);

  const [show, setShow] = useShowArray(2);

  const [state, setState] = useState({
    title: {
      error: type === 'edit' ? false : true,
      value: type === 'edit' ? Content.title : '',
    },
    postId: type === 'edit' ? Content.id : 0,
    postPath: type === 'edit' ? Content.path : '',
    images: img.images,
    imagesId: [],
    imageActive: false,
    tags: initTags,
    ...newState,
  });

  function addTags(tags) {
    setState(prev => ({ ...prev, tags: tags }));
  }

  function showing(e) {
    const id = Number(e.target.id.split('_')[1]);
    setShow(id);
  }

  function saveTitle(id) {
    const { title } = state;
    if (title.value !== '') {
      setState({ ...state, step: 1, catId: id });
      setShow(1);
    } else {
      setState({ ...state, title: { value: '', error: true } });
    }
  }

  async function SumbitPage() {
    setLoading(true);
    const content = convertToRaw(state.editor.getCurrentContent());
    const html = state.html;
    const slug = state.title.value.split(' ').join('-');

    const vals = {
      title: state.title.value,
      slug: slug,
      editor: content,
      html: html,
      image_content: state.imagesId,
      // tags: state.tags,
    };

    const response =
      type === 'edit'
        ? await MasterApi('PATCH', `/page/${state.postId}`, vals)
        : await MasterApi('POST', `/page`, vals);
    console.log('SumbitPage -> response', response);

    setLoading(false);
  }

  async function handleSumbit(name, index, html, editor) {
    let HtmlArray2 = html.split('<h4>step</h4>');

    const fHtml = HtmlArray2.reduce((acc, block, index) => {
      if (block === '') return null;
      const blockSplit = block.split('<st>');
      if (index === 0) {
        if (blockSplit.length === 1 && blockSplit[0] !== '')
          return `${acc}${blockSplit[0]}`;
      }
      const outPut = `<div class="step mr-4 mb-4">
      <h3
        class="rounded-l-full text-green-700 text-lg lalezar bg-yellow-300 h-10 pt-1 pr-4 ml-4 mt-6 mb-3"
      >
        ${blockSplit[0]}
        <span
          class="float-left cursor-pointer text-green-700 ml-6 text-xl"
          >-</span
        >
      </h3>
      ${blockSplit[1]}
      </div>`;
      return `${acc}${outPut}`;
    }, ``);

    setState({
      ...state,
      editor: editor,
      html: fHtml,
    });
    setShow(3);
  }

  function setTitle(e) {
    setState({ ...state, title: { value: e.target.value, error: false } });
  }

  function addImage(data) {
    setState({
      ...state,
      images: [
        ...state.images,
        {
          ...data,
        },
      ],
      imagesId: [...state.imagesId, data.id],
    });
  }

  return (
    <>
      {/* <button onClick={() => console.log(state)}>show state</button> */}
      <ImageUpload
        active={state.imageActive}
        addImage={addImage}
        images={state.images}
      />

      <TitleSection
        error={state.title.error}
        index={0}
        active={show[0]}
        value={state.title.value}
        onShow={showing}
        onChange={setTitle}
        onSave={saveTitle}
        ParentId={state.parentId}
        CatId={state.catId}
      />

      <NoStepEditorSection
        title={'مشخصات صفحه '}
        name={`page`}
        index={1}
        EditorState={state.editor}
        onSubmit={handleSumbit}
        onShow={showing}
      />
      <TagSection addTags={addTags} initTags={state.tags} />

      <div style={{ textAlign: 'center' }}>
        <Butt onClick={SumbitPage}>ذخیره مقاله</Butt>
      </div>
    </>
  );
}

function NoStepEditorSection(props) {
  const { name, title, index, EditorState, onShow, onSubmit } = props;

  const [active, setActive] = useState(false);
  const toggle = () => setActive(!active);

  function handleShow(e) {
    onShow(e);
    toggle();
  }

  return (
    <Section>
      <EditTitle disable={false}>
        <p id={`section_${index}`} onClick={handleShow}>
          {title}
        </p>
      </EditTitle>

      {active && (
        <ShowBox>
          <RichEditor
            name={name}
            onSubmit={onSubmit}
            init={EditorState}
            index={index}
          />
        </ShowBox>
      )}
    </Section>
  );
}

function TagSection(props) {
  const animatedComponents = makeAnimated();
  const { addTags, initTags = [] } = props;
  const { tags } = useCatContext();

  const options = tags.map(tag => ({
    value: tag.id,
    label: tag.name,
  }));

  const defOptions = options.filter(opt => {
    if (initTags.includes(Number(opt.value))) return opt;

    return null;
  });
  const [active, setActive] = useState(false);

  function getTags(Seltags) {
    const tagsId = Seltags.map(tag => Number(tag.value));
    addTags(tagsId);
  }

  function toggle() {
    setActive(!active);
  }

  return (
    <Section>
      <EditTitle disable={false} onClick={toggle}>
        <p>افزودن تگ</p>
      </EditTitle>
      {active && (
        <div>
          <div style={{ display: 'inline' }}>
            <RSelect
              closeMenuOnSelect={false}
              components={animatedComponents}
              defaultValue={defOptions}
              isMulti
              options={options}
              onChange={getTags}
            />
          </div>
        </div>
      )}
    </Section>
  );
}

function TitleSection(props) {
  const { error, index, active, value, onShow, onChange } = props;

  return (
    <Section>
      <EditTitle disable={false}>
        <p id={`section_${index}`} onClick={onShow}>
          عنوان صفحه
        </p>
      </EditTitle>
      {active && (
        <div>
          <div>
            <Input type="text" value={value} onChange={onChange} />
            {error && <span> انتخاب عنوان ضروری است</span>}
          </div>
        </div>
      )}
    </Section>
  );
}

function ImageUpload(props) {
  const { setLoading } = useLoadingDispatch();
  const inputRef = React.useRef(null);
  const { active, images, addImage } = props;

  const [state, setState] = useState({
    file: '',
    imagePreviewUrl: '',
    active: active || false,
  });

  async function _handleSubmit(e) {
    e.preventDefault();

    setLoading(true);
    const response = await MasterApi('POST', '/image', {
      images: state.imagePreviewUrl,
    });
    if (response.status === 201) {
      addImage({
        id: response.data.id,
        image_url: response.data['image_url'],
      });
      setState({
        file: '',
        imagePreviewUrl: '',
        active: true,
      });
    }

    setLoading(false);
  }

  function _handleImageChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      setState(prev => ({
        ...prev,
        file: file,
        imagePreviewUrl: reader.result,
      }));
    };

    reader.readAsDataURL(file);
  }

  let { imagePreviewUrl } = state;
  let $imagePreview = null;

  if (imagePreviewUrl) {
    $imagePreview = (
      <div style={{ margin: ' 10px auto', textAlign: 'center' }}>
        <img
          style={{ maxHeight: '400px' }}
          src={imagePreviewUrl}
          alt={'نمایش عکس'}
        />
      </div>
    );
  } else {
    // $imagePreview = <div className="previewText"></div>;
  }
  function show() {
    setState({ ...state, active: !state.active });
  }

  return (
    <Section>
      <EditTitle>
        <p id={`section_img`} onClick={show}>
          گالری تصاویر
        </p>
      </EditTitle>
      {state.active && (
        <div>
          <div>
            <div className="previewComponent">
              <form onSubmit={_handleSubmit}>
                <input
                  name={'saeed'}
                  className="fileInput"
                  type="file"
                  ref={inputRef}
                  onChange={_handleImageChange}
                />
                <Butt className="submitButton" type="submit">
                  ارسال تصویر
                </Butt>
              </form>
              <div className="imgPreview">{$imagePreview}</div>
              <br />
              <ImageBox>
                {images.map((image, index) => {
                  return (
                    <div key={`img_${index}_${image.id}`}>
                      <div>
                        <img src={`${image.image_url}`} alt="تصویر" />
                      </div>
                      <CopyToClipboard text={image.image_url}>
                        <button>Copy to clipboard </button>
                      </CopyToClipboard>
                    </div>
                  );
                })}
              </ImageBox>
            </div>
          </div>
        </div>
      )}
    </Section>
  );
}

const ImageBox = styled.div`
  border: 1px solid black;
  width: 100%;
  height: 300px;
  div {
    width: 150px;
    height: 150px;
    display: inline-block;

    img {
      width: 100%;
      height: 100%;
    }
  }
`;

function HeaderImgSection(props) {
  const { imgUrl, type, images, onSubmit } = props;

  const [state, setState] = useState({
    img: imgUrl,
    inputText: '',
    active: false,
    edit: type === 'edit' ? true : false,
  });

  function show() {
    setState({ ...state, active: !state.active });
  }

  let $imagePreview = null;
  if (state.img) {
    $imagePreview = (
      <div style={{ margin: ' 10px auto', textAlign: 'center' }}>
        <img
          style={{ maxHeight: '400px' }}
          src={state.img.image_url}
          alt={'نمایش عکس'}
        />
      </div>
    );
  } else {
    // $imagePreview = <div className="previewText"></div>;
  }

  function handleChange(e) {
    const value = e.target.value;
    setState({ ...state, inputText: value });
  }

  function handleRemove() {
    setState({
      ...state,
      img: { image_url: '' },
      edit: false,
    });
  }

  function setImage() {
    const url = state.inputText;
    const headerImage =
      images.filter(image => image.image_url === url)[0] || null;
    setState({ ...state, img: headerImage || {} });
  }

  function submitImg() {
    setState({ ...state, active: false });
    onSubmit(state.img);
  }

  return (
    <Section>
      <EditTitle>
        <p id={`section_img`} onClick={show}>
          تصویر شاخص
        </p>
      </EditTitle>
      {state.active && (
        <div>
          {state.edit ? (
            <button onClick={handleRemove}> تغییر تصویر</button>
          ) : (
            <div>
              <label htmlFor="inputLink"> لینک را وارد کنید</label>
              <input value={state.inputText} onChange={handleChange} />
              <button onClick={setImage}>پیش نمایش</button>
            </div>
          )}

          <div className="imgPreview">{$imagePreview}</div>
          <Butt className="submitButton" onClick={submitImg}>
            تأیید
          </Butt>
        </div>
      )}
    </Section>
  );
}
