import React, { useState } from 'react';
import { MasterApi } from '../../api/MasterApi';
import styled from 'styled-components';
import { useLoadingDispatch } from '../../contexts/LoadingContext';
import { UseUserDispatch, UseAuthContext } from '../../contexts/AuthContext';

import UserImg from './../../assets/img/user.jpg';
import { useHistory } from 'react-router-dom';
import { useCatContext } from 'contexts/ResourceContext';

// api domain : http://cafegerd.ir/api/public/api
// admin user : admin@danoma.ir
// pass : >P2LpHPqV2G.[&/-

function Register() {
  const dispatch = UseUserDispatch();
  const history = useHistory();
  const user = UseAuthContext();
  const { setTags, setCats } = useCatContext();

  async function getTagsCats() {
    // setLoading(true);
    const cats = await MasterApi('GET', 'cat');
    if (cats.status === 200) {
      setCats(cats.data);
    }

    const allTags = await MasterApi('GET', 'tags');
    if (allTags.status === 200) {
      setTags(allTags.data);
    }

    setLoading(false);
    history.push('/posts');
  }

  if (user && user.isAdmin) getTagsCats();

  const { setLoading } = useLoadingDispatch();
  const [state, setState] = useState({
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
    nameLog: '',
    passlog: '',
  });

  async function handleSubmitLogin(e) {
    setLoading(true);
    e.preventDefault();
    setState({ ...state, deactive: true });
    const response = await MasterApi('POST', '/login', {
      email: state.nameLog,
      password: state.passlog,
    });

    if (!response || response.status !== 200) {
      alert('failed');
      setLoading(false);

      return '';
    }

    dispatch({
      type: 'LOGIN',
      payload: {
        name: response.data.name,
        role: response.data.role,
        token: response.data.token,
      },
    });

    // setLoading(false);
  }

  function handleChangeLogin(e) {
    e.persist();
    const name = e.target.name;
    const value = e.target.value;
    setState({
      ...state,
      [name]: value,
    });
  }

  return (
    <RegLogWrapper>
      <h1>ورود</h1>
      <form action="" onSubmit={handleSubmitLogin}>
        <RegisterTop>
          <div className="image-box" style={{ marginTop: '0.5rem ' }}>
            <div className="img">
              <img src={UserImg} alt="img" />
            </div>
          </div>
          <div className="register-box" style={{ paddingTop: '1rem' }}>
            <div className="input-box">
              <label htmlFor="input">ایمیل</label>
              <input type="text" name="nameLog" onChange={handleChangeLogin} />
            </div>

            <div className="input-box">
              <label htmlFor="input">رمز عبور</label>
              <input
                type="password"
                name="passlog"
                onChange={handleChangeLogin}
              />
            </div>
          </div>
        </RegisterTop>

        <RegisterButton>
          <button type="submit">ورود </button>
        </RegisterButton>
      </form>
    </RegLogWrapper>
  );
}

const RegisterButton = styled.div`
  display: block;
  width: 100%;
  margin-top: 2rem;
  margin-bottom: 2rem;
  text-align: center;
  button {
    font-family: Parastoo;
    cursor: pointer;
    border-radius: 10px;
    padding: 0.7rem 8rem;
    color: white;
    background-color: #4baf00;
    font-size: 1.2rem;
  }
`;

const RegisterTop = styled.div`
  display: grid;
  grid-template-columns: 2fr 3fr;
  padding: 2rem 2rem;

  .image-box {
    text-align: left;
    margin-top: 2.5rem;
    margin-left: 5rem;
    .img {
      img {
        width: 10rem;
        border: solid 2px #c5c5c5;
        border-radius: 7px;
      }
    }
    .attach-box {
      width: 10rem;
      display: inline-block;
      text-align: center;
      margin-top: 1.5rem;
      .attach-input {
        display: none;
        color: transparent;
      }
      .attach-label {
        font-family: Parastoo;
        cursor: pointer;
        font-size: 1.8rem;
        color: #4baf00;
        .attach-button {
          height: 2.5rem;
          width: 6.5rem;
          border-radius: 10px;
          background-color: #4baf00;
          color: white;
          font-size: 0.9rem;
          padding-top: 0.4rem;
          display: inline-block;
          &::after {
            content: '';
            display: block;
            height: 2rem;
            width: 5rem;
            position: absolute;
            left: 127px;
            top: 1px;
            opacity: 0;
          }
        }
      }
      .attach-input:checked ~ .attach-label:after {
        opacity: 1;
      }
    }
  }

  .register-box {
    text-align: right;
    border-right: solid 3px #4baf00;

    .input-box {
      display: grid;
      grid-template-columns: 8rem 1fr;
      margin-right: 5rem;
      margin-top: 1rem;
      label {
        font-family: Parastoo;
        font-size: 1rem;
        color: #317200;
        padding-top: 0.5rem;
      }
      input {
        font-family: Parastoo;
        color: rgb(75, 72, 72);
        padding-right: 0.8rem;
        height: 2.6rem;
        width: 20rem;
        border: #4baf00 solid 1px;
        background-color: white;
        margin-right: 1rem;

        &:focus {
          font-family: Parastoo;
          color: rgb(75, 72, 72);
          opacity: 1; /* Firefox */
        }

        &:-ms-input-focus {
          /* Internet Explorer 10-11 */
          font-family: Parastoo;
          color: rgb(75, 72, 72);
        }

        &::-ms-input-focus {
          /* Microsoft Edge */
          font-family: Parastoo;
          color: rgb(75, 72, 72);
        }
        &::placeholder {
          font-family: Parastoo;
          color: rgb(75, 72, 72);
          opacity: 1; /* Firefox */
        }

        &:-ms-input-placeholder {
          /* Internet Explorer 10-11 */
          font-family: Parastoo;
          color: rgb(75, 72, 72);
        }

        &::-ms-input-placeholder {
          /* Microsoft Edge */
          font-family: Parastoo;
          color: rgb(75, 72, 72);
        }
      }
    }
    .space {
      margin-top: 3rem !important;
    }

    .rules {
      margin-right: 18rem;
      margin-top: 2rem;
      label {
        font-family: Parastoo;
        font-size: 1rem;
        color: #317200;
        padding-top: 0.5rem;
        margin-left: 0.5rem;

        a {
          color: #4baf00;
        }
      }
    }
  }
`;

const RegLogWrapper = styled.div`
  width: 1200px;
  margin: 0 auto;
  background: #f7f7f7;
  padding: 1rem 0rem;
  box-shadow: 0 0 20px #ccc;

  h1 {
    font-family: Parastoo;
    text-align: center;
    color: #4baf00;
    font-size: 1.6rem;
    margin-top: 2rem;
    margin-bottom: 2rem;
  }
`;

export default Register;
