import React, { useState } from 'react';
import { MasterApi } from '../../api/MasterApi';
import { Link } from 'react-router-dom';

function Login() {
  const [state, setState] = useState({ user: '', password: '' });

  function onChange(e) {
    const { value, name } = e.target;
    setState({ ...state, [name]: value });
  }
  async function handleSubmit(e) {
    e.preventDefault();
    const response = await MasterApi('POST', '/login', {
      email: '',
      password: '',
    });
    if (response.status === 200 || 201 || 202)
      localStorage.setItem('token', response.data.token);
  }

  return (
    <div
      style={{
        direction: 'ltr',
        textAlign: 'left',
        marginTop: '50px',
        padding: '2rem',
        backgroundColor: 'white',
      }}
    >
      user:
      <input type="text" name="user" />
      password
      <input type="password" name="password" />
    </div>
  );
}

export default Login;
