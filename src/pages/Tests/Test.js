import React, { useEffect, useState } from 'react';
import { interval } from 'rxjs';
import axios from 'axios';

const body = document.querySelector('body');

const counter = interval(2000);

// const me = counter.subscribe(() => {
//   console.log('object');
//   const red = Math.random() * 255;
//   const green = Math.random() * 255;
//   const blue = Math.random() * 255;

//   body.style.backgroundColor = `rab(${red},${green},${blue})`;
//   return `rab(${red},${green},${blue})`;
// });
// console.log('me', me);

export default function Test(props) {
  const [state, setState] = useState('saeed');
  const [color, setColor] = useState('red');

  useEffect(() => {
    counter.subscribe(() => {
      console.log('blue');
      const red = Math.random() * 255;
      const green = Math.random() * 255;
      const blue = Math.random() * 255;

      const color = `rgb(${red},${green},${blue})`;

      setColor(color);
      // return `rab(${red},${green},${blue})`;
    });
    async function fetchData() {}
    fetchData();
    return () => {};
  }, []);

  return (
    <div>
      <h1>Test</h1>

      {state}
      <br />
      <br />
      <br />
      <br />
      <div style={{ color }}>him</div>
    </div>
  );
}
