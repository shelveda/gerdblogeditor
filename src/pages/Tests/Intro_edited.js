import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  Image,
} from 'react-native';
import { WebView } from 'react-native-webview';
import Swiper from 'react-native-swiper';
import AutoHeightWebView from 'react-native-autoheight-webview';
import config from 'react-native-config';
import _ from 'lodash';

// import ActivityIndicator from '../../components/araz/ActivityIndicator';
import colors from '../../constants/colors';
import AppText from '../../components/araz/AppText';
import AppButton from '../../components/araz/AppButton';
import AppTopBack from '../../components/araz/AppTopBack';
import CompanyName from '../../components/araz/CompanyName';
import routes from '../../navigation/routes';

export default function Intro({ route, navigation }) {
  const { codeCompany } = route.params.code;
  // const {data} = route.params.data;
  // // const {error} = route.params.error;
  // // const {loading} = route.params.loading;
  // // const {loadCompany} = route.params.loadCompany;
  const { token } = route.params.token;
  console.log(codeCompany, token);
  const [data, setData] = useState(data);
  const [Loading, setLoading] = useState(true);

  // console.log(data);
  // const [active, setActive] = useState(true);
  // const closeActivityIndicator = async () =>
  //   await setTimeout(
  //     () =>
  //       setActive({
  //         active: false,
  //       }),
  //     1500
  //   );

  const URL_IMAGE = config.API_FILE;

  // console.log(error, loading, token);

  const hasToken = () => {
    try {
      if (token === null) {
        navigation.navigate('Main', {
          screen: routes.LOGIN,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const getCompany = async id => {
      const response = await fetch(`http://192.168.1.252/api/v1/Symbol/${id}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer 71da3862-0796-47f2-bb17-c409e68c5667',
        },
      });

      try {
        const json = await response.json().then(res => {
          setLoading(false);
          setData(res);
        });
        // console.log(data);
      } catch (error) {
        // console.log(error);
      }
    };
    // closeActivityIndicator();
    // hasToken();
    // checkData();
    getCompany(codeCompany);
    // setTimeout(() => , 2000);
  }, []);
  console.log(data);

  if (Loading) return <Text>Loading...</Text>;

  return (
    <>
      <AppTopBack />
      {/* <ActivityIndicator animating={active} size="large" /> */}
      {/* {!data ? (
        <Text>Loading...</Text>
      ) : ( */}
      <CompanyName
        fullName={data.fullName}
        name={data.name}
        logo={data.fileId}
      />
      {/* )} */}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    width: '90%',
    height: null,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 15,
    flexDirection: 'column',
    flex: 1,
  },
  slider: {
    width: '95%',
    height: 140,
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    marginHorizontal: 10,
    flexDirection: 'column',
    flex: 1,
  },
  wrapper: {
    borderRadius: 15,
    overflow: 'hidden',
    flex: 1,
  },
  slideWrapper: {},
  slide: {
    width: '100%',
    height: '100%',
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#9DD6EB',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  content: {
    marginHorizontal: 15,
    marginVertical: 5,
    marginTop: 15,
    flex: 1,
  },
  text_company: {
    color: colors.text,
    textAlign: 'justify',
  },
  wrapper_webview: {
    // width: Dimensions.get('window').width - 5,
    marginTop: 15,
    // marginHorizontal: 10,
    width: '100%',
    // height: 320,
    // flex: 0,
  },
  webview: {
    // width: 320,
    // height: 'auto',
    backgroundColor: 'yellow',
    fontFamily: 'IRANSansMobile(FaNum)_Medium',
    fontSize: 14,
  },
});
