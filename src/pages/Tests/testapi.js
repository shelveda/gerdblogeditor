const axios = require('axios');

const defaultOptions = {
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
};

let instance = axios.create(defaultOptions);

// instance.interceptors.request.use(function(config) {
//   const token = sessionStorage.getItem('token');
//   config.headers.Authorization = token ? `Bearer ${token}` : '';
//   return config;
// });

async function get() {
  try {
    const me = await axios.post('http://79.143.85.228:8080/api/login', {
      email: 'admin@test.com',
      password: 'iamadmin',
    });
    console.log('me', me);
  } catch (err) {
    console.log(err);
  }
}
get();
