const rxjs = require('rxjs');
const { Observable, of, from, fromEvent } = rxjs;

// const o = new Observable(observer => {
//   setTimeout(() => {
//     observer.next('hello');
//     observer.next('hello');
//     observer.next('hello');
//     observer.next('hello');
//     observer.next('hello');
//     observer.next('hello');
//   }, 2000);

//   setTimeout(() => {
//     observer.complete('done');
//   }, 3000);
// });

// o.subscribe({
//   next: mess => console.log(mess),
//   complete: mess => console.log(mess + 'com'),
// });

// const producer = new Observable(subscribe => {
//   subscribe.next('hwlll');
//   subscribe.next('hwlll2');
//   subscribe.complete();
//   subscribe.next('hwll3l');
// });

const observer = {
  next: console.log,
  error: err => console.log(err),
  complete: () => console.log('done'),
};

const clicks = fromEvent(window, 'click').subscribe(ev => {
  console.log('saeed');
});

const numbers = of(1, 2, 3, 4).subscribe(observer);

// producer.subscribe(observer);
