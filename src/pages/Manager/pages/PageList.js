import React, { useEffect, useState } from 'react';
import { MasterApi } from 'api/MasterApi';
import { Link, useHistory } from 'react-router-dom';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import Swal from 'sweetalert2';

import { errorAlert, warningAlert } from '../../../components/Alerts';
// import Reset from '../../utils/Reset';
import Pagination from '../Pagination';
import Modal, { useModal } from 'hooks/useDmodal';
import SeoManager from './Seo';
import * as R from 'ramda';
import createDateString from 'utils/createDateString';

function PageList(props) {
  const { setLoading } = useLoadingDispatch();

  const [state, setState] = useState({
    loading: true,
    posts: [],
    // pages: [],
    url: '',
    showPost: [],
    call: false,
  });
  const [sortType, setSortType] = useState({
    type: 'title',
    asc: false,
  });

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      const response = await MasterApi('GET', `/pages`, {});

      if (response && response.status === 200) {
        // const res2 = await MasterApi('GET', `/pages`, {});
        // if (res2.status === 200) {
        if (true) {
          // const posts = response.data.data;
          function mapper(page) {
            return {
              ...page,
              crDate: createDateString(new Date(page.created_at)),
              upDate: createDateString(new Date(page.updated_at)),
            };
          }

          const posts = R.map(mapper, response.data);
          setState({
            posts: posts,
            // pages: res2.data.data,
            loading: false,
          });
        } else {
          function closeWindow() {
            // Reset();
            // window.close();
            errorAlert(closeWindow);
          }
        }
      } else {
        function closeWindow() {
          // Reset();
          // window.close();
        }
        errorAlert(closeWindow);
      }
      setLoading(false);
    }

    fetchData();
  }, [state.call, setLoading]);

  function handleAlert(path) {
    Swal.fire({
      title: 'آیا مطمئن هستید؟',
      text: 'پاک کردن مقاله قابل بازگشت نیست!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'خیر',
      confirmButtonText: 'بله',
    }).then(result => {
      if (result.value) {
        handleDeletePost(path);
      }
    });
  }

  function handleDeletePost(path) {
    async function fetchData() {
      setLoading(true);
      const response = await MasterApi('DELETE', path);
      if (response.status === 202) {
        setState({ ...state, call: true });
      } else {
        errorAlert();
      }
      setLoading(false);
    }

    warningAlert(fetchData);
  }

  if (state.loading) return '';

  function sorterBy(prop) {
    return () => {
      const nPosts = [...state.posts];

      function sorting(arr, prop, asc) {
        const way = asc ? 'ascend' : 'descend';
        return R.sortWith([R[way](R.prop(prop))])(arr);
      }

      const { type, asc } = sortType;
      if (prop === type) {
        const newPostShow = sorting(nPosts, prop, !asc);
        setState(prev => ({
          ...prev,
          posts: newPostShow,
        }));
        setSortType(prev => ({ ...prev, asc: !asc }));
        return '';
      }

      const newPostShow = sorting(nPosts, prop, true);
      setState(prev => ({
        ...prev,
        posts: newPostShow,
      }));

      setSortType(prev => ({ ...prev, type: prop, asc: true }));
    };
  }

  return (
    <>
      <button
        onClick={() => {
          console.log(state);
        }}
      >
        state
      </button>
      <div className="text-xs text-center bg-green-500 border border-green-600 rounded-t-lg">
        <GridHeader />
        <RowHeader sorterBy={sorterBy} sort={sortType} setSort={setSortType} />
        {state.posts.map((post, index) => {
          function OnDeletePost() {
            handleDeletePost(post.path);
            handleAlert(post.path);
          }
          return (
            <Row
              key={`${post.id}_post`}
              post={post}
              OnDeletePost={OnDeletePost}
              index={index}
            />
          );
        })}
      </div>
      <Pagination />
    </>
  );
}

const GridHeader = props => {
  return (
    <div className="table-title rounded-t-lg w-full relative">
      <h1 className="text-white text-xl border-b-white text-center pt-1 pb-2 rounded-t-lg">
        مدیریت صفحات
      </h1>
      <Link to="/pageEditor/newPost">
        <button
          className="tablink text-green-600 bg-white w-8 h-8 content-center text-center rounded-lg text-2xl  opacity-75 hover:opacity-100"
          title="مقاله جدید"
        >
          +
        </button>
      </Link>
    </div>
  );
};

const RowHeader = props => {
  const { sortType, sorterBy } = props;
  return (
    <div className="grid grid-cols-10 gap-1 m-1">
      <div className="heading shadow bg-gray-200 p-2">ردیف</div>
      <div
        className="heading relative cursor-pointer shadow bg-gray-200 p-2 col-span-3"
        onClick={sorterBy('title')}
      >
        <span className="sortable">عنوان صفحه</span>
      </div>

      <div
        className="heading relative text-right active cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('crDate')}
      >
        <span className="sortable date">تاریخ انتشار</span>
      </div>
      <div
        className="heading relative text-right cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('upDate')}
      >
        <span className="sortable date">آخرین ویرایش</span>
      </div>
      <div className="heading shadow bg-gray-200 p-2 col-span-2">اطلاعات</div>
      <div className="heading shadow bg-gray-200 p-2 col-span-2">عملیات</div>
    </div>
  );
};

const Row = props => {
  const { post, OnDeletePost, index } = props;

  const { isShowing, toggle } = useModal();
  const { title, crDate, upDate, id } = post;

  return (
    <>
      <div className="grid grid-cols-10 gap-1 m-1 py-2 bg-white hover:shadow">
        <div className="index col-1">{index + 1}</div>
        <div className="title col-2 col-span-3">
          {/* <Link to={`/${path}`} className="text-blue-400 hover:text-black"> */}
          {title}
          {/* </Link> */}
        </div>
        <div className="creation-date col-5">{crDate}</div>
        <div className="last-edit-date col-6">{upDate}</div>
        <div className="statistics col-7 col-span-2">
          <div className="flex pr-3">
            <div className="comment-icon"></div>
            <div className="w-8 text-right comment-count text-yellow-500">
              555
            </div>
            <div className="like-icon"></div>
            <div className="w-8 text-right like-count text-pink-500">555</div>
            <div className="view-icon"></div>
            <div className="w-8 text-right view-count text-blue-400">555</div>
          </div>
        </div>

        <div className="actions col-8 col-span-2">
          <div className="flex justify-center">
            {/* <!-- if published, show this--> */}
            <button
              className="unpublish rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="تعلیق"
            ></button>

            <Link to={`/page/${id}`}>
              <button
                className="edit rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
                title="ویرایش"
              ></button>
            </Link>
            <button
              className="seo rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="سئو"
              onClick={toggle}
            ></button>
            <button
              className="note rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="یادداشت ها"
            ></button>
            <button
              className="delete rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="حذف"
              onClick={OnDeletePost}
            ></button>
          </div>
        </div>
      </div>
      <Modal
        size="xs"
        toggle={toggle}
        modalTitle={` تنظیمات سئو ${post.title}`}
        isShowing={isShowing}
      >
        <SeoManager post={post} loc={'page'} />
      </Modal>
    </>
  );
};

export default PageList;
