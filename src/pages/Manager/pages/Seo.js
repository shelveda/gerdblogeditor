import React, { useEffect, useState } from 'react';
import { MasterApi } from 'api/MasterApi';
import styled from 'styled-components';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import { useHistory } from 'react-router-dom';

// #region components

const Select = styled.select`
  width: 150px;
  height: 30px;
  border: 1px solid #17a589;
  font-family: Parastoo;
  border-radius: 5px;
`;

export default function Seo(props) {
  const { post, loc } = props;
  const [state, setState] = useState({
    title: '',
    description: '',
    redirect: '',
    canonical: '',
    robot: 'index, follow',
    // data: {
    //   title: '',
    // },
  });
  // const params = useParams();
  const history = useHistory();
  const { setLoading } = useLoadingDispatch();

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      // const postVals = await MasterApi('GET', `/${loc}/${post.id}/simplified`);
      const pageSeo = await MasterApi('GET', `/${loc}/${post.id}/seo`);

      if (pageSeo.status === 200) {
        const { canonical, description, redirect, title, robot } =
          pageSeo.data || {};
        setState({
          canonical: canonical || '',
          description: description || '',
          title: title || '',
          redirect: redirect || '',
          robot: robot || 'google',
        });
      }
      if (pageSeo.status === 404) {
        history.push('/posts');
      }
      setLoading(false);
    }
    fetchData();
  }, []);

  function onInputChange(e) {
    const value = e.target.value;
    const name = e.target.name;
    setState(prev => ({ ...prev, [name]: value }));
  }

  async function onSubmit() {
    setLoading(true);
    const { title, description, redirect, canonical, robot } = state;

    const vals = {
      title,
      description,
      redirect,
      canonical,
      robot,
    };

    const response = await MasterApi('PATCH', `/${loc}/${post.id}/seo`, vals);

    setLoading(false);
  }
  // const loc = {
  //   page: 'صفحه',
  //   post: 'پست',
  // };

  // const translateLoc = loc[params.loc];

  return (
    <>
      <div style={{ marginTop: '10px' }}>
        <div style={{ paddingTop: '10px', marginTop: '5x' }} className="mx-10">
          <div className="mt-2">
            <label className="mx-2">عنوان : </label>
            <input
              className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
              type="text"
              name={'title'}
              onChange={onInputChange}
              value={state.title}
            />
          </div>
          <div className="mt-2">
            <label className="mx-2">توضیحات : </label>
            <br />
            <textarea
              name="description"
              id="des_seo"
              cols="30"
              rows="10"
              onChange={onInputChange}
              value={state.description}
              style={{ border: '1px solid black' }}
              className="mt-2"
            ></textarea>
          </div>
          <div className="mt-2">
            <label className="mx-2">Redirect </label>
            <input
              type="text"
              name={'redirect'}
              onChange={onInputChange}
              value={state.redirect}
              className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
            />
          </div>
          <div className="mt-2">
            <label className="mx-2">canonical</label>
            <input
              type="text"
              name={'canonical'}
              onChange={onInputChange}
              value={state.canonical}
              className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
            />
          </div>
          <div className="mt-2">
            <label className="mx-2">robots:</label>
            <Select
              name="robot"
              id="selectBots"
              value={state.robot}
              onChange={onInputChange}
            >
              <option value="index, follow">index follow </option>
              <option value="index, nofollow">index noFollow</option>
              <option value="noindex, follow">noIndex follow</option>
              <option value="noindex, nofollow">noIndex noFollow</option>
            </Select>
          </div>
        </div>
      </div>
      <button
        className="w-48 mx-auto block my-4 p-2 bg-green-600 text-white rounded-lg hover:bg-green-500"
        onClick={onSubmit}
      >
        save
      </button>
    </>
  );
}
