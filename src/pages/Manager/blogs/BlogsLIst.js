import React, { useEffect, useState } from 'react';
import { MasterApi } from 'api/MasterApi';
import { Link } from 'react-router-dom';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import Swal from 'sweetalert2';
import { useCatContext } from 'contexts/ResourceContext';

import { errorAlert, warningAlert } from '../../../components/Alerts';
// import Reset from '../../utils/Reset';
import Pagination from '../Pagination';
import Modal, { useModal } from 'hooks/useDmodal';
import SeoManager from './Seo';
import * as R from 'ramda';
import createDateString from 'utils/createDateString';

function BlogList(props) {
  const { setLoading } = useLoadingDispatch();
  const { cats } = useCatContext();
  const [state, setState] = useState({
    loading: true,
    posts: [],
    // pages: [],
    unpublishedPosts: [],
    url: '',
    showPost: [],
  });
  const [sortType, setSortType] = useState({
    type: 'crDate',
    asc: false,
  });
  const [sortTypeUn, setSortTypeUn] = useState({
    type: 'crDate',
    asc: false,
  });

  async function init() {
    setLoading(true);
    const response = await MasterApi('GET', `/blog/posts`, {});

    if (response && response.status === 200) {
      // if (res2.status === 200) {
      if (true) {
        // const posts = response.data.data;
        function mapper(post) {
          const catId = Number(post.cat_id);
          const catInsid = cats[2] ? cats[2] : [];
          const dCat = R.find(R.propEq('id', catId))(catInsid) || {
            id: 1,
            name: 'دسته بندی نشده',
            parent_cat: [
              {
                id: 1,
                name: 'دسته بندی نشده',
              },
            ],
          };
          const dParent = dCat.parent_cat[0];

          return {
            ...post,
            dCat,
            dParent,
            cName: dCat.name,
            pName: dParent.name,
            crDate: createDateString(new Date(post.created_at)),
            upDate: createDateString(new Date(post.updated_at)),
          };
        }

        const Unposts = R.filter(
          post => !post.published_at,
          R.map(mapper, response.data.data)
        );
        const posts = R.filter(
          post => post.published_at,
          R.map(mapper, response.data.data)
        );

        setState({
          posts: posts,
          call: false,
          // pages: res2.data.data,
          unpublishedPosts: Unposts,
          loading: false,
        });
      } else {
        function closeWindow() {
          // Reset();
          // window.close();
        }
        errorAlert(closeWindow);
      }
    } else {
      function closeWindow() {
        // Reset();
        // window.close();
      }
      errorAlert(closeWindow);
    }
    setLoading(false);
  }

  useEffect(() => {
    init();
  }, []);

  function handleAlert(path) {
    Swal.fire({
      title: 'آیا مطمئن هستید؟',
      text: 'پاک کردن مقاله قابل بازگشت نیست!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'خیر',
      confirmButtonText: 'بله',
    }).then(result => {
      if (result.value) {
        handleDeletePost(path);
      }
    });
  }

  function handleDeletePost(path) {
    async function fetchData() {
      setLoading(true);
      const response = await MasterApi('DELETE', path);
      if (response.status === 202) {
        setState({ ...state, call: true });
        return init();
      } else {
        errorAlert();
      }
      setLoading(false);
    }

    warningAlert(fetchData);
  }
  const publishPost = post => publish => async () => {
    setLoading(true);
    const response = await MasterApi('PATCH', `/blog/post/${post.id}`, {
      published_at: publish ? true : null,
    });
    if (response.status === 202) {
      try {
        await init();
      } catch (error) {}
    } else {
      Swal.fire({
        title: '',
        text: 'مشکلی رخ داده است ',
        icon: 'error',
        timer: 1000,
      });
    }
    setLoading(false);
  };

  if (state.loading) return '';

  function sorterBy(posts, publish, prop) {
    return () => {
      const nPosts = [...posts];

      function sorting(arr, prop, asc) {
        const way = asc ? 'ascend' : 'descend';
        return R.sortWith([R[way](R.prop(prop))])(arr);
      }

      let { type, asc } = publish ? sortType : sortTypeUn;

      if (prop === type) {
        const newPostShow = sorting(nPosts, prop, !asc);
        if (publish) {
          setState(prev => ({
            ...prev,
            posts: newPostShow,
          }));
          setSortType(prev => ({ ...prev, asc: !asc }));
        } else {
          setState(prev => ({
            ...prev,
            unpublishedPosts: newPostShow,
          }));
          setSortTypeUn(prev => ({ ...prev, asc: !asc }));
        }

        return '';
      }

      const newPostShow = sorting(nPosts, prop, true);
      if (publish) {
        setState(prev => ({
          ...prev,
          posts: newPostShow,
        }));

        setSortType(prev => ({ ...prev, type: prop, asc: true }));
      } else {
        setState(prev => ({
          ...prev,
          unpublishedPosts: newPostShow,
        }));

        setSortTypeUn(prev => ({ ...prev, type: prop, asc: true }));
      }
    };
  }

  const cuSorter = R.curryN(3, sorterBy);

  return (
    <>
      {/* <button
        onClick={() => {
          console.log(state);
        }}
      >
        state
      </button> */}
      <div className="text-xs text-center bg-green-500 border border-green-600 rounded-t-lg">
        <GridHeader title="مدیریت بلاگ منتشر نشده" />
        <RowHeader
          sorterBy={cuSorter(state.unpublishedPosts, false)}
          sort={sortTypeUn}
        />
        {state.unpublishedPosts.map((post, index) => {
          const catInsid = cats[2] ? cats[2] : [];

          const cat = catInsid.find(
            cat => Number(cat.id) === Number(post.cat_id)
          );

          function OnDeletePost() {
            handleDeletePost(post.path);
            handleAlert(post.path);
          }
          return (
            <Row
              iconType={'play'}
              key={`${post.id}_post`}
              cat={cat}
              post={post}
              OnDeletePost={OnDeletePost}
              index={index}
              publishPost={publishPost(post)(true)}
            />
          );
        })}
      </div>
      <Pagination />
      <div className="py-6">
        <div className="text-xs text-center bg-green-500 border border-green-600 rounded-t-lg">
          <GridHeader title={'مدیریت بلاگ منتشر شده'} />
          <RowHeader sorterBy={cuSorter(state.posts, true)} sort={sortType} />
          {state.posts.map((post, index) => {
            const catInsid = cats[2] ? cats[2] : [];

            const cat = catInsid.find(
              cat => Number(cat.id) === Number(post.cat_id)
            );

            function OnDeletePost() {
              handleDeletePost(post.path);
              handleAlert(post.path);
            }
            return (
              <Row
                key={`${post.id}_post`}
                iconType={'pause'}
                cat={cat}
                post={post}
                OnDeletePost={OnDeletePost}
                index={index}
                publishPost={publishPost(post)(false)}
              />
            );
          })}
        </div>

        <Pagination />
      </div>
    </>
  );
}

const GridHeader = props => {
  const { title } = props;
  return (
    <div className="table-title rounded-t-lg w-full relative">
      <h1 className="text-white text-xl border-b-white text-center pt-1 pb-2 rounded-t-lg">
        {title}
      </h1>
      <Link to="/editor/newPost">
        <button
          className="tablink text-green-600 bg-white w-8 h-8 content-center text-center rounded-lg text-2xl  opacity-75 hover:opacity-100"
          title="مقاله جدید"
        >
          +
        </button>
      </Link>
      <div id="category_filter">
        <div className="custom-select w-32 h-8 rounded-lg">
          <select>
            <option value="0">همه رسته ها</option>
            <option value="1">اداری</option>
            <option value="2">آشپزی</option>
          </select>
        </div>
      </div>
      <div id="subcat_filter">
        <div className="custom-select w-32 h-8 rounded-lg">
          <select>
            <option value="0">همه حوزه ها</option>
            <option value="1">اداری</option>
            <option value="2">آشپزی</option>
          </select>
        </div>
      </div>
    </div>
  );
};

const sortClassNameCreator = R.curryN(2, (sort, prop) => {
  const { type, asc } = sort;
  if (prop !== type) return '';

  if (!asc) return 'sortable date';
  return 'sortable date dec';
});

const RowHeader = props => {
  const { sort, sorterBy } = props;
  const sortClass = sortClassNameCreator(sort);

  return (
    <div className="grid grid-cols-12 gap-1 m-1">
      <div className="heading shadow bg-gray-200 p-2">ردیف</div>
      <div
        className="heading relative cursor-pointer shadow bg-gray-200 p-2 col-span-3"
        onClick={sorterBy('title')}
      >
        <span className={sortClass('title')}>عنوان مقاله</span>
      </div>
      <div
        className="heading relative cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('pName')}
      >
        <span className={sortClass('pName')}>رسته</span>
      </div>
      <div
        className="heading relative cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('cName')}
      >
        <span className={sortClass('cName')}>حوزه</span>
      </div>
      <div
        className="heading relative text-right cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('crDate')}
      >
        <span className={sortClass('crDate')}>تاریخ انتشار</span>
      </div>
      <div
        className="heading relative text-right cursor-pointer shadow bg-gray-200 p-2"
        onClick={sorterBy('upDate')}
      >
        <span className={sortClass('upDate')}>آخرین ویرایش</span>
      </div>
      <div className="heading shadow bg-gray-200 p-2 col-span-2">اطلاعات</div>
      <div className="heading shadow bg-gray-200 p-2 col-span-2">عملیات</div>
    </div>
  );
};

const Row = props => {
  const { post, OnDeletePost, index, publishPost, iconType } = props;
  const { isShowing, toggle } = useModal();
  const { id, title, path, dCat, dParent, crDate, upDate } = post;
  const iconClassName =
    iconType === 'play'
      ? 'publish rounded-md px-2 mx-1 text-green-500 opacity-50 hover:opacity-100 '
      : 'unpublish rounded-md px-2 mx-1 text-green-500 opacity-50 hover:opacity-100 ';
  return (
    <>
      <div className="grid grid-cols-12 gap-1 m-1 py-2 bg-white hover:shadow">
        <div className="index col-1">{index + 1}</div>

        <div className="title col-2 col-span-3">
          <Link
            to={`/editor${path}`}
            className="text-blue-400 hover:text-black"
          >
            {title}
          </Link>
        </div>

        <div className="category col-3">{dParent.name}</div>
        <div className="sub-category col-4">{dCat.name}</div>
        <div className="creation-date col-5">{crDate}</div>
        <div className="last-edit-date col-6">{upDate}</div>

        <div className="statistics col-7 col-span-2">
          <div className="flex pr-3">
            <div className="comment-icon"></div>
            <div className="w-8 text-right comment-count text-yellow-500">
              555
            </div>
            <div className="like-icon"></div>
            <div className="w-8 text-right like-count text-pink-500">555</div>
            <div className="view-icon"></div>
            <div className="w-8 text-right view-count text-blue-400">555</div>
          </div>
        </div>

        <div className="actions col-8 col-span-2">
          <div className="flex justify-center">
            {/* <!-- if published, show this--> */}
            <button
              className={iconClassName}
              title="تعلیق"
              onClick={publishPost}
            ></button>

            <Link to={`/blog/${id}`}>
              <button
                className="edit rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
                title="ویرایش"
              ></button>
            </Link>
            <button
              className="seo rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="سئو"
              onClick={toggle}
            ></button>
            <button
              className="note rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="یادداشت ها"
            ></button>
            <button
              className="delete rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="حذف"
              onClick={OnDeletePost}
            ></button>
          </div>
        </div>
      </div>
      <Modal
        size="xs"
        toggle={toggle}
        modalTitle={` تنظیمات سئو ${post.title}`}
        isShowing={isShowing}
      >
        <SeoManager post={post} loc={'post'} />
      </Modal>
    </>
  );
};

export default BlogList;
