import React from 'react';

export default function Pagination() {
  return (
    <div className="pagination text-center w-full mt-5">
      <ul>
        <li className="inline">
          <span className="cursor-pointer rounded-full w-8 h-8 inline-block text-center leading-8">
            {'<'}
          </span>
        </li>
        <li className="inline">
          <span className="active cursor-pointer rounded-full w-8 h-8 inline-block text-center leading-8">
            1
          </span>
        </li>

        <li className="inline">
          <span className="cursor-pointer rounded-full w-8 h-8 inline-block text-center leading-8">
            {'>'}
          </span>
        </li>
      </ul>
    </div>
  );
}
