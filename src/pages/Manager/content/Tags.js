import React, { useEffect, useState } from 'react';
import { useCatContext } from 'contexts/ResourceContext';

import { MasterApi } from 'api/MasterApi';
import { useLoadingDispatch } from 'contexts/LoadingContext';

// const options = [
//   { value: 'chocolate', label: 'Chocolate' },
//   { value: 'strawberry', label: 'Strawberry' },
//   { value: 'vanilla', label: 'Vanilla' },
// ];

export default function Tags(props) {
  const [state, setState] = useState({
    tagName: '',
    desc: '',
  });
  const { setLoading } = useLoadingDispatch();
  const { setTags } = useCatContext();

  async function handleAddTags() {
    setLoading(true);
    const response = await MasterApi('POST', '/tag', {
      name: state.tagName,
      description: state.desc,
    });
    if (response.status === 201) {
      const allTags = await MasterApi('GET', '/tags');
      if (allTags.status === 200) {
        setTags(allTags.data);
      }
    }
    setLoading(false);
  }

  function onInputChange(e) {
    const value = e.target.value;
    const name = e.target.name;
    setState(prev => ({ ...prev, [name]: value }));
  }

  return (
    <div class="py-6 text-center border-b-2 border-green-600">
      <label>تعریف برچسب جدید:</label>
      <input
        name={'tagName'}
        onChange={onInputChange}
        type="text"
        value={state['tagName']}
        class="border border-green-600 mr-2 mx-2 rounded-lg text-sm py-1 px-4"
        placeholder="عنوان برچسب"
      />
      <input
        name={'desc'}
        onChange={onInputChange}
        type="text"
        value={state['desc']}
        class="border border-green-600 rounded-lg mx-2 text-sm py-1 px-4"
        placeholder="توضیحات"
      />
      <button
        class="submitButton mt-1 h-8 font-bold align-middle leading-4"
        onClick={handleAddTags}
      >
        تایید
      </button>

      <div class="py-6 mb-8 justify-center flex">
        <TagList />
      </div>
    </div>
  );
}

const TagList = React.memo(props => {
  const { tags, setTags } = useCatContext();
  const { setLoading } = useLoadingDispatch();

  const deleteTag = tag => async e => {
    setLoading(true);
    const response = await MasterApi('DELETE', `/tag/${tag.id}`);
    if (response.status === 202) {
      setTags(response.data);
    }
    setLoading(false);
  };

  return (
    <>
      {tags.map((tag, index) => {
        return (
          <span class="tag text-sm ml-2 mb-2">
            <span class="inline-block bg-green-300 px-2 rounded-r-md -ml-1">
              {tag.name}
            </span>
            <span
              onClick={deleteTag(tag)}
              class="inline-block bg-red-300 px-2 text-white rounded-l-md hover:bg-red-400 cursor-pointer"
            >
              x
            </span>
          </span>
        );
      })}
    </>
  );
});
