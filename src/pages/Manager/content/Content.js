import React from 'react';
import Tags from './Tags';
import CatList from './CatList';

export default function Content() {
  return (
    <>
      <div class="editor">
        <div class="editor-title w-full rounded-lg">
          <h1 class="text-white text-xl text-center pt-1 pb-2">
            مدیریت برچسب‌ها
          </h1>
        </div>

        <Tags />

        <div class="editor-title w-full rounded-lg">
          <h1 class="text-white text-xl text-center pt-1 pb-2">
            مدیریت رسته-حوزه
          </h1>
        </div>

        <CatList />
      </div>
    </>
  );
}
