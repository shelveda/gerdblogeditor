import React, { useEffect, useState } from 'react';
import { MasterApi } from '../../../api/MasterApi';
import { useCatContext } from '../../../contexts/ResourceContext';
import { useLoadingDispatch } from '../../../contexts/LoadingContext';
import Swal from 'sweetalert2';
import { errorAlert, warningAlert } from '../../../components/Alerts';
import OutsideCallBack from 'hooks/useOutSideCallback';
import { identical } from 'ramda';

function CatList() {
  const { setLoading } = useLoadingDispatch();
  const { cats, setCats } = useCatContext();

  const [state, setState] = useState({
    cats: [],
    parents: [],
    children: [],
    parentInput: '',
    parentSlug: '',
    parentSelect: 1,
    childInput: '',
    childSlug: '',
    type: 'new',
    editInput: '',
    editSlug: '',
    editId: 1,
  });

  useEffect(() => {
    async function fetchData() {
      const parents = cats[1] || [];
      const children = cats[2] || [];
      setState(prev => ({ ...prev, cats: cats, parents, children }));
    }
    fetchData();
  }, [cats]);

  function handleChange(e) {
    setState({ ...state, [e.target.name]: e.target.value });
  }

  const onCatEdit = cat => () => {
    const { name, slug, id } = cat;
    setState(prev => ({
      ...prev,
      editInput: name,
      editSlug: slug,
      editId: id,
      type: 'edit',
    }));
  };

  async function onCatEditApprove() {
    setLoading(true);
    const response = await MasterApi('PATCH', `/cat/${state.editId}`, {
      name: state.editInput,
      slug: state.editSlug,
    });

    if (response && response.status === 202) {
      try {
        const allCats = await MasterApi('GET', '/cat');
        setCats(allCats.data);
        sessionStorage.setItem('cats', JSON.stringify(allCats.data));

        Swal.fire({
          title: '',
          text: 'انجام شد',
          icon: 'success',
          timer: 1000,
        });
      } catch (error) {
        errorAlert();
      }
    } else {
      errorAlert();
    }

    setLoading(false);
  }

  async function addParent() {
    setLoading(true);
    const data = {
      name: state.parentInput,
      depth: 1,
      slug: state.parentSlug,
    };
    const response = await MasterApi('POST', '/cat', data);

    if (response) {
      if (response.status === 201) {
        const allCats = await MasterApi('GET', '/cat');
        setCats(allCats.data);
        sessionStorage.setItem('cats', JSON.stringify(allCats.data));
      }
    } else {
      errorAlert();
    }

    setLoading(false);
  }

  async function addChild() {
    setLoading(true);

    const parentId = Number(state.parentSelect);
    const NewCats = await MasterApi('POST', `cat`, {
      name: state.childInput,
      slug: state.childSlug ? state.childSlug : state.childInput,
      parent_cat_id: parentId,
      depth: 2,
    });

    if (NewCats.status === 201) {
      setCats(NewCats.data);
      sessionStorage.setItem('cats', JSON.stringify(NewCats.data));
    }
    setLoading(false);
  }

  function DeleteParent(id) {
    Swal.fire({
      title: 'آیا مطمئن هستید؟',
      text: 'پاک کردن رسته قابل بازگشت نیست!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'خیر',
      confirmButtonText: 'بله',
    }).then(result => {
      if (result.value) {
        onDeleteChild(id);
      }
    });
  }

  // async function onDeleteParent(id) {
  //   setLoading(true);

  //   const remainedCats = await MasterApi('DELETE', `/cat/${id}`);
  //   const id = Number(e.target.id.split('_')[1]);

  //   async function fetchData() {
  //     setLoading(true);
  //     const remainedCats = await MasterApi('DELETE', `/cat/${id}`);
  //     if (remainedCats.status === 202) {
  //       setCats(remainedCats.data);
  //       sessionStorage.setItem('cats', JSON.stringify(remainedCats.data));
  //     } else {
  //       errorAlert();
  //     }
  //     setLoading(false);
  //   }

  //   warningAlert(fetchData);
  // }

  function DeleteChild(id) {
    Swal.fire({
      title: 'آیا مطمئن هستید؟',
      text: 'پاک کردن حوزه قابل بازگشت نیست!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'خیر',
      confirmButtonText: 'بله',
    }).then(result => {
      if (result.value) {
        onDeleteChild(id);
      }
    });
  }

  async function onDeleteChild(id) {
    setLoading(true);
    const remainedCats = await MasterApi('DELETE', `/cat/${id}`);
    setCats(remainedCats.data);
    sessionStorage.setItem('cats', JSON.stringify(remainedCats.data));
    setLoading(false);
  }

  return (
    // <div style={{ padding: '0 2rem', width: '80%', margin: '0 10%' }}>
    <>
      <div class="py-6 text-center border-b-2 border-green-600">
        <label>تعریف رستۀ جدید:</label>
        <input
          name={'parentInput'}
          onChange={handleChange}
          type="text"
          value={state['parentInput']}
          class="border border-green-600 mr-2 rounded-lg text-sm py-1 px-4 "
          placeholder="عنوان رسته"
        />
        <input
          name={'parentSlug'}
          onChange={handleChange}
          type="text"
          value={state['parentSlug']}
          class="border border-green-600 mr-2 rounded-lg text-sm py-1 px-4 mx-2"
          placeholder="اسلاگ"
        />
        <button
          class="submitButton mt-1 h-8 font-bold align-middle leading-4"
          onClick={addParent}
        >
          تایید
        </button>
      </div>
      <div class="py-6 text-center border-b-2 border-green-600">
        <label>تعریف حوزۀ جدید:</label>
        <select
          class="border border-green-600 rounded-lg mr-2 text-sm pb-1 px-4 relative"
          onChange={handleChange}
          name={'parentSelect'}
          value={state.parentSelect}
        >
          <option key={`$unSelect_${0}`} value={0}>
            انتخاب نشده
          </option>
          {state.parents.map((cat, index) => {
            if (cat.id !== 1)
              return (
                <option key={`${cat.id}_${index}`} value={cat.id}>
                  {cat.name}
                </option>
              );
            return '';
          })}
        </select>

        <input
          type="text"
          class="border border-green-600 mx-2 rounded-lg text-sm py-1 px-4"
          value={state.childInput}
          name={'childInput'}
          onChange={handleChange}
          placeholder="عنوان حوزه"
        />
        <input
          type="text"
          class="border border-green-600 mx-2 rounded-lg text-sm py-1 px-4"
          value={state.childSlug}
          name={'childSlug'}
          onChange={handleChange}
          placeholder="اسلاگ"
        />
        <button
          onClick={addChild}
          class="submitButton mt-1 h-8 font-bold align-middle leading-4"
        >
          تایید
        </button>
      </div>
      <div class="">
        {state.type === 'edit' && (
          <div class="py-6 text-center border-b-2 border-green-600">
            <label>ویرایش:</label>
            <input
              name={'editInput'}
              onChange={handleChange}
              type="text"
              value={state['editInput']}
              className="border border-green-600 mr-2 rounded-lg text-sm py-1 px-4 "
              placeholder="عنوان رسته یا حوزه"
            />
            <input
              name={'editSlug'}
              onChange={handleChange}
              type="text"
              value={state['editSlug']}
              class="border border-green-600 mr-2 rounded-lg text-sm py-1 px-4 mx-2"
              placeholder="اسلاگ"
            />
            <button
              class="submitButton mt-1 h-8 font-bold align-middle leading-4"
              onClick={onCatEditApprove}
            >
              تایید
            </button>
            <button
              class=" bg-red-300 submitButton mt-1 h-8 font-bold mx-2 align-middle leading-4 "
              onClick={() => {
                setState(prev => ({ ...prev, type: 'new' }));
              }}
              style={{ backgroundColor: '#feb2b2' }}
            >
              لغو
            </button>
          </div>
        )}
        <div className="py-6">
          <ul>
            {state.parents.map((parent, index) => {
              const curChildren =
                state.children.filter(child => {
                  let inParent = child.parent_cat[0] || { id: 1 };
                  return parent.id === inParent.id;
                }) || [];

              const inProp = {
                parent,
                index,
                curChildren,
                DeleteParent,
                DeleteChild,
                onCatEdit,
              };

              return <CatBox {...inProp} />;
            })}
          </ul>
        </div>
      </div>
    </>
  );
}

function CatBox(props) {
  const {
    parent,
    curChildren,
    index,
    DeleteParent,
    DeleteChild,
    onCatEdit,
  } = props;
  const [show, toggle, setShow] = useShower();

  // const [show, setShow] = useState(false);

  return (
    <li
      class="cat mb-4 bg-green-400 p-2 rounded-md"
      key={`${parent.id}_${index}`}
      value={parent.id}
      style={{ position: 'relative' }}
    >
      <span
        class="inline-block border-l-4 border-green-600 ml-4 pl-4"
        onClick={toggle}
        style={{ cursor: 'pointer' }}
      >
        {parent.name}
      </span>
      {/* <Icon className="fas fa-pencil-alt"></Icon>
    <Icon
      id={`parentcats_${parent.id}`}
      className="fas fa-trash-alt"
      onClick={DeleteParent}
    ></Icon> */}
      {show && (
        <div
          className="tooltip actions  bg-green-500 rounded-md mr-2"
          style={{
            top: '-20px',
            right: 0,
            position: 'absolute',
          }}
        >
          <OutsideCallBack callback={setShow(false)} Listener="click">
            <div class="flex justify-center">
              <button
                className="edit px-2 rounded-r-md opacity-50 hover:opacity-100 "
                title="ویرایش"
                onClick={onCatEdit(parent)}
              ></button>
              <button
                class="delete px-2 rounded-l-md opacity-50 hover:opacity-100 "
                title="حذف"
                onClick={() => DeleteParent(parent.id)}
              ></button>
            </div>
          </OutsideCallBack>
        </div>
      )}
      <ul class="inline-block text-sm">
        {curChildren.map((child, index) => {
          const inProp = {
            child,
            index,
            DeleteChild,
            onCatEdit,
          };
          return <CatIn {...inProp} />;
        })}
      </ul>
    </li>
  );
}

function CatIn(props) {
  const { child, index, DeleteChild, onCatEdit } = props;
  const [show, toggle, setShow] = useShower();

  return (
    <li
      className="sub-cat inline-block mx-2 bg-green-200 px-2 rounded-md"
      key={`cat_sub_${index}`}
      style={{ position: 'relative' }}
    >
      <span
        class="inline-block "
        onClick={toggle}
        style={{ cursor: 'pointer' }}
      >
        {child.name}
      </span>

      {show && (
        <div
          className="tooltip actions  bg-green-500 rounded-md"
          style={{
            top: '-22px',
            right: 0,
            position: 'absolute',
          }}
        >
          <OutsideCallBack callback={setShow(false)} Listener="click">
            <div class="flex justify-center">
              <button
                className="edit px-2 rounded-r-md opacity-50 hover:opacity-100 "
                title="ویرایش"
                onClick={onCatEdit(child)}
              ></button>
              <button
                class="delete px-2 rounded-l-md opacity-50 hover:opacity-100 "
                title="حذف"
                onClick={() => {
                  DeleteChild(child.id);
                }}
              ></button>
            </div>
          </OutsideCallBack>
        </div>
      )}
      {/* <Icon className="fas fa-pencil-alt"></Icon>
            <Icon
              id={`subcats_${child.id}`}
              className="fas fa-trash-alt"
              onClick={() => {
                DeleteChild(child.id);
              }}
            /> */}
    </li>
  );
}

function useShower() {
  const [show, setShow] = useState('');
  function toggle() {
    setShow(!show);
  }
  const showing = bool => () => setShow(bool);
  return [show, toggle, showing];
}

export default CatList;
