import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { MasterApi } from '../../api/MasterApi';
import * as R from 'ramda';

import { useLoadingDispatch } from '../../contexts/LoadingContext';
import Pagination from './Pagination';
import Swal from 'sweetalert2';

export default function CommentList() {
  const { setLoading } = useLoadingDispatch();
  const [state, setState] = useState({
    comments: [],
    loading: true,
  });

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      const response = await MasterApi('GET', `/comment`);
      if (response.status === 200) {
        const initComments = response.data;
        function grouper(item) {
          const { approved } = item;
          return approved ? 'approved' : 'unapproved';
        }
        const allComments = R.groupBy(grouper, initComments);
        console.log(
          '🚀 ~ file: CommentList.js ~ line 28 ~ fetchData ~ allComments',
          allComments
        );

        console.log(
          '🚀 ~ file: CommentList.js ~ line 39 ~ fetchData ~ initComments',
          initComments
        );

        setState({
          comments: allComments,
          // replies: response.data.replies || [],
          loading: false,
        });
      }
      setLoading(false);
    }
    fetchData();
  }, [setLoading]);

  const approveComment = approved => async id => {
    console.log('object');
    setLoading(true);
    const response = await MasterApi('PATCH', `/comment/${id}`, {
      approved,
    });
    if (response.status === 202) {
      await setNewPost();
    }
    setLoading(false);
  };

  function handleAlert(id) {
    Swal.fire({
      title: 'آیا مطمئن هستید؟',
      text: 'پاک کردن مقاله قابل بازگشت نیست!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'خیر',
      confirmButtonText: 'بله',
    }).then(result => {
      if (result.value) {
        deleteComment(id);
      }
    });
  }

  async function deleteComment(id) {
    setLoading(true);
    const response = await MasterApi('DELETE', `/comment/${id}`);
    if (response.status === 202) {
      await setNewPost();
    }
    setLoading(false);
  }

  async function setNewPost() {
    const newComments = await MasterApi('Get', `/comment`);
    if (newComments.status === 200) {
      const initComments = newComments.data;
      function grouper(item) {
        const { approved } = item;
        return approved ? 'approved' : 'unapproved';
      }
      const allComments = R.groupBy(grouper, initComments);
      setState({
        ...state,
        comments: allComments,
      });
    }
  }

  if (state.loading) return '';

  return (
    <>
      <GridBuilder
        title={'نظرات تأیید شده'}
        comments={state.comments.approved || []}
        deleteComment={handleAlert}
        approveComment={approveComment(false)}
        iconType={'pause'}
      />
      <Pagination />
      <br />
      <br />

      <GridBuilder
        title={'نظرات تأیید نشده'}
        comments={state.comments.unapproved || []}
        deleteComment={handleAlert}
        approveComment={approveComment(true)}
        iconType={'play'}
      />
      <Pagination />
    </>
  );
}

const GridBuilder = props => {
  const { comments, deleteComment, approveComment, title, iconType } = props;
  return (
    <div className="text-xs text-center bg-green-500 border border-green-600 rounded-t-lg">
      <GridHeader title={title} />
      <RowHeader />
      {comments.map((comment, index) => {
        // const cat = cats[2].find(cat => Number(cat.id) === post.cat_id);
        // function OnDeletePost() {
        //   handleDeletePost(post.path);
        //   handleAlert(post.path);
        // }

        return (
          <Row
            // seoToggle={toggle}
            // cat={cat}
            deleteComment={deleteComment}
            approveComment={approveComment}
            data={comment}
            iconType={iconType}

            // OnDeletePost={OnDeletePost}
          />
        );
      })}
    </div>
  );
};

const GridHeader = props => {
  const { title } = props;
  return (
    <div className="table-title rounded-t-lg w-full relative">
      <h1 className="text-white text-xl border-b-white text-center pt-1 pb-2 rounded-t-lg">
        {title}
      </h1>
    </div>
  );
};

const RowHeader = props => {
  return (
    <div className="grid grid-cols-10 gap-1 m-1">
      <div className="heading shadow bg-gray-200 p-2">ردیف</div>
      <div className="heading shadow bg-gray-200 p-2">نام کاربر</div>
      <div className="heading shadow bg-gray-200 p-2 col-span-3">متن نظر</div>
      <div className="heading shadow bg-gray-200 p-2">زمان ارسال</div>
      <div className="heading shadow bg-gray-200 p-2 col-span-3">
        عنوان مقاله
      </div>
      <div className="heading shadow bg-gray-200 p-2">عملیات</div>
    </div>
  );
};

const Row = props => {
  const { data, approveComment, deleteComment, iconType } = props;
  const { id, content, name, post_web_path, post_title } = data;

  const iconClassName =
    iconType === 'play'
      ? 'publish rounded-md px-2 mx-1 text-green-500 opacity-50 hover:opacity-100 '
      : 'unpublish rounded-md px-2 mx-1 text-green-500 opacity-50 hover:opacity-100 ';

  function onApprove() {
    approveComment(id);
  }
  function onDelete() {
    deleteComment(id);
  }
  return (
    <>
      <div className="grid grid-cols-10 gap-1 m-1 py-2 bg-white hover:shadow">
        <div className="index col-1">1</div>
        <div className="username col-2">
          <Link
            to="link-to-profile"
            target="_blank"
            className="text-blue-400 hover:text-black"
          >
            {name}
          </Link>
        </div>
        <div className="comment-body col-3 col-span-3">{content}</div>
        <div className="creation-date-time col-4 flex justify-around">
          <span className="creation-time">23:30</span>
          <span className="creation-date">99/05/23</span>
        </div>

        <div className="article-title col-5 col-span-3">
          <a rel="noopener noreferrer" target="_blank" href={post_web_path}>
            {/* 
          <Link
            to={post_web_path}
            target="_blank"
            className="text-blue-400 hover:text-black"
          > */}
            {post_title}
            {/* </Link> */}
          </a>
        </div>

        <div className="actions col-6">
          <div className="flex justify-center">
            <button
              className={iconClassName}
              title="تایید و انتشار"
              onClick={onApprove}
            ></button>
            <button
              className="delete rounded-md px-2 mx-1 opacity-50 hover:opacity-100 "
              title="حذف"
              onClick={onDelete}
            ></button>
          </div>
        </div>
      </div>
    </>
  );
};
