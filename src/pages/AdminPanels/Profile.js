import React from 'react';
import { Link } from 'react-router-dom';
import profilepic from 'assets/img/user.jpg';

export default function Profile(props) {
  return (
    <div id="main-Wrapper">
      <div id="profile-part1">
        <h1>نمایۀ شخصی</h1>
        <div id="profile-main-1">
          <div className={`image-box`}>
            <div className={`img`}>
              <img src={profilepic} alt="img" />
            </div>
            <div className={`attach-box`}>
              <input type="file" className={`attach-input`} id="add" />
              <label for="add" className={`attach-label`}>
                <span className={`attach-button`}>افزودن عکس</span>
              </label>
            </div>
          </div>
          <div className={`register-box`}>
            <div className={`input-box`}>
              <label for="input">نام و نام‌خانوادگی</label>
              <input type="text" />
            </div>

            <div className={`input-box`}>
              <label for="input">ایمیل</label>
              <input type="email" />
            </div>
            <div className={`input-box space`}>
              <label for="input">رمز عبور</label>
              <input type="password" />
            </div>
            <div className={`input-box`}>
              <label for="input">تکرار رمز عبور</label>
              <input type="password" />
            </div>
            <div className={`rules`}>
              <label for="check">
                با
                <p>
                  <Link className={`ruleslink`} to={`/`}>
                    قوانین
                  </Link>
                </p>
                دانوما موافقم
              </label>
              <input type="checkbox" id="check" checked />
            </div>
          </div>
        </div>
        <div id="register-button">
          <span className="save">ذخیرۀ تغییرات</span>
        </div>
      </div>
      <div id="profile-part2">
        <h1>مشخصات فردی</h1>
        <div className={`row1`}>
          <div className={`input-box`}>
            <label for="input">سن</label>
            <input type="text" />
          </div>
          <div className={`radio-box`}>
            <label className={`sex`}>جنسیت</label>
            <div id="radio-group">
              <label>
                مرد
                <input id="man" type="radio" value="value1" name="group1" />
              </label>
              <label>
                زن
                <input id="woman" type="radio" value="value2" name="group1" />
              </label>
            </div>
          </div>
        </div>
        <div className={`row2`}>
          <label>تحصیلات</label>
          <input type="text" placeholder="مقطع" />
          <input type="text" placeholder="رشته" />
        </div>
        <div className={`row3`}>
          <div className={`text-box`}>
            <h2>تخصص ها</h2>
            <ol>
              <li>
                <label>مدیریت</label>
                <i className={`fas fa-chevron-left`}></i>
                <label>کارآفرینی</label>
                <i className={`far fa-trash-alt`}></i>
              </li>
              <li>
                <label>حقوق</label>
                <i className={`fas fa-chevron-left`}></i>
                <label>خانواده</label>
                <i className={`far fa-trash-alt`}></i>
              </li>
            </ol>
          </div>
          <div className={`select-box`}>
            <select>
              <option value="1">مدیریت</option>
              <option value="2">مدیریت</option>
              <option value="3">مدیریت</option>
            </select>
            <select>
              <option value="1">مدیریت</option>
              <option value="2">مدیریت</option>
              <option value="3">مدیریت</option>
            </select>
            <div className={`plus`}>
              <i className={`far fa-plus-square`}></i>
              <label>اضافه کن</label>
            </div>
          </div>
        </div>
        <div id="register-button">
          <span className="save">ذخیرۀ تغییرات</span>
        </div>
      </div>
      <footer>فوتر اینجاست</footer>
    </div>
  );
}
