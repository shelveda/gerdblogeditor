import React from 'react';

import RichEditor from 'components/Editor/RichEditor';

export default function EditorSection(props) {
  const { name, title, type, index, EditorState, id, onSubmit } = props;

  return (
    <div id={id} className="editortabcontent">
      <RichEditor
        name={name}
        onSubmit={onSubmit}
        type={type}
        init={EditorState}
        index={index}
        title={title}
      />
    </div>
  );
}
