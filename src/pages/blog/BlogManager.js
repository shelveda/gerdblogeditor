import React, { useState, useEffect } from 'react';
import useShowArray from 'hooks/useShowArray';
import { MasterApi } from 'api/MasterApi';
import './editor.css';
import testJason from './test.js';

import ImageUpload from './comps/ImageUpload';
import TitleSection from './comps/TitleSection';
import TagSection from './comps/TagSection';
import EditorSection from './comps/EditorSection';
import HeaderImgSection from './comps/HeaderImgSection';

// import Tree from '../../components/TreeMenu/Tree';
import { convertToRaw } from 'draft-js';
import { createEditorState, stepDivider } from './comps/enditorInit';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import { useHistory, useLocation } from 'react-router-dom';
import Swal from 'sweetalert2';

import * as R from 'ramda';

// key attachments for later

const keys = ['blog'];
const editrosKeys = [
  {
    title: 'بلاگ',
    name: 'blog',
  },
];

export default function EditorManager({ Content, type, img, initID, pId }) {
  const url = useLocation();
  const history = useHistory();
  const { setLoading } = useLoadingDispatch();

  let initTags = [];
  if (type === 'edit') initTags = Content.tags.map(tag => Number(tag.id));

  const newState = createEditorState(keys, Content, type);

  let inStep = 0;

  let init = false;
  if (type === 'edit') {
    const { brief } = Content || 'init';
    brief === 'init' ? (inStep = 4) : (inStep = 10);
    if (brief === 'init') init = true;
  }
  const initCatIds = Content ? Content.cat_ids || [] : [];

  const [state, setState] = useState({
    title: {
      error: type === 'edit' ? false : true,
      value: type === 'edit' ? Content.title : '',
    },
    slug: type === 'edit' ? Content.slug : '',
    step: inStep,
    catId: type === 'edit' ? Number(Content.cat_id) : 0,
    postId: initID,
    postPath: type === 'edit' ? Content.path : '',
    images: img.images,
    imagesId: img.images.map(img => img.id),
    imgHeader: type === 'edit' ? Content.image_header || {} : {},
    imageActive: false,
    tags: initTags,
    catIds: type === 'edit' ? initCatIds.map(id => Number(id)) : [],
    catState: {},
    init: init,
    ...newState,
  });

  useEffect(() => {
    if (url.pathname === '/blog') {
      const newState = createEditorState(keys, {}, 'new');
      setState({
        title: {
          error: true,
          value: '',
        },
        // step: 4,
        step: 5,
        catId: 0,
        postId: 0,
        postPath: '',
        images: [],
        imagesId: [],
        imgHeader: {},
        imageActive: false,
        tags: [],
        catIds: [],
        catState: {},
        init: init,
        ...newState,
      });
    }
  }, [url]);

  const { step } = state;

  function addTags(tags) {
    setState(prev => ({ ...prev, tags: tags }));
  }

  function saveTitle(ids) {
    const { title } = state;

    if (title.value !== '') {
      const newStep = step > 1 ? step : 1;
      setState({ ...state, step: newStep, catIds: ids });
      // setShow(1);
    } else {
      setState({ ...state, title: { value: '', error: true } });
    }
  }

  async function SumbitPage() {
    setLoading(true);

    const editorContent = keys.reduce((acc, key) => {
      return {
        ...acc,
        [key]:
          state[key].editor &&
          convertToRaw(state[key].editor.getCurrentContent()),
      };
    }, {});

    const html = keys.reduce(
      (acc, key) => {
        if (state[key].html === null) return acc;
        return { ...acc, [key]: state[key].html };
      },
      { url: state.imgHeader.image_url }
    );

    // const slug = state.title.value.split(' ').join('-');

    const vals = {
      title: state.title.value,
      brief: state.intro.html,
      slug: state.slug,
      video_url: '',
      editor: editorContent,
      html: html,
      cat_ids: state.catIds,
      cat_id: state.catIds[0],
      image_content: state.imagesId,
      tags: state.tags,
      attachments: [],
      published_at: type === 'edit' ? true : null,
      image_header: state.imgHeader.id,
    };

    const response =
      type === 'edit'
        ? await MasterApi('PATCH', `blog/post/${state.postId}`, vals)
        : await MasterApi('POST', `blog/post`, vals);

    const { status } = response;
    if (status === 200 || status === 201 || status === 202) {
      if (init) {
        //   setTimeout(() => {
        //   history.go('');
        // }, 500);
        history.push(`/blog/${response.data.id}`);
        return '';
      }
      Swal.fire({
        title: 'مقاله ویرایش شد',
        icon: 'success',
        // showConfirmButton: true,
        timer: 1000,
      });
      setLoading(false);
      return '';
    }
    Swal.fire({
      position: 'bottom-end',
      icon: 'error',
      title: 'شما مقدمه را تأیید نکرده اید یا اروری رخ داده است',
      showConfirmButton: true,
      timer: 10000,
    }); // setTab(newStep);
    setLoading(false);
  }

  async function handleSumbit(name, index, html, editor) {
    const stepHtml = stepDivider(html);

    setState({
      ...state,
      [name]: {
        editor: editor,
        html: stepHtml,
      },
    });

    Swal.fire({
      position: 'bottom-end',
      icon: 'success',
      title: 'تغییرات انجام شد',
      showConfirmButton: false,
      timer: 500,
    }); // setTab(newStep);
    // setShow(Number(index + 1));
  }
  async function saveInit() {
    setLoading(true);
    const slug = state.title.value.split(' ').join('-');
    const vals = {
      ...testJason,
      title: state.title.value,
      slug,
    };

    const response = await MasterApi('POST', `post`, vals);

    if (response.status === 201 || 202 || 200) {
      setLoading(false);
      setState({
        ...state,
        step: 4,
        postId: Number(response.data.id),
        init: true,
      });
      history.push(`/blog/${response.data.id}`);
      return '';
    }
  }

  function setTitle(e) {
    setState({ ...state, title: { value: e.target.value, error: false } });
  }
  function setSlug(e) {
    setState({ ...state, slug: e.target.value });
  }

  function addImage(data) {
    setState({
      ...state,
      images: R.uniq([
        ...state.images,
        {
          ...data,
        },
      ]),
      imagesId: R.uniq([...state.imagesId, data.id]),
    });
  }
  function removeImage(nid) {
    const id = Number(nid);
    const cloneImgsIds = [...state.imagesId];
    const cloneImages = [...state.images];

    const filteredImgs = R.uniq(
      R.filter(image => Number(image.id) !== id),
      cloneImages
    );
    const filteredIds = R.uniq(
      R.filter(fid => Number(fid) !== id),
      cloneImgsIds
    );

    setState({
      ...state,
      images: filteredImgs,
      imagesId: filteredIds,
    });
  }

  function handleHeaderImgSubmit(img) {
    setState({
      ...state,
      imgHeader: img,
    });
  }

  function onCatSelect(cats) {
    setState(prev => ({ ...prev, catIds: cats }));
  }

  // create tabs from
  const editorPages = editrosKeys.reduce((acc, item, index) => {
    const { title, name } = item;

    const inProps = {
      title,
      name,
      step,
      type,
      index: index + 4,
      EditorState: state[name].editor,
      onSubmit: handleSumbit,
    };
    return [
      ...acc,
      {
        Component: EditorSection,
        PROPS: inProps,
      },
    ];
  }, []);

  const headerPages = [
    {
      Component: ImageUpload,
      PROPS: {
        addImage: addImage,
        images: state.images,
        removeImage: removeImage,
      },
    },
    {
      Component: HeaderImgSection,
      PROPS: {
        imgHeader: state.imgHeader,
        images: state.images,
        type: type,
        onSubmit: handleHeaderImgSubmit,
      },
    },
    {
      Component: TitleSection,
      PROPS: {
        onSave: saveTitle,
        catIds: state.catIds,
        onCatSelect: onCatSelect,
      },
    },
    {
      Component: TagSection,
      PROPS: {
        addTags: addTags,
        initTags: state.tags,
      },
    },
  ];

  const PAGES = [...headerPages, ...editorPages];

  const [tab, setTab] = useState(0);

  return (
    <>
      {/* <button
        onClick={() => {
          console.log(state);
        }}
      >
        state
      </button> */}
      <TitleBar
        saveInit={saveInit}
        value={state.title.value}
        onChange={setTitle}
        onChangeSlug={setSlug}
        slug={state.slug}
        type={type}
        init={init}
      />

      <div className="editortab flex">
        <EditorTabs setTab={setTab} step={step} />
        <div className="editor-contents flex-1 border border-green-600 rounded-lg p-4">
          <DisplayBox PAGES={PAGES} tab={tab} />
        </div>
      </div>
      <div className='className="mr-32"'>
        <button
          className="w-48 mx-auto block my-4 p-2 bg-green-600 text-white rounded-lg hover:bg-green-500"
          style={{
            textAlign: 'center',
            backgroundColor: type === 'edit' ? '' : 'gray',
          }}
          onClick={SumbitPage}
          disabled={type === 'edit' ? false : true}
        >
          ذخیره مقاله
        </button>
      </div>
    </>
  );
}

const TitleBar = props => {
  const { value, onChange, type, saveInit, init, slug, onChangeSlug } = props;
  let header = 'مقاله جدید';
  if (type === 'edit') header = 'ادیت';
  if (init) header = 'مقاله در حالت اولیه';
  return (
    <>
      <div className="editor-title w-full rounded-lg">
        <h1 className="text-white text-xl text-center pt-1 pb-2">{header}</h1>
      </div>
      <div className="article-title flex my-4">
        <label
          for="article-title"
          className="text-gray-600 pt-1 font-bold w-32 inline-block text-center"
        >
          عنوان مقاله:
        </label>
        <input
          type="text"
          name="article-title"
          className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
          value={value}
          onChange={onChange}
        />
        <label
          for="article-title"
          className="text-gray-600 pt-1 font-bold w-32 inline-block text-center mx-2"
        >
          اسلاگ:
        </label>
        {type === 'edit' && (
          <input
            type="text"
            name="article-title"
            className="flex-1 rounded-lg text-green-600 bg-white border border-green-600 shadow  h-8 p-2"
            value={slug}
            onChange={onChangeSlug}
          />
        )}

        {type !== 'edit' && (
          <button
            className="w-8 text-center text-xl rounded-lg bg-green-600 text-white shadow mr-1  hover:bg-green-500"
            onClick={saveInit}
          >
            ✓
          </button>
        )}
      </div>
    </>
  );
};

const EditorTabs = props => {
  const { setTab, step } = props;

  const className =
    'editortablinks mt-1 w-32 p-2  rounded-r-lg text-left text-xs block';

  const [active, setActive, activate] = useShowArray(11);

  React.useEffect(() => {
    activate(step);
    setTab(step);
  }, [step]);

  const shower = num => () => {
    activate(num);
    setTab(num);
  };

  React.useEffect(() => {
    activate(0);
    setTab(0);
  }, []);

  const classNameActive =
    'editortablinks mt-1 w-32 p-2  rounded-r-lg text-left text-xs block active';

  return (
    <div className="editor-menu w-32 my-4 text-left">
      <button
        id="defaultEditorTab"
        className={active[0] ? classNameActive : className}
        onClick={shower(0)}
      >
        گالری
      </button>
      <button
        id="article_image_button"
        className={active[1] ? classNameActive : className}
        onClick={shower(1)}
        disabled={step < 1}
        style={{ backgroundColor: step < 1 ? 'gray' : '' }}
      >
        تصویر شاخص
      </button>
      <button
        id="article_cats_button"
        className={active[2] ? classNameActive : className}
        onClick={shower(2)}
        disabled={step < 2}
        style={{ backgroundColor: step < 2 ? 'gray' : '' }}
      >
        دسته بندی
      </button>
      <button
        id="article_tags_button"
        className={active[3] ? classNameActive : className}
        onClick={shower(3)}
        disabled={step < 3}
        style={{ backgroundColor: step < 3 ? 'gray' : '' }}
      >
        برچسب ها
      </button>

      {editrosKeys.map((item, index) => {
        const { title, name } = item;
        return (
          <button
            key={`${title}${index}${name}`}
            className={active[index + 4] ? classNameActive : className}
            onClick={shower(index + 4)}
            disabled={step < index + 4}
            style={{ backgroundColor: step < index + 4 ? 'gray' : '' }}
          >
            {title}
          </button>
        );
      })}
    </div>
  );
};

const DisplayBox = props => {
  const { PAGES, tab } = props;
  const Page = PAGES[tab];
  const { Component, PROPS } = Page;
  return <Component {...PROPS} />;
};
