import React, { useState, useEffect } from 'react';
import EditorManager from './EditorManager';

import { MasterApi } from '../../api/MasterApi';
import { useLoadingDispatch } from '../../contexts/LoadingContext';
import { useLocation } from 'react-router-dom';

function EditorPage(props) {
  const { setLoading } = useLoadingDispatch();
  const url = useLocation();
  const { match } = props;
  const { postId } = match.params;
  const [state, setState] = useState({
    content: null,
    loading: true,
    title: '',
    type: 'new',
    images: [],
    imgHeader: {},
  });

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      if (postId) {
        const response =
          (await MasterApi('POST', `/post/${postId}/edit`)) || {};
        const images = await MasterApi('POST', `images/${postId}`);

        if (response.status === 200 && images.status === 200) {
          setLoading(false);
          return setState(prev => ({
            ...prev,
            title: response.data.title,
            content: response.data,
            imgUrl: response.data.image_header,
            loading: false,
            type: 'edit',
            images: images.data.image_content,
            imgHeader: images.data.image_header,
          }));
        }
      } else {
        setState(prev => ({
          ...prev,
          loading: false,
          type: 'new',
        }));
      }
      setLoading(false);
    }

    fetchData();
  }, [postId, url]);

  let initID = 0;
  if (state.type === 'edit') {
    initID = state.content.id;
  }

  if (state.loading) return '';

  return (
    <div className="editor">
      <EditorManager
        Content={state.content}
        type={state.type}
        img={{ images: state.images, imgHeader: state.imgHeader }}
        initID={initID}
        pId={initID}
      />
    </div>
  );
}

export default EditorPage;
