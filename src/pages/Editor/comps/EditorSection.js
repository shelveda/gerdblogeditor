import React from 'react';

import RichEditor from 'components/Editor/RichEditor';

export default function EditorSection(props) {
  const { name, title, type, index, EditorState, id, onSubmit } = props;
  console.log(
    '🚀 ~ file: EditorSection.js ~ line 7 ~ EditorSection ~ EditorState',
    EditorState
  );

  return (
    <div id={id} className="editortabcontent">
      <RichEditor
        name={name}
        onSubmit={onSubmit}
        type={type}
        init={EditorState}
        index={index}
        title={title}
      />
    </div>
  );
}
