import React, { useState } from 'react';
import { useLoadingDispatch } from 'contexts/LoadingContext';
import { MasterApi } from 'api/MasterApi';
import CopyToClipboard from 'react-copy-to-clipboard';
export default function ImageUpload(props) {
  const { setLoading } = useLoadingDispatch();
  const inputRef = React.useRef(null);
  const { images, addImage, removeImage } = props;

  const [state, setState] = useState({
    file: '',
    imagePreviewUrl: '',
  });

  async function _handleSubmit(e) {
    e.preventDefault();

    setLoading(true);
    const response = await MasterApi('POST', '/images', {
      images: state.imagePreviewUrl,
    });

    if (response.status === 201) {
      addImage({
        id: response.data.id,
        image_url: response.data['image_url'],
      });
      setState({
        file: '',
        imagePreviewUrl: '',
        active: true,
      });
    }

    setLoading(false);
  }

  function _handleImageChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      setState(prev => ({
        ...prev,
        file: file,
        imagePreviewUrl: reader.result,
      }));
    };

    reader.readAsDataURL(file);
  }

  let { imagePreviewUrl } = state;
  let $imagePreview = null;

  if (imagePreviewUrl) {
    $imagePreview = (
      <div style={{ margin: ' 10px auto', textAlign: 'center' }}>
        <img
          style={{ maxHeight: '400px' }}
          src={imagePreviewUrl}
          alt={'نمایش عکس'}
        />
      </div>
    );
  } else {
    // $imagePreview = <div className="previewText"></div>;
  }

  const handleRemoveImg = image => async () => {
    setLoading(true);
    const response = await MasterApi('DELETE', `/images/${image.id}`, {});

    if (response.status === 202) {
      removeImage(image.id);
      setState({
        file: '',
        imagePreviewUrl: '',
        active: true,
      });
    }

    setLoading(false);
  };

  return (
    <div id="article_gallery" className="editortabcontent">
      <div className="previewComponent flex">
        <div id="thumbnails" className="w-48 border-l border-green-600 pl-4">
          <div className="bg-green-600 text-center text-white p-2 rounded-lg text-sm">
            همۀ تصاویر این مقاله
          </div>
          {images.map((image, index) => {
            return (
              <div
                className="thumbnail mt-4"
                key={`${image.image_url}_${image.id}`}
              >
                <div className="rounded-md border border-green-600">
                  <img src={`${image.image_url}`} alt="تصویر" />
                </div>
                <div className="flex mt-1 justify-between">
                  <CopyToClipboard text={image.image_url}>
                    <button className="text-xs text-center rounded-md bg-green-300 hover:bg-green-400 py-1 px-6 border border-green-600">
                      کپی کردن آدرس
                    </button>
                  </CopyToClipboard>
                  <button
                    onClick={handleRemoveImg(image)}
                    className="text-xs text-center rounded-md bg-red-300 hover:bg-red-400 py-1 px-2 border border-red-600"
                  >
                    حذف
                  </button>
                </div>
              </div>
            );
          })}
        </div>
        <div className="flex-1">
          <form onSubmit={_handleSubmit} className="mr-4">
            <label
              for="article_image_input"
              className="cursor-pointer bg-green-300 p-2 rounded-lg text-xs border border-green-600 ml-4 bg-opacity-75 hover:bg-opacity-100"
            >
              انتخاب تصویر
            </label>

            <input
              name={'saeed'}
              ref={inputRef}
              onChange={_handleImageChange}
              id="article_image_input"
              className="fileInput"
              type="file"
            />
            <button className="submitButton" type="submit">
              ارسال تصویر
            </button>
          </form>
          <div className="imgPreview m-4 ml-0 border border-dotted border-green-300 text-center text-green-500 rounded-lg overflow-hidden">
            {!$imagePreview && <span>تصویری انتخاب نشده است.</span>}
            <div className="imgPreview">{$imagePreview}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
