import React, { useState } from 'react';
// import styled from 'styled-components';
import Swal from 'sweetalert2';
import { useCatContext } from 'contexts/ResourceContext';

function TitleSection(props) {
  const { catIds, onSave } = props;
  // const CatIds = [6, 8];
  const { cats } = useCatContext();
  const [catIDs, setcatIDs] = useState(
    catIds.filter(id => id !== 0 && id !== 1)
  );

  function handleSave() {
    const remIds = [...catIDs].filter(id => id !== 0);
    setcatIDs(remIds);
    onSave(remIds);
    Swal.fire({
      position: 'bottom-end',
      icon: 'success',
      title: 'تغییرات انجام شد',
      showConfirmButton: false,
      timer: 500,
    });
  }

  function oneCatSelect(index, id) {
    const newCatIds = [...catIDs];

    newCatIds[index] = id;
    if (id === 'remove') {
      newCatIds.splice(index, 1);
    }
    setcatIDs(newCatIds);
  }

  function addRow() {
    const newCatIds = [...catIDs];
    newCatIds.push(0);
    setcatIDs(newCatIds);
  }

  return (
    <>
      <div className="flex">
        <div className="flex-1 bg-green-600 text-center text-white p-2 rounded-lg text-sm">
          انتخاب رسته و حوزه
        </div>
        <button
          className="submitButton w-24 mr-2 font-bold"
          onClick={handleSave}
        >
          تایید
        </button>
      </div>
      <div>
        {catIDs.map((id, index) => {
          return (
            <ADDANOTHERCAT
              key={`title_${id}${index}`}
              index={index}
              catId={id}
              cats={cats}
              onCatSelect={oneCatSelect}
            />
          );
        })}
        <div>
          <button
            className="submitButton mt-4 w-8 h-8 font-bold align-middle"
            onClick={addRow}
          >
            +
          </button>
        </div>
      </div>
    </>
  );
}

const ADDANOTHER = props => {
  const { catId, cats, index, onCatSelect } = props;
  const children = cats[2];
  const parents = cats[1];
  let current = [];
  let pId = parents[0].id;

  if (catId !== 0) {
    const cat = children.find(item => item.id === catId);
    const curP = cat.parent_cat.length !== 0 ? cat.parent_cat[0] : { id: 1 };

    pId = curP.id;
    current =
      children.filter(child => {
        let inParent = child.parent_cat[0] || { id: 1 };
        return pId === inParent.id;
      }) || [];
  }

  const [state, setState] = useState({
    curChildren: current,
    parentId: pId,
    id: catId,
  });

  function handleParentSelect(e) {
    const id = Number(e.target.value);
    const curChildren =
      children.filter(child => {
        let inParent = child.parent_cat[0] || { id: 1 };

        return id === inParent.id;
      }) || [];
    setState(prev => ({ ...prev, curChildren, parentId: id }));
    if (curChildren.length > 0) onCatSelect(index, curChildren[0].id);
  }

  const { parentId } = state;

  function onCatChange(e) {
    const id = Number(e.target.value);
    setState(prev => ({ ...prev, id }));
    onCatSelect(index, id);
  }

  function handleRemove() {
    onCatSelect(index, 'remove');
  }

  return (
    <div style={{ marginTop: '1rem' }}>
      <select
        className="border border-green-600 rounded-lg text-sm py-1 px-4 ml-2"
        value={parentId}
        onChange={handleParentSelect}
      >
        {parents.map((cat, index) => {
          return (
            <option key={`${cat.id}_${index}`} value={cat.id}>
              {cat.name}
            </option>
          );
        })}
      </select>

      <select
        className="border border-green-600 rounded-lg text-sm py-1 px-4"
        value={state.id}
        onChange={onCatChange}
      >
        {state.curChildren.length !== 0
          ? state.curChildren.map((child, index) => {
              return (
                <option key={`${child.id}_${index}`} value={child.id}>
                  {child.name}
                </option>
              );
            })
          : ''}
      </select>

      <button
        onClick={handleRemove}
        className="text-xs text-center rounded-md bg-red-300 hover:bg-red-400 px-2 h-8 ml-2 border border-red-600 mx-2 align-middle"
      >
        حذف
      </button>
    </div>
  );
};

const ADDANOTHERCAT = React.memo(ADDANOTHER);

export default React.memo(TitleSection);
