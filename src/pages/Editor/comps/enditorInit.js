import React from 'react';
import styled from 'styled-components';
import { CompositeDecorator } from 'draft-js';

import * as R from 'ramda';

import { convertFromRaw, EditorState } from 'draft-js';

const LinkA = styled.a`
  color: #3b5998;
  text-decoration: underline;
`;

export const LinkB = styled.a`
  /* color: #3b5998; */
  text-decoration: underline;
  background-image: linear-gradient(
    to right,
    rgb(244, 107, 69) 0%,
    rgb(238, 168, 73) 51%,
    rgb(244, 107, 69) 100%
  );
`;

// #region for Editor start

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};
const EditorLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkA href={url} rel={rel}>
      {props.children}
    </LinkA>
  );
};

// rich
function findLinkRichEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'RICHLINK'
    );
  }, callback);
}

const EditorRichLink = props => {
  const { url, rel } = props.contentState.getEntity(props.entityKey).getData();
  return (
    <LinkB href={url} rel={rel}>
      {props.children}
    </LinkB>
  );
};

const decorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: EditorLink,
  },
  {
    strategy: findLinkRichEntities,
    component: EditorRichLink,
  },
]);

function createEditorState(keys, Content, type) {
  return keys.reduce((acc, key) => {
    let editor = EditorState.createEmpty(decorator);

    if (type === 'edit') {
      const { blocks = [], entityMap = [] } = Content.editor[key];
      const newBlocks = blocks.map(item => {
        if (item.text === null) return { ...item, text: ' ' };
        return item;
      });

      const NewEditorContent = { blocks: newBlocks, entityMap };
      editor = EditorState.createWithContent(
        convertFromRaw(NewEditorContent),
        decorator
      );
    }

    const html = type === 'edit' ? Content.html[key] : '';

    return {
      ...acc,
      [key]: {
        editor: editor || null,
        html: html,
      },
    };
  }, {});
}

// const checkRich=(html)=>{
//   const
// }
function stepDivider(html) {
  let HtmlArray2 = html.split('<h4>step</h4>');

  let fHtml = HtmlArray2.reduce((acc, block, index) => {
    if (block === '') return '';
    const blockSplit = block.split('<st>');
    if (index === 0) {
      if (blockSplit.length === 1 && blockSplit[0] !== '')
        return `${acc}${blockSplit[0]}`;
    }

    // const checkForRich = ()
    const outPut = `<div class="step mr-4 mb-4">
    <h3 class="rounded-l-full text-green-700 text-xl shadow1 lalezar h-10 pt-1 pr-4 ml-4 mt-6 mb-3">
      ${blockSplit[0]}
      <span
        class="float-left cursor-pointer text-green-700 ml-6 text-xl"
        >-</span
      >
    </h3>
    ${blockSplit[1]}
    </div>`;
    return `${acc}${outPut}`;
  }, ``);
  console.log(fHtml);

  if (fHtml === '<p class="p-4 pl-6 text-justify"><br></p>') return null;
  if (fHtml === '<p class="p-4 pl-6 text-justify"> </p>') return null;
  if (fHtml === '<p class="p-4 pl-6 text-justify"></p>') return null;
  if (fHtml === '<p class="p-4 pl-6 text-justify"><br/></p>') return null;
  if (fHtml === '<p class="p-4 pl-6 text-justify"></p>') return null;
  if (fHtml === '<p class="p-4 pl-6 text-justify"><br></br></p>') return null;
  if (fHtml === '') return null;

  return fHtml;
}

const richDivider = text => {
  let init = text;
  const allRichTexts = R.match(/<richstart>[\s\S]*?<richend>/g)(init);
  allRichTexts.forEach(el => {
    let innerText = el;
    innerText = R.replace('<richstart>', '')(innerText);
    innerText = R.replace('<richend>', '')(innerText);
    const imgInside = R.match(/<figure><img(.*?)<\/figure>/g)(innerText)[0];
    const LinkInside = R.match(/<a href(.*?)107, 69\) 100%\);">(.*?)<\/a>/g)(
      innerText
    )[0];

    let others = R.replace(/<figure><img(.*?)<\/figure>/g, '')(innerText);
    others = R.replace(
      /<a href(.*?)107, 69\) 100%\);">(.*?)<\/a>/g,
      ''
    )(others);
    others = R.replace(/<p class="p-4 pl-6 text-justify"> <\/p>/g, '')(others);
    others = R.replace(/<p class="p-4 pl-6 text-justify"><\/p>/g, '')(others);
    others = R.replace(
      /<p class="p-4 pl-6 text-justify"><br><\/p>/g,
      ''
    )(others);
    others = R.replace(
      /<p class="p-4 pl-6 text-justify"><br><\/br><\/p>/g,
      ''
    )(others);
    others = R.replace(
      /<p class="p-4 pl-6 text-justify"><br\/><\/p>/g,
      ''
    )(others);
    others = R.replace(/\r?\n|\r/g, '')(others);

    const finalText = `
    <div class="richbox"><div class="richbox-image">${imgInside}</div><div class="richbox-text">${others}</div>${LinkInside}</div>
    `;
    init = R.replace(el, finalText, init);
  });
  return init;
};

export { createEditorState, stepDivider, richDivider };
