export default {
  cat_ids: [1],
  cat_id: '1',
  brief: 'init',
  html: {
    intro: '<p>test </p>',
    steps: null,
    documents: null,
    hints: null,
    exp: null,
    refs: null,
    writers: null,
  },
  editor: {
    intro: { blocks: [], entityMap: [] },
    steps: { blocks: [], entityMap: [] },
    documents: { blocks: [], entityMap: [] },
    hints: { blocks: [], entityMap: [] },
    exp: { blocks: [], entityMap: [] },
    refs: { blocks: [], entityMap: [] },
    writers: { blocks: [], entityMap: [] },
  },
};
