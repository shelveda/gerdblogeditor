import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './assets/tailwind.min.css';
// import './assets/sass/main.scss';
import './assets/index.css';
import './assets/style.css';

const Root = () => <App />;

ReactDOM.render(<Root />, document.getElementById('root'));
serviceWorker.unregister();
