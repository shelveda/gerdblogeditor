import { css } from 'styled-components';

export default {
  Figure: `
        display: inline-block;
        margin: 0 0 1rem;
    `,
  ImgFluid: `
        max-width: 100%;
        height: auto;
        vertical-align: middle;
        border-style: none;        
    `,
  FigureImg: `
        line-height: 1;
        margin-bottom: 0.5rem;
    `,
  FigCaption: `
        font-size: 90%;
        color: #6C757D;
        display: block;
    `,
  Media: {
    Def: `
            align-items: flex-start;
            display: flex;
        `,
    Body: `flex: 1;`,
  },
  ListGroup: {
    Def: `
            display: flex;
            flex-direction: column;
            padding-left: 0;
            margin-bottom: 0;
        `,
    Item:
      `
            background-color: #FFF;
            position: relative;
            margin-bottom: -1px;
        ` +
      css`
        &:first-child {
          border-top-left-radius: 0.125rem;
          border-top-right-radius: 0.125rem;
        }
      `,
  },
  ListInline: {
    Def: `
            list-style: none;
            padding-left: 0;
        `,
    Item: `display: inline-block;`,
  },
  Nav: {
    Def: `
            flex-wrap: wrap;
            list-style: none;
            display: flex;        
            padding-left: 0;
            margin-bottom: 0;
        `,
    Link: `
            display: block;
            padding: 0.5rem 1rem;
        `,
  },
  MdForm: `
        position: relative;
        margin-top: 1.5rem;
        margin-bottom: 1.5rem;
    `,
  MdOutline: `margin: 0;`,
  Card: {
    Def: `
          min-width: 0;
          word-wrap: break-word;
          font-weight: 400;
          background-color: #FFF;
          background-clip: border-box;
          border-radius: 0.25rem;
          border: 1px solid rgba(0, 0, 0, 0.125);
          position: relative;
          flex-direction: column;
          -ms-flex-direction: column;
          display: flex;
          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          -o-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
      `,
    Header: `
          background-color: rgba(0, 0, 0, 0.03);
          border-bottom: 1px solid rgba(0, 0, 0, 0.125);
          padding: 0.75rem 1.25rem;
          margin-bottom: 0;
      `,
    Body: `
          flex: 1 1 auto;
          -ms-flex: 1 1 auto;
          padding: 1.25rem;
      `,
  },
  CustomControl: {
    Def: `
             min-height: 1.5rem;  
             position: relative;
             display: block;
             padding-left: 1.5rem;
        `,
    Inline: `
             display: inline-flex;
             margin-right: 1rem;
        `,
    Input: {
      Def: `
                  box-sizing: border-box;
                  opacity: 0;
                  position: absolute;
                  z-index: -1;
                  padding: 0;
             `,
      CheckBox: css`
        ~ label {
          &:before,
          &:after {
            border-radius: 0.3rem;
          }
        }
      `,
      Radio: css`
        ~ label {
          &:before,
          &:after {
            border-radius: 50%;
          }
        }
      `,
    },
    Label:
      `
             vertical-align: top;
             position: relative;
             margin-bottom: 0 !important;
        ` +
      css`
        &:before {
          pointer-events: none;
          background-color: #fff;
          border: #adb5bd solid 1px;
          transition: background-color 0.15s ease-in-out,
            border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        &:after {
          background: no-repeat 50% / 50% 50%;
        }
        &:before,
        &:after {
          width: 1rem;
          height: 1rem;
          content: '';
          position: absolute;
          top: 0.25rem;
          left: -1.5rem;
          display: block;
        }
      `,
  },
  Button: `
        font-size: 0.64rem;
        white-space: normal;
        word-wrap: break-word;
        font-weight: 400;
        font-family: inherit;
        vertical-align: middle;
        text-transform: uppercase;
        text-align: center;
        color: #212529;
        background-color: transparent;
        border-radius: 0.125rem;
        border: 0;
        cursor: pointer;
        position: relative;
        display: inline-block;
        overflow: hidden;
        -webkit-tap-highlight-color: transparent;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    `,
  Btn: {
    Def:
      `
        text-transform: inherit;
        border: 1px solid transparent;
    ` +
      css`
        &[className*='bg-primary'] {
          &:hover,
          &:focus {
            background-color: #035388 !important;
          }
        }
        &[className*='bg-teal'] {
          &:hover,
          &:focus {
            background-color: #0e1f2f !important;
          }
        }
        &[className*='bg-success'] {
          &:hover,
          &:focus {
            background-color: #075647 !important;
          }
        }
      `,
    Custom: `
        white-space: inherit;
        background-color: #FFF;
        border: 1px solid rgba(112, 112, 112, 0.25) !important;
        padding: 0 0.25rem 0.25rem 0.25rem;
        box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.16) !important;
        -moz-box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.16) !important;
        -webkit-box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.16) !important;
        -o-box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.16) !important;
    `,
  },
  Number: `
        text-align: left;
        direction: ltr !important;
    `,
  Divider: `height: 1px;`,
  ListSeparate: css`
    li:not(:last-child) {
      border-right: 0.5px solid #707070;
      margin: 0 0.5rem;
    }
  `,
  NoSelect: `
        user-select: none;
        pointer-events: none;
    `,
  Table:
    `
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 1rem;
  ` +
    css`
      th {
        text-align: inherit;
        border: 0;
      }
      th,
      td {
        padding: 0.6rem 0.3rem;
      }
    `,
  Pagination:
    `
    list-style: none;
    border-radius: 0.25rem;
    display: flex;
    padding-left: 0;
  ` +
    css`
      span {
        font-size: 0.9rem;
        line-height: 1.25;
        color: #212529;
        background-color: transparent;
        border: 0;
        outline: 0;
        border-radius: 0.3rem;
        position: relative;
        display: block;
        transition: all 0.3s linear 0s;
        padding: 0.25rem 0.5rem;
        margin: 0 0.25rem;
      }
    `,
  Modal: {
    Def: `
      width: 100%;
      height: 100%;
      outline: 0;
      background: rgba(0, 0, 0, 0.25);
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 1101;
      display: block;
      overflow-x: hidden;
      overflow-y: auto;
      transition: opacity 0.15s linear;
      padding-right: 0 !important;
    `,
    Dialog: {
      Def: `
        width: auto;
        min-height: calc(100% - 3.5rem);
        pointer-events: none;
        align-items: center;
        position: relative;
        display: flex;
        transform: none;
        transition: transform 0.3s ease-out;
        margin: 1.75rem auto;
      `,
      Size: {
        Sm: `max-width: 300px;`,
        Lg: `max-width: 800px;`,
        Xl: `max-width: 1140px;`,
      },
    },
    Content: {
      Def: `
        width: 100%;
        outline: 0;
        pointer-events: auto;
        background-color: #FFF;
        background-clip: padding-box;
        border-radius: 0.125rem;
        border: 0;
        box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
        position: relative;
        flex-direction: column;
        display: flex;
      `,
      Header: {
        Def: `
          align-items: flex-start;
          justify-content: space-between;
          border-top-right-radius: 0.3rem;
          border-top-left-radius: 0.3rem;
          border-bottom: 1px solid #DEE2E6;
          display: flex;
          padding: 1rem;
        `,
        Title: `
          line-height: 1.5;
          margin-bottom: 0;
        `,
        Close: `
          font-size: 1.5rem;
          font-weight: 700;
          appearance: none;
          line-height: 1;
          color: #000;
          background-color: transparent;
          opacity: 0.5;
          border: 0;
          text-shadow: 0 1px 0 #FFF;
          cursor: pointer;
          float: right;
          padding: 0;
        `,
      },
      Body: `
        position: relative;
        flex: 1 1 auto;
        padding: 1rem;
      `,
      Footer: `
        align-items: center;
        justify-content: flex-end;
        border-bottom-right-radius: 0.3rem;
        border-bottom-left-radius: 0.3rem;
        border-top: 1px solid #DEE2E6;
        display: flex;
        padding: 1rem;
      `,
    },
  },
  Font: {
    Size: {
      Xs: `font-size: 0.4rem !important;`,
      Sm: `font-size: 0.7rem !important;`,
      Md: `font-size: 0.8rem !important;`,
      Lg: `font-size: 1rem !important;`,
      Xl: `font-size: 1.2rem !important;`,
      Max: `font-size: 1.5rem !important;`,
      Last: `font-size: 2.5rem !important;`,
    },
    Normal: `font-family: 'sarabunregular', serif !important;`,
    Bold: `font-family: 'sarabunbold', serif !important;`,
  },
  Align: {
    Content: {
      Start: `align-content: start !important;`,
      Center: `align-content: center !important;`,
      End: `align-content: flex-end !important;`,
      Stretch: `align-content: stretch !important;`,
    },
    Self: {
      Start: `align-self: start !important;`,
      Center: `align-self: center !important;`,
      End: `align-self: flex-end !important;`,
      Stretch: `align-self: stretch !important;`,
    },
    Items: {
      Start: `align-items: start !important;`,
      Center: `align-items: center !important;`,
      End: `align-items: flex-end !important;`,
      Stretch: `align-items: stretch !important;`,
    },
    Top: `vertical-align: top !important;`,
    Middle: `vertical-align: middle !important;`,
  },
  Justify: {
    Content: {
      Between: `justify-content: space-between !important;`,
      Start: `justify-content: start !important;`,
      Center: `justify-content: center !important;`,
      End: `justify-content: flex-end !important;`,
    },
    Self: {
      Start: `justify-self: start !important;`,
      Center: `justify-self: center !important;`,
      End: `justify-self: flex-end !important;`,
    },
    Items: {
      Start: `justify-items: start !important;`,
      Center: `justify-items: center !important;`,
      End: `justify-items: flex-end !important;`,
    },
  },
  Text: {
    Black: `color: #000 !important;`,
    White: `color: #FFF !important;`,
    Light: {
      Def: `color: #F0F0F7 !important;`,
      Opacity5: `color: rgba(240, 240, 247, 0.5) !important;`,
      _50: {
        Def: `color: #F0F4F8 !important;`,
        _2: `color: #F5F7FA !important;`,
      },
      _70: {
        Def: `color: #DADAE7 !important;`,
        _2: `color: #F7F7FB !important;`,
        _3: `color: #F7F9FB !important;`,
      },
    },
    Secondary: {
      Def: `color: #4D4F5C !important;`,
      _70: {
        Def: `color: #707070 !important;`,
        Opacity1: `color: rgba(112, 112, 112, 0.1) !important;`,
        Opacity25: `color: rgba(112, 112, 112, 0.25) !important;`,
        Opacity5: `color: rgba(112, 112, 112, 0.5) !important;`,
      },
    },
    Primary: {
      Def: `color: #00A2E8 !important;`,
      _10: `color: #133681 !important;`,
      _20: {
        Def: `color: #243B53 !important;`,
        _2: `color: #2DAEDF !important;`,
      },
      _30: {
        Def: `color: #035388 !important;`,
        Opacity1: `color: rgba(3, 83, 136, 0.1) !important;`,
      },
      _40: `color: #4098D7 !important;`,
      _60: {
        Def: `color: #0F609B !important;`,
        _2: `color: #ECF6FF !important;`,
      },
      _70: `color: #0072FF !important;`,
      _80: `color: #EEF4FF !important;`,
      _90: `color: #DCEEFB !important;`,
    },
    Teal: {
      Def: `color: #102A43 !important;`,
      Opacity2: `color: rgba(16, 42, 67, 0.2) !important;`,
      Opacity5: `color: rgba(16, 42, 67, 0.5) !important;`,
      _10: `color: #1F2933 !important;`,
      _50: `color: #0E1F2F !important;`,
    },
    Success: {
      Def: `color: #0C6B58 !important;`,
      _10: {
        Def: `color: #199473 !important;`,
        Opacity1: `color: rgba(25, 148, 115, 0.1);`,
        _2: `color: #16A085 !important;`,
      },
      _20: {
        Def: `color: #27AB83 !important;`,
        Opacity1: `color: rgba(39, 171, 131, 0.1) !important;`,
      },
      _30: `color: #3EBD93 !important;`,
      _60: `color: #ECFFF6 !important;`,
      _70: `color: #075647 !important;`,
    },
    Warning: {
      Def: `color: #FFDA83 !important;`,
      _30: `color: #513C06 !important;`,
      _50: `color: #FFFADB !important;`,
      _60: {
        Def: `color: #FFA000 !important;`,
        _2: `color: #FFF6E6 !important;`,
      },
      _70: {
        Def: `color: #FFF5B7 !important;`,
        _2: `color: #7C5E10 !important;`,
      },
    },
    Danger: {
      Def: `color: #BA2525 !important;`,
      _40: `color: #FF4444 !important;`,
      _50: {
        Def: `color: #FFEEEE !important;`,
        _2: `color: #FFEDDD !important;`,
      },
      _60: {
        Def: `color: #D64545 !important;`,
        _2: {
          Def: `color: #FF6565 !important;`,
          Opacity1: `color: rgba(255, 101, 101, 0.1) !important;`,
        },
      },
    },
    Purple: {
      Def: `color: #653CAD !important;`,
      _40: `color: #421987 !important;`,
      _50: `color: #F5F1FE !important;`,
      _60: `color: #6C63FF !important;`,
    },
    Lg: `
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        `,
    Align: {
      Right: `text-align: right !important;`,
      Center: `text-align: center !important;`,
      Left: `text-align: left !important;`,
      Justify: `text-align: justify !important;`,
      Initial: `text-align: initial !important;`,
    },
    Transform: {
      None: `text-transform: none !important;`,
      Lowercase: `text-transform: lowercase !important;`,
      Capitalize: `text-transform: capitalize !important;`,
      Uppercase: `text-transform: uppercase !important;`,
    },
  },
  Bg: {
    Transparent: `background-color: transparent !important;`,
    Black: `background-color: #000 !important;`,
    White: `background-color: #FFF !important;`,
    Light: {
      Def: `background-color: #F0F0F7 !important;`,
      Opacity5: `background: rgba(240, 240, 247, 0.5) !important;`,
      _50: {
        Def: `background-color: #F0F4F8 !important;`,
        _2: `background-color: #F5F7FA !important;`,
      },
      _70: {
        Def: `background-color: #DADAE7 !important;`,
        _2: `background-color: #F7F7FB !important;`,
        _3: `background-color: #F7F9FB !important;`,
      },
    },
    Secondary: {
      Def: `background-color: #4D4F5C !important;`,
      _70: {
        Def: `background-color: #707070 !important;`,
        Opacity1: `background: rgba(112, 112, 112, 0.1) !important;`,
        Opacity25: `background: rgba(112, 112, 112, 0.25) !important;`,
        Opacity5: `background: rgba(112, 112, 112, 0.5) !important;`,
      },
    },
    Primary: {
      Def: `background-color: #00A2E8 !important;`,
      _10: `background-color: #133681 !important;`,
      _20: {
        Def: `background-color: #243B53 !important;`,
        _2: `background-color: #2DAEDF !important;`,
      },
      _30: {
        Def: `background-color: #035388 !important;`,
        Opacity1: `background: rgba(3, 83, 136, 0.1) !important;`,
      },
      _40: `background-color: #4098D7 !important;`,
      _60: {
        Def: `background-color: #0F609B !important;`,
        _2: `background-color: #ECF6FF !important;`,
      },
      _70: `background-color: #0072FF !important;`,
      _80: `background-color: #EEF4FF !important;`,
      _90: `background-color: #DCEEFB !important;`,
    },
    Teal: {
      Def: `background-color: #102A43 !important;`,
      Opacity2: `background: rgba(16, 42, 67, 0.2) !important;`,
      Opacity5: `background: rgba(16, 42, 67, 0.5) !important;`,
      _10: `background-color: #1F2933 !important;`,
      _50: `background-color: #0E1F2F !important;`,
    },
    Success: {
      Def: `background-color: #0C6B58 !important;`,
      _10: {
        Def: `background-color: #199473 !important;`,
        Opacity1: `background: rgba(25, 148, 115, 0.1);`,
        _2: `background-color: #16A085 !important;`,
      },
      _20: {
        Def: `background-color: #27AB83 !important;`,
        Opacity1: `background: rgba(39, 171, 131, 0.1) !important;`,
      },
      _30: `background-color: #3EBD93 !important;`,
      _60: `background-color: #ECFFF6 !important;`,
      _70: `background-color: #075647 !important;`,
    },
    Warning: {
      Def: `background-color: #FFDA83 !important;`,
      _30: `background-color: #513C06 !important;`,
      _50: `background-color: #FFFADB !important;`,
      _60: {
        Def: `background-color: #FFA000 !important;`,
        _2: `background-color: #FFF6E6 !important;`,
      },
      _70: {
        Def: `background-color: #FFF5B7 !important;`,
        _2: `background-color: #7C5E10 !important;`,
      },
    },
    Danger: {
      Def: `background-color: #BA2525 !important;`,
      _40: `background-color: #FF4444 !important;`,
      _50: {
        Def: `background-color: #FFEEEE !important;`,
        _2: `background-color: #FFEDDD !important;`,
      },
      _60: {
        Def: `background-color: #D64545 !important;`,
        _2: {
          Def: `background-color: #FF6565 !important;`,
          Opacity1: `background: rgba(255, 101, 101, 0.1) !important;`,
        },
      },
    },
    Purple: {
      Def: `background-color: #653CAD !important;`,
      _40: `background-color: #421987 !important;`,
      _50: `background-color: #F5F1FE !important;`,
      _60: `background-color: #6C63FF !important;`,
    },
  },
  Rounded: {
    _0: `border-radius: 0 !important;`,
    Pill: `border-radius: 50rem !important;`,
    Circle: `border-radius: 50% !important;`,
    Lg: {
      Def: `border-radius: 0.3rem !important;`,
      Top: `border-radius: 0.3rem 0.3rem 0 0 !important;`,
      Right: `border-radius: 0 0.3rem 0.3rem 0 !important;`,
      Bottom: `border-radius: 0 0 0.3rem 0.3rem !important;`,
      Left: `border-radius: 0.3rem 0 0 0.3rem !important;`,
    },
    Xl: {
      Def: `border-radius: 1rem !important;`,
      Top: `border-radius: 1rem 1rem 0 0 !important;`,
      Right: `border-radius: 0 1rem 1rem 0 !important;`,
      Bottom: `border-radius: 0 0 1rem 1rem !important;`,
      Left: `border-radius: 1rem 0 0 1rem !important;`,
    },
  },
  Border: {
    Black: `border: 1px solid #000 !important;`,
    White: `border: 1px solid #FFF !important;`,
    Light: {
      Def: `border: 1px solid #F0F0F7 !important;`,
      Opacity5: `border: 1px solid rgba(240, 240, 247, 0.5) !important;`,
      _50: {
        Def: `border: 1px solid #F0F4F8 !important;`,
        _2: `border: 1px solid #F5F7FA !important;`,
      },
      _70: {
        Def: `border: 1px solid #DADAE7 !important;`,
        _2: `border: 1px solid #F7F7FB !important;`,
        _3: `border: 1px solid #F7F9FB !important;`,
      },
    },
    Secondary: {
      Def: `border: 1px solid #4D4F5C !important;`,
      _70: {
        Def: `border: 1px solid #707070 !important;`,
        Opacity1: `border: 1px solid rgba(112, 112, 112, 0.1) !important;`,
        Opacity25: `border: 1px solid rgba(112, 112, 112, 0.25) !important;`,
        Opacity5: `border: 1px solid rgba(112, 112, 112, 0.5) !important;`,
      },
    },
    Primary: {
      Def: `border: 1px solid #00A2E8 !important;`,
      _10: `border: 1px solid #133681 !important;`,
      _20: {
        Def: `border: 1px solid #243B53 !important;`,
        _2: `border: 1px solid #2DAEDF !important;`,
      },
      _30: {
        Def: `border: 1px solid #035388 !important;`,
        Opacity1: `border: 1px solid rgba(3, 83, 136, 0.1) !important;`,
      },
      _40: `border: 1px solid #4098D7 !important;`,
      _60: {
        Def: `border: 1px solid #0F609B !important;`,
        _2: `border: 1px solid #ECF6FF !important;`,
      },
      _70: `border: 1px solid #0072FF !important;`,
      _80: `border: 1px solid #EEF4FF !important;`,
      _90: `border: 1px solid #DCEEFB !important;`,
    },
    Teal: {
      Def: `border: 1px solid #102A43 !important;`,
      Opacity2: `border: 1px solid rgba(16, 42, 67, 0.2) !important;`,
      Opacity5: `border: 1px solid rgba(16, 42, 67, 0.5) !important;`,
      _10: `border: 1px solid #1F2933 !important;`,
      _50: `border: 1px solid #0E1F2F !important;`,
    },
    Success: {
      Def: `border: 1px solid #0C6B58 !important;`,
      _10: {
        Def: `border: 1px solid #199473 !important;`,
        Opacity1: `border: 1px solid rgba(25, 148, 115, 0.1);`,
        _2: `border: 1px solid #16A085 !important;`,
      },
      _20: {
        Def: `border: 1px solid #27AB83 !important;`,
        Opacity1: `border: 1px solid rgba(39, 171, 131, 0.1) !important;`,
      },
      _30: `border: 1px solid #3EBD93 !important;`,
      _60: `border: 1px solid #ECFFF6 !important;`,
      _70: `border: 1px solid #075647 !important;`,
    },
    Warning: {
      Def: `border: 1px solid #FFDA83 !important;`,
      _30: `border: 1px solid #513C06 !important;`,
      _50: `border: 1px solid #FFFADB !important;`,
      _60: {
        Def: `border: 1px solid #FFA000 !important;`,
        _2: `border: 1px solid #FFF6E6 !important;`,
      },
      _70: {
        Def: `border: 1px solid #FFF5B7 !important;`,
        _2: `border: 1px solid #7C5E10 !important;`,
      },
    },
    Danger: {
      Def: `border: 1px solid #BA2525 !important;`,
      _40: `border: 1px solid #FF4444 !important;`,
      _50: {
        Def: `border: 1px solid #FFEEEE !important;`,
        _2: `border: 1px solid #FFEDDD !important;`,
      },
      _60: {
        Def: `border: 1px solid #D64545 !important;`,
        _2: {
          Def: `border: 1px solid #FF6565 !important;`,
          Opacity1: `border: 1px solid rgba(255, 101, 101, 0.1) !important;`,
        },
      },
    },
    Purple: {
      Def: `border: 1px solid #653CAD !important;`,
      _40: `border: 1px solid #421987 !important;`,
      _50: `border: 1px solid #F5F1FE !important;`,
      _60: `border: 1px solid #6C63FF !important;`,
    },
    Width: {
      _2: `border-width: 2px !important;`,
      _4: `border-width: 4px !important;`,
    },
    Dashed: `border-style: dashed !important;`,
    _0: `border: 0 !important;`,
    Top: {
      _0: `border-top: 0 !important;`,
    },
    Right: {
      _0: `border-right: 0 !important;`,
    },
    Bottom: {
      _0: `border-bottom: 0 !important;`,
      Secondary: {
        _70: {
          Opacity25: `border-bottom: 1px solid rgba(112, 112, 112, 0.25) !important;`,
          Opacity5: `border-bottom: 1px solid rgba(112, 112, 112, 0.5) !important;`,
        },
      },
    },
    Left: {
      _0: `border-left: 0 !important;`,
      Success: {
        _10: `border-left: 10px solid #199473 !important;`,
      },
    },
  },
  Shadow: {
    None: `box-shadow: none !important;`,
    Sm: `box-shadow: 0 0.2rem 0.3rem 0.1rem rgba(0, 0, 0, 0.1) !important;`,
    Lg: `box-shadow: 0 0.2rem 0.3rem 0.1rem rgba(0, 0, 0, 0.3) !important;`,
  },
  C: {
    Default: `cursor: default !important;`,
    Pointer: `cursor: pointer !important;`,
    Text: `cursor: text !important;`,
  },
  W: {
    _75: `width: 75% !important;`,
    _100: `width: 100% !important;`,
    Auto: `width: auto !important;`,
  },
  H: {
    _75: `height: 75% !important;`,
    _100: `height: 100% !important;`,
    Auto: `height: auto !important;`,
  },
  V: {
    W: {
      _75: `width: 75vw !important;`,
      _100: `width: 100vw !important;`,
    },
    H: {
      _75: `height: 75vh !important;`,
      _100: `height: 100vh !important;`,
    },
  },
  Position: {
    Static: `position: static !important;`,
    Relative: `position: relative !important;`,
    Absolute: `position: absolute !important;`,
    Fixed: `position: fixed !important;`,
  },
  Flex: {
    _1: `flex: 1 !important;`,
    Fill: `flex: 1 1 auto !important;`,
    Column: `flex-direction: column !important;`,
    Wrap: `flex-wrap: wrap !important;`,
  },
  D: {
    None: `display: none !important;`,
    Inline: `display: inline !important;`,
    Block: `display: block !important;`,
    InlineBlock: `display: inline-block !important;`,
    Flex: `display: flex !important;`,
    InlineFlex: `display: inline-flex !important;`,
    Grid: `display: grid !important;`,
  },
  Overflow: {
    Visible: `overflow: visible !important;`,
    Hidden: `overflow: hidden !important;`,
    Auto: `overflow: auto !important;`,
  },
  Transition: `transition: all 0.3s ease 0s;`,
  Direction: {
    Rtl: `direction: rtl;`,
    Ltr: `direction: ltr;`,
  },
  P: {
    _0: `padding: 0 !important;`,
    _1: `padding: 0.25rem !important;`,
    _2: `padding: 0.5rem !important;`,
    _3: `padding: 1rem !important;`,
    _4: `padding: 1.5rem !important;`,
    _5: `padding: 3rem !important;`,
    _65: `padding: 0.65rem !important;`,
    T: {
      _0: `padding-top: 0 !important;`,
      _1: `padding-top: 0.25rem !important;`,
      _2: `padding-top: 0.5rem !important;`,
      _3: `padding-top: 1rem !important;`,
      _4: `padding-top: 1.5rem !important;`,
      _5: `padding-top: 3rem !important;`,
      _65: `padding-top: 0.65rem !important;`,
    },
    R: {
      _0: `padding-right: 0 !important;`,
      _1: `padding-right: 0.25rem !important;`,
      _2: `padding-right: 0.5rem !important;`,
      _3: `padding-right: 1rem !important;`,
      _4: `padding-right: 1.5rem !important;`,
      _5: `padding-right: 3rem !important;`,
      _65: `padding-right: 0.65rem !important;`,
    },
    B: {
      _0: `padding-bottom: 0 !important;`,
      _1: `padding-bottom: 0.25rem !important;`,
      _2: `padding-bottom: 0.5rem !important;`,
      _3: `padding-bottom: 1rem !important;`,
      _4: `padding-bottom: 1.5rem !important;`,
      _5: `padding-bottom: 3rem !important;`,
      _65: `padding-bottom: 0.65rem !important;`,
    },
    L: {
      _0: `padding-left: 0 !important;`,
      _1: `padding-left: 0.25rem !important;`,
      _2: `padding-left: 0.5rem !important;`,
      _3: `padding-left: 1rem !important;`,
      _4: `padding-left: 1.5rem !important;`,
      _5: `padding-left: 3rem !important;`,
      _65: `padding-left: 0.65rem !important;`,
    },
    X: {
      _0: `
                padding-right: 0 !important;
                padding-left: 0 !important;
            `,
      _1: `
                padding-right: 0.25rem !important;
                padding-left: 0.25rem !important;
            `,
      _2: `
                padding-right: 0.5rem !important;
                padding-left: 0.5rem !important;
            `,
      _3: `
                padding-right: 1rem !important;
                padding-left: 1rem !important;
            `,
      _4: `
                padding-right: 1.5rem !important;
                padding-left: 1.5rem !important;
            `,
      _5: `
                padding-right: 3rem !important;
                padding-left: 3rem !important;
            `,
      _65: `
                padding-right: 0.65rem !important;
                padding-left: 0.65rem !important;
            `,
    },
    Y: {
      _0: `
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            `,
      _1: `
                padding-top: 0.25rem !important;
                padding-bottom: 0.25rem !important;
            `,
      _2: `
                padding-top: 0.5rem !important;
                padding-bottom: 0.5rem !important;
            `,
      _3: `
                padding-top: 1rem !important;
                padding-bottom: 1rem !important;
            `,
      _4: `
                padding-top: 1.5rem !important;
                padding-bottom: 1.5rem !important;
            `,
      _5: `
                padding-top: 3rem !important;
                padding-bottom: 3rem !important;
            `,
      _65: `
                padding-top: 0.65rem !important;
                padding-bottom: 0.65rem !important;
            `,
    },
  },
  M: {
    Auto: `margin: auto !important;`,
    _0: `margin: 0 !important;`,
    _1: `margin: 0.25rem !important;`,
    _2: `margin: 0.5rem !important;`,
    _3: `margin: 1rem !important;`,
    _4: `margin: 1.5rem !important;`,
    _5: `margin: 3rem !important;`,
    _65: `margin: 0.65rem !important;`,
    T: {
      Auto: `margin-top: auto !important;`,
      _0: `margin-top: 0 !important;`,
      _1: `margin-top: 0.25rem !important;`,
      _2: `margin-top: 0.5rem !important;`,
      _3: `margin-top: 1rem !important;`,
      _4: `margin-top: 1.5rem !important;`,
      _5: `margin-top: 3rem !important;`,
      _65: `margin-top: 0.65rem !important;`,
    },
    R: {
      Auto: `margin-right: auto !important;`,
      _0: `margin-right: 0 !important;`,
      _1: `margin-right: 0.25rem !important;`,
      _2: `margin-right: 0.5rem !important;`,
      _3: `margin-right: 1rem !important;`,
      _4: `margin-right: 1.5rem !important;`,
      _5: `margin-right: 3rem !important;`,
      _65: `margin-right: 0.65rem !important;`,
    },
    B: {
      Auto: `margin-bottom: auto !important;`,
      _0: `margin-bottom: 0 !important;`,
      _1: `margin-bottom: 0.25rem !important;`,
      _2: `margin-bottom: 0.5rem !important;`,
      _3: `margin-bottom: 1rem !important;`,
      _4: `margin-bottom: 1.5rem !important;`,
      _5: `margin-bottom: 3rem !important;`,
      _65: `margin-bottom: 0.65rem !important;`,
    },
    L: {
      Auto: `margin-left: auto !important;`,
      _0: `margin-left: 0 !important;`,
      _1: `margin-left: 0.25rem !important;`,
      _2: `margin-left: 0.5rem !important;`,
      _3: `margin-left: 1rem !important;`,
      _4: `margin-left: 1.5rem !important;`,
      _5: `margin-left: 3rem !important;`,
      _65: `margin-left: 0.65rem !important;`,
    },
    X: {
      Auto: `
                margin-right: auto !important;
                margin-left: auto !important;
            `,
      _0: `
                margin-right: 0 !important;
                margin-left: 0 !important;
            `,
      _1: `
                margin-right: 0.25rem !important;
                margin-left: 0.25rem !important;
            `,
      _2: `
                margin-right: 0.5rem !important;
                margin-left: 0.5rem !important;
            `,
      _3: `
                margin-right: 1rem !important;
                margin-left: 1rem !important;
            `,
      _4: `
                margin-right: 1.5rem !important;
                margin-left: 1.5rem !important;
            `,
      _5: `
                margin-right: 3rem !important;
                margin-left: 3rem !important;
            `,
      _65: `
                margin-right: 0.65rem !important;
                margin-left: 0.65rem !important;
            `,
    },
    Y: {
      Auto: `
                margin-top: auto !important;
                margin-bottom: auto !important;
            `,
      _0: `
                margin-top: 0 !important;
                margin-bottom: 0 !important;
            `,
      _1: `
                margin-top: 0.25rem !important;
                margin-bottom: 0.25rem !important;
            `,
      _2: `
                margin-top: 0.5rem !important;
                margin-bottom: 0.5rem !important;
            `,
      _3: `
                margin-top: 1rem !important;
                margin-bottom: 1rem !important;
            `,
      _4: `
                margin-top: 1.5rem !important;
                margin-bottom: 1.5rem !important;
            `,
      _5: `
                margin-top: 3rem !important;
                margin-bottom: 3rem !important;
            `,
      _65: `
                margin-top: 0.65rem !important;
                margin-bottom: 0.65rem !important;
            `,
    },
  },
  Row: `
    flex-wrap: wrap;
    display: flex;
    margin: 0;
  `,
  Col: {
    Default: `
      width: 100%;
      position: relative;
      padding-right: 15px;
      padding-left: 15px;
    `,
    _1: `
      max-width: 8.333333%;
      flex: 0 0 8.333333%;
    `,
    _2: `
      max-width: 16.666667%;
      flex: 0 0 16.666667%;
    `,
    _3: `
      max-width: 0 0 25%;
      flex: 0 0 0 0 25%;
    `,
    _4: `
      max-width: 33.333333%;
      flex: 0 0 33.333333%;
    `,
    _5: `
      max-width: 41.666667%;
      flex: 0 0 41.666667%;
    `,
    _6: `
      max-width: 50%;
      flex: 0 0 50%;
    `,
    _7: `
      max-width: 58.333333%;
      flex: 0 0 58.333333%;
    `,
    _8: `
      max-width: 66.666667%;
      flex: 0 0 66.666667%;
    `,
    _9: `
      max-width: 75%;
      flex: 0 0 75%;
    `,
    _10: `
      max-width: 83.333333%;
      flex: 0 0 83.333333%;
    `,
    _11: `
      max-width: 91.666667%;
      flex: 0 0 91.666667%;
    `,
    _12: `
      max-width: 100%;
      flex: 0 0 100%;
    `,
  },
};
