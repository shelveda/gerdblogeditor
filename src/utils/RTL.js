import React from 'react';
import rtl from 'jss-rtl';
import { create } from 'jss';
import {
  StylesProvider,
  jssPreset,
  createGenerateClassName,
} from '@material-ui/styles';

const jss = create({
  insertionPoint: 'jss-insertion-point',
  plugins: [...jssPreset().plugins, rtl()],
});

const generateClassName = createGenerateClassName();

export default props => (
  <StylesProvider jss={jss} generateClassName={generateClassName}>
    {props.children}
  </StylesProvider>
);
