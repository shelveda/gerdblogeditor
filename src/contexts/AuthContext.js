import React, { createContext, useContext, useReducer } from 'react';

const User = JSON.parse(sessionStorage.getItem('user'));

const initialState = {
  isAdmin: User && User.role === 'Admin' ? true : false,
  ...User,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      sessionStorage.setItem(
        'user',
        JSON.stringify({ name: action.payload.name, role: action.payload.role })
      );
      sessionStorage.setItem('token', action.payload.token);
      return {
        ...state,
        isAdmin: action.payload.role === 'Admin' ? true : false,
        name: action.payload.name,
        role: action.payload.role,
      };
    case 'LOGOUT':
      sessionStorage.clear();
      return {
        ...state,
        isAdmin: false,
        user: null,
      };
    default:
      return state;
  }
};

export const AuthProvider = props => {
  const [user, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={user}>
      <DispatchUserContext.Provider value={dispatch}>
        {props.children}
      </DispatchUserContext.Provider>
    </AuthContext.Provider>
  );
};

const AuthContext = createContext();
const DispatchUserContext = createContext();

export const UseAuthContext = () => useContext(AuthContext);
export const UseUserDispatch = () => useContext(DispatchUserContext);
