import React, { createContext, useState, useContext, useEffect } from 'react';
import { MasterApi } from '../api/MasterApi';

export const ResourceProvider = props => {
  const [resource, setResource] = useState('');
  const [cats, setCatsState] = useState(
    JSON.parse(sessionStorage.getItem('cats')) || []
  );

  const [tags, setTagState] = useState(
    JSON.parse(sessionStorage.getItem('tags')) || []
  );

  const setCats = cats => {
    // const newCats = cats.filter(cat => cat.id !== 1);
    setCatsState(cats);
    sessionStorage.setItem('cats', JSON.stringify(cats));
  };

  const setTags = tags => {
    setTagState(tags);
    sessionStorage.setItem('tags', JSON.stringify(tags));
  };

  useEffect(() => {
    async function fetchData() {
      let response;
      const token = sessionStorage.getItem('token');
      if (!token) return '';
      if (cats.length === 0) {
        response = await MasterApi('GET', '/cat');
        if (response && response.status === 200) {
          response.data.length !== 0 && setCats(response.data);
        }
      }

      if (tags.length === 0) {
        response = await MasterApi('GET', '/tags');
        if (response && response.status === 200) {
          response.data.length !== 0 && setTags(response.data);
        }
      }
    }
    // fetchData();
  }, []);

  const ResourcesValues = React.useMemo(
    () => ({
      resource,
      setResource,
    }),
    [resource]
  );
  const Cats = React.useMemo(
    () => ({
      cats,
      setCats,
      tags,
      setTags,
    }),
    [cats, tags]
  );

  return (
    <ResourceContext.Provider value={ResourcesValues}>
      <CatContext.Provider value={Cats}>{props.children}</CatContext.Provider>
    </ResourceContext.Provider>
  );
};

const ResourceContext = createContext();
export const useResourceContext = () => useContext(ResourceContext);
const CatContext = createContext();
export const useCatContext = () => useContext(CatContext);
