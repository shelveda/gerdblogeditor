import React, { createContext, useState, useContext } from 'react';

export const LoadingProvider = ({ children }) => {
  const [Loading, setLoading] = useState(false);

  return (
    <LoadingContext.Provider value={{ Loading }}>
      <LoadingDispatch.Provider value={{ setLoading }}>
        {children}
      </LoadingDispatch.Provider>
    </LoadingContext.Provider>
  );
};

const LoadingContext = createContext();
const LoadingDispatch = createContext();

export const useLoadingContext = () => useContext(LoadingContext);
export const useLoadingDispatch = () => useContext(LoadingDispatch);
